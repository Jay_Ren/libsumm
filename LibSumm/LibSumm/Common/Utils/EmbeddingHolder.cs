﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Utils
{
    [Serializable]
    public class EmbeddingHolder
    {
        public bool Contains(string word)
        {
            return embeddingVecs.ContainsKey(word);
        }

        public void Add(string w, float[] vec)
        {
            if (!embeddingVecs.ContainsKey(w) && vec.Length == dimension)
            {
                embeddingVecs.Add(w, vec);
                vocab.Add(w);
            }
        }

        public float[] Vectorize(string word)
        {
            if (embeddingVecs.ContainsKey(word))
                return embeddingVecs[word];

            return null;
        }
        public float[] Vectorize(IEnumerable<string> wordList)
        {
            float[] sentVector = new float[dimension];
            foreach (string word in wordList)
            {
                if (!embeddingVecs.ContainsKey(word)) continue;
                float[] v = embeddingVecs[word];
                for (int i = 0; i < dimension; i++)
                {
                    sentVector[i] += v[i];
                }
            }
            return sentVector;
        }

        public void ReadWordEmbedding(string file, Dictionary<string, float[]> coll)
        {
            StreamReader sr = new StreamReader(file);
            string[] temp = sr.ReadLine().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            if (temp.Length == 2)
            {
                dimension = int.Parse(temp[1]);
            }
            else
            {
                throw new FormatException();
            }

            int count = 0;
            while (!sr.EndOfStream)
            {

                if (count++ % 10000 == 0)
                    Console.Write("[Read] {0} {1}\r", file, count);

                string[] line = sr.ReadLine().Split(new char[] { ' ' });
                string word = line[0].ToLower();
                if (!coll.ContainsKey(word))
                {
                    vocab.Add(word);
                    float[] vec = new float[dimension];
                    for (int i = 0; i < vec.Length; i++)
                    {
                        vec[i] = float.Parse(line[i + 1]);
                    }
                    coll.Add(word, vec);
                    //else {
                    //    Console.Write(word+",");
                    //}
                }
                else
                {
                    //Console.WriteLine(word + " duplicated");
                }

            }
            sr.Close();

            Console.Write("[Read] {0} {1}\n", file, count);
        }


        //public void ReadWordEmbedding(string file, string vocabulary, Dictionary<string, float[]> coll)
        //{
        //    int count = 0;
        //    if (!string.IsNullOrWhiteSpace(vocabulary))
        //    {
        //        using (StreamReader reader = new StreamReader(vocabulary, Encoding.UTF8))
        //        {
        //            string line = null;
        //            while ((line = reader.ReadLine()) != null)
        //            {
        //                if (count++ % 10000 == 0)
        //                    Console.Write("[LOG] READ - {0} [{1}]\r", vocabulary, count);

        //                line = line.Trim();
        //                vocab.Add(line.ToLower());
        //            }
        //        }

        //        Console.Write("[LOG] READ - {0} [{1}]\r", vocabulary, count);
        //    }

        //    if (!File.Exists(file))
        //    {
        //        Console.ForegroundColor = ConsoleColor.Red;
        //        Console.WriteLine("[ERROR]: CANNOT FIND {0}", file);
        //        Console.ForegroundColor = ConsoleColor.White;
        //        return;
        //    }

        //    count = 0;
        //    int num = 0;
        //    using (StreamReader reader = new StreamReader(file, Encoding.UTF8))
        //    {
        //        string line = null;
        //        while ((line = reader.ReadLine()) != null)
        //        {
        //            if (count++ % 10000 == 0)
        //                Console.Write("[LOG] READ - {0} [{1}]\r", file, count);

        //            line = line.Trim();
        //            if (string.IsNullOrEmpty(line))
        //                continue;

        //            string[] items = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

        //            string word = null;
        //            int offset = 0;
        //            if (vocab != null)
        //                word = vocab[num++];
        //            else
        //            {
        //                offset = 1;
        //                word = items[0];
        //            }

        //            List<float> vec = new List<float>();
        //            for (int k = offset; k < items.Length; ++k)
        //            {
        //                vec.Add(float.Parse(items[k]));
        //            }

        //            if (!coll.ContainsKey(word))
        //                coll.Add(word, vec.ToArray());
        //        }
        //    }

        //    Console.Write("[LOG] READ - {0} [{1}]\n", file, count);
        //}

        public void Append(string file)
        {
            ReadWordEmbedding(file, embeddingVecs);
        }

        public EmbeddingHolder(string file)
        {
            embeddingVecs = new Dictionary<string, float[]>();
            Append(file);
            Console.WriteLine();
        }

        private int dimension = 50;

        public int Dimension { get { return dimension; } }

        public ICollection<string> Vocab { get { return embeddingVecs.Keys; } }

        private Dictionary<string, float[]> embeddingVecs = null;
        List<string> vocab = new List<string>();


        public Dictionary<string, double> Convert(float[] dist)
        {
            if (dist.Length != vocab.Count)
            {
                throw new OverflowException();
            }

            Dictionary<string, double> dict = new Dictionary<string, double>();

            for (int i = 0; i < vocab.Count; i++)
            {
                dict.Add(vocab[i], dist[i]);
            }

            return dict;
        }

        public float[] BOWDistVector(string[] words)
        {

            int dim = Vocab.Count;

            float[] dist = new float[dim];
            int num = 0;
            foreach (string w in words)
            {
                int index = vocab.IndexOf(w);
                if (index >= 0)
                {
                    dist[index] += 1f;
                    num++;
                }
            }

            for (int i = 0; i < dist.Length; i++)
                dist[i] /= num;

            return dist;
        }

        public float[] BOWSoftMaxDistVector(string[] words)
        {

            int dim = Vocab.Count;

            float[] dist = new float[dim];
            foreach (string w in words)
            {
                int index = vocab.IndexOf(w);
                if (index >= 0)
                {
                    dist[index] += 1f;
                }
            }

            float num = 0;
            foreach (float f in dist)
            {
                num += (float)Math.Exp(f);
            }

            for (int i = 0; i < dist.Length; i++)
                dist[i] = (float)(Math.Exp(dist[i]) / num);

            return dist;
        }


        public float[] BOWVector(string[] words)
        {
            int dim = Vocab.Count;
            List<string> tmpwords = vocab;

            float[] vec = new float[dim];
            foreach (string w in words)
            {
                int index = tmpwords.IndexOf(w);
                if (index >= 0)
                    vec[index] = 1f;
            }
            return vec;
        }
        public float[] BOWTFVector(string[] words)
        {
            int dim = Vocab.Count;
            List<string> tmpwords = vocab;

            float[] vec = new float[dim];
            foreach (string w in words)
            {
                int index = tmpwords.IndexOf(w);
                if (index >= 0)
                    vec[index] += 1f;
            }
            return vec;
        }


        public List<string> Words(float[] bowvec)
        {
            int dim = Vocab.Count;
            if (bowvec.Length != dim)
            {
                throw new Exception("Not a BOW vec");
            }

            List<string> rs = new List<string>();
            List<string> tmpwords = vocab;
            for (int i = 0; i < bowvec.Length; i++)
            {
                if (bowvec[i] > 0)
                {
                    rs.Add(tmpwords[i]);
                }
            }

            return rs;
        }
    }
}
