﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROUGEPackage
{
   public class ROUGEUtils
    {
        private ITextFromatter formatter = new ChineseFormatter();
        private ROUGE rouge;
        private Setting setting;
        public ROUGEUtils(Setting setting)
        {
            this.setting = setting;
            formatter = new EnglishFormatter();
            rouge = new ROUGE(setting, formatter);
        }


        public double[] Recall(string system,IList<string> references) {
            double[] max = new double[this.setting.NgramSize];
            for (int i = 0; i < references.Count; i++)
            {
                var results = rouge.EvaluateFromString(references[i], system, i + "");
                for (int j = 0; j < max.Length; j++) {
                    if (max[j] < results[j].Recall) {
                        max[j] = results[j].Recall;
                    }
                }
            }
            return max;
        }

        public double[] F1(string system, IList<string> references)
        {
            double[] max = new double[this.setting.NgramSize];
            for (int i = 0; i < references.Count; i++)
            {
                var results = rouge.EvaluateFromString(references[i], system, i + "");
                for (int j = 0; j < max.Length; j++)
                {
                    if (max[j] < results[j].FMeasure)
                    {
                        max[j] = results[j].FMeasure;
                    }
                }
            }
            return max;
        }

        //public double[] DumpRougeNgramRecall(string system, string givenSystem, IList<string> references) {
        //    var r1 = RougeNgramRecall(system, references);
        //    var r2 = RougeNgramRecall(givenSystem, references);
        //    var r = RougeNgramRecall(system + " " + givenSystem, references);
        //    for (int i = 0; i < r.Length; i++) {
        //        r[i] = r1[i] + r2[i] - r[i];
        //    }
        //    return r;
        //}


        public double[] Recall(string system,string givenSystem,IList<string> references) {
           //var r1= RougeNgramRecall(system,references);
           //var r2 = RougeNgramRecall(givenSystem, references);
           //var r = RougeNgramRecall(system+" "+givenSystem, references);
           //for (int i = 0; i < r.Length; i++)
           //{
           //    r[i] = r1[i] + r2[i] - r[i];
           //    r1[i] = r1[i] - r[i];
           //}
           //return r1;

            var r2 = Recall(givenSystem, references);
            var r = Recall(system + " " + givenSystem, references);
            for (int i = 0; i < r.Length; i++)
            {
                r[i] = r[i] - r2[i];
            }
            return r;
        }


    }
}
