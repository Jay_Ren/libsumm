﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ROUGEPackage
{
    public interface ITextFromatter
    {
        string FormatText(string text);
    }

    public class EnglishFormatter:ITextFromatter
    {
        public string FormatText(string text)
        {
            text = text.ToLower();
            text = Regex.Replace(text, @"[^0-9a-z]", " ");
            text = Regex.Replace(text, @"\s+", " ");
            return text.Trim();
        }
    }
    public class ChineseFormatter : ITextFromatter
    {
        public string FormatText(string text)
        {
            text = text.ToLower();
            text = Regex.Replace(text, "[^0-9a-z\u4e00-\u9fa5]", " ");
            text = Regex.Replace(text, @"\s+", " ");
            return text.Trim();
        }
    }
}
