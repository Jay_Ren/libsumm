﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ROUGEPackage
{
    public class ROUGE
    {
        public List<EvaluateResult> EvaluateFromFile(string referenceFile, string systemFile,string ID="")
        {
            string reference = ReadFile(referenceFile);
            string system = ReadFile(systemFile);
            system = Truncate(system);
            List<EvaluateResult> results = Evaluate(reference, system,ID);
            return results;
        }
        public List<EvaluateResult> EvaluateFromString(string reference, string system,string ID="")
        {
            reference = FormatSummary(reference);
            system = FormatSummary(system);
            system = Truncate(system);
            List<EvaluateResult> results = Evaluate(reference, system,ID);
            return results;
        }
        private string FormatSummary(string text)
        {
            StringBuilder sb = new StringBuilder();
            string[] sents = text.Split('\n');
            foreach (string sent in sents)
            {
                string formatted = formatter.FormatText(sent);
                if (formatted.Length > 0)
                {
                    sb.Append(formatted + "\n");
                }
            }
            return sb.ToString().Trim();
        }
        private List<EvaluateResult> Evaluate(string reference, string system,string ID="")
        {
            List<EvaluateResult> results = new List<EvaluateResult>();
            for (int i = 1; i <= setting.NgramSize; i++)
            {
                Dictionary<string, int> referenceNgramMap = ToNgramMap(reference, i);
                Dictionary<string, int> systemNgramMap = ToNgramMap(system, i);
                int referenceCount = referenceNgramMap.Values.Sum();
                int systemCount = systemNgramMap.Values.Sum();
                EvaluateResult result = new EvaluateResult();
                result.EvaluateType = string.Format("MyROUGE-{0}", i);
                result.EvaluateID = ID;
                results.Add(result);
                if (referenceCount == 0 || systemCount == 0) continue;
                int matchCount = 0;
                foreach (var pair in referenceNgramMap)
                {
                    if (!systemNgramMap.ContainsKey(pair.Key)) continue;
                    matchCount += Math.Min(pair.Value, systemNgramMap[pair.Key]);
                }
                result.Precise = matchCount / (double)systemCount;
                result.Recall = matchCount / (double)referenceCount;
            }
            return results;
        }
        public Dictionary<string,int> ToNgramMap(string text,int n)
        {
            Dictionary<string, int> ngramMap = new Dictionary<string, int>();
            string[] sents = text.Split(new char[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string sent in sents)
            {
                string[] words = sent.Split(new char[] { ' ' },StringSplitOptions.RemoveEmptyEntries);
                int len = words.Length-n;
                for (int i = 0; i <= len; i++)
                {
                    List<string> subWords = new List<string>(n);
                    for (int j = 0; j < n; j++)
                    {
                        string word = words[i + j];
                        if (setting.Stopwords != null && setting.Stopwords.Contains(word))
                        {
                            continue;
                        }
                        subWords.Add(words[i + j]);
                    }
                    if (subWords.Count == 0) continue;
                    string ngram = string.Join(" ", subWords);
                    if (ngramMap.ContainsKey(ngram))
                    {
                        ngramMap[ngram]++;
                    }
                    else
                    {
                        ngramMap[ngram] = 1;
                    }
                }
            }
            return ngramMap;
        }


        string ReadFile(string filename)
        {
            StringBuilder sb = new StringBuilder();
            using (StreamReader sr = new StreamReader(filename, setting.FileEncoding))
            {
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine().Trim().ToLower();
                    if (line.Length == 0) continue;
                    if (line.Length > 0)
                    {
                        sb.AppendLine(line);
                    }
                }
            }
            string text = sb.ToString().Trim() ;
            text = FormatSummary(text);
            return text;
        }
        string Truncate(string text)
        {
            if (setting.SummaryLength <= 0) return text;
            switch (setting.CurrentLengthType)
            {
                case Setting.LengthLimitType.WORD:
                    StringBuilder sb = new StringBuilder();
                    string[] lines=text.Split('\n');
                    int restLen = setting.SummaryLength;
                    foreach (string line in lines)
                    {
                        string[] words = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        if (words.Length <= restLen)
                        {
                            sb.AppendLine(line);
                            restLen -= words.Length;
                        }
                        else
                        {
                            string subLine = string.Join(" ", words.Take(restLen));
                            sb.AppendLine(subLine);
                            break;
                        }

                    }
                    text = sb.ToString().Trim();
                    break;
                case Setting.LengthLimitType.BYTE:
                    if (text.Length > setting.SummaryLength)
                    {
                        text = text.Substring(0, setting.SummaryLength);
                    }
                    break;
            }
            return text;
        }
        public ROUGE(Setting setting, ITextFromatter formatter)
        {
            this.setting = setting;
            this.formatter = formatter;
        }
        Setting setting;
        ITextFromatter formatter;
    }

}
