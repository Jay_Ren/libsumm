﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROUGEPackage
{
    public class Setting
    {
        public enum LengthLimitType
        {
            WORD,BYTE
        }
        public LengthLimitType CurrentLengthType { get; set; }
        public int SummaryLength { get; set; }
        public HashSet<string> Stopwords { get; set; }
        public Encoding FileEncoding { get; set; }
        public int NgramSize { get; set; }
        public Setting()
        {
            CurrentLengthType = LengthLimitType.WORD;
            SummaryLength = -1;
            Stopwords = null;
            FileEncoding = Encoding.UTF8;
            NgramSize = 2;
        }
    }
}
