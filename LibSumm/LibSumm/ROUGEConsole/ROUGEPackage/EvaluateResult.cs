﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROUGEPackage
{
    public class EvaluateResult
    {
        public double Precise { get; set; }
        public double Recall { get; set; }
        public double StdPrecise { get; private set; }
        public double StdRecall { get; private set; }
        public double StdFMeasure { get { return (StdPrecise + StdRecall) / 2; } }
        public double FMeasure
        {
            get
            {
                if (Precise == 0 || Recall == 0) return 0;
                return 2 * Precise * Recall / (Precise + Recall);
            }
        }
        public string EvaluateType { get; set; }
        public string EvaluateID { get; set; }
        public string ShowResult()
        {
            StringBuilder sb = new StringBuilder();
            if (EvaluateID==null|| EvaluateID.Length == 0)
                sb.AppendLine(EvaluateType);
            else
                sb.AppendLine(EvaluateType + "===" + EvaluateID);
            string mask = "0.0000";
            string pStr = Precise.ToString(mask);
            string rStr = Recall.ToString(mask);
            string fStr = FMeasure.ToString(mask);
            if (StdFMeasure == 0)
            {
                string info = string.Format("P={0}\tR={1}\tF={2}", pStr, rStr, fStr);
                sb.AppendLine(info);
            }
            else
            {
                string pStdStr = StdPrecise.ToString(mask);
                string rStdStr = StdRecall.ToString(mask);
                string fStdStr = StdFMeasure.ToString(mask);
                string info = string.Format("P={0}({1})\tR={2}({3})\tF={4}({5})", pStr,pStdStr, rStr,rStdStr, fStr,fStdStr);
                sb.AppendLine(info);
            }
            return sb.ToString();
        }
        public static List<EvaluateResult> ComputeOverallEvaluation(List<EvaluateResult> resultList)
        {
            List<EvaluateResult> overallList = new List<EvaluateResult>();
            var groups = resultList.GroupBy(x => x.EvaluateType);
            foreach (var group in groups)
            {
                EvaluateResult overall = new EvaluateResult();
                overallList.Add(overall);
                overall.EvaluateType = group.Key;
                var pList = group.Select(x => x.Precise);
                var rList = group.Select(x => x.Recall);
                overall.Precise = pList.Average();
                overall.Recall = rList.Average();
                overall.StdPrecise = Math.Sqrt(pList.Average(x => Math.Pow(x - overall.Precise, 2)));
                overall.StdRecall=Math.Sqrt(rList.Average(x=>Math.Pow(x-overall.Recall,2)));
            }
            return overallList;
        }
    }
}
