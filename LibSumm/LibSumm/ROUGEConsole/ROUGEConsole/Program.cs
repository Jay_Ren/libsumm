﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ROUGEPackage;
using System.IO;

namespace ROUGEConsole
{
    class Program
    {
        static void Main(string[] args)
        {
          
            Setting setting = new Setting();
            setting.NgramSize = 4;
            
            ITextFromatter formatter = new ChineseFormatter();
            formatter = new EnglishFormatter();
            ROUGE rouge = new ROUGE(setting, formatter);
            string reference = "上市 公司 公布 的 2014年 四 季报 显示";
            string document = "上市 公司 四 公布 的 2014  年 季报 显示";
            reference = "Steroids have been in increasing use since first observed in international competition in 1956 . Used legitimately for some diseases , steroids are used to build up muscles , strength , and endurance , and are taken by female as well as male athletes . The relative newness of women 's sports allows countries to make inroads more quickly than in men 's athletics . With the lure of scholarships and professional ventures there is more at stake now than ever for female athletes . Use of steroids by 8-12th grade girls has gone up 100 % from 1996 to 1998 . During the 1990 's they were in widespread use in international competitions , affecting women athletes from many nations and sports fields . Steroids increase the amount of testosterone , which is dramatic in women because of the small amount in their bodies originally . However , there are many side effects , including liver damage , increase in facial hair , voice changes , acne , sterility , enlarged Adam 's apple , shrinkage of the uterus , gynecological problems , elevated cholesterol , aggressive behavior , and stunted growth in younger users . The consequences of steroid use , physically , are strain on liver , kidneys , and heart , leading to vascular damage and risks of blood-clotting , stroke , and heart attack . Abnormal incidences of cancer , birth defects , and depression have also been associated with steroid use . Legal and professional consequences include suspension and banning from competitions , dropping from teams , fines , loss of promotional contracts , and loss of awards and medals . Coaches and doctors who administer steroids to unknowing athletes risk fines or prison . ";
            document = "Increasing numbers of female athletes use anabolic steroids . Among female college athletes , an estimated 5 % in swimming , 6 % in basketball and 10 % in track and field have used steroids . The fastest growing group of users is adolescent females . New steroids are often marketed as dietary supplements and girls are unaware of what they are taking . A young tennis player tested positive for steroids after taking an \" all natural \" supplement later discovered to contain a variation on the banned steroid nandrolone . A former Olympic track coach believes at least 40 % of the U.S. women 's team in Seoul had used steroids at some time . Because of their naturally low testosterone levels , steroids have a more dramatic effect on women , boosting their levels up to 10 times . The health effects can be dramatic , including liver damage and tumors , elevated cholesterol , heart attacks , strokes , stunted growth in adolescents , infertility , uncontrollably violent behavior , chronic depression , deeper voices , excessive facial hair , and acne . Many East German female athletes were given steroids , often unbeknownst to them or their parents and suffered serious side-effects . Athletes who have tested positive for steroids include a Bulgarian triple jumper , a Romanian hammer thrower , a Russian runner , a Dominican high , a Jamaican sprinter , a Spanish pole vaulter , a German marathon runner , two Moroccan athletes , and two Chinese Taipei weightlifters . A number of female Chinese athletes have tested positive , including several swimmers and rowers . International organizations impose bans of between two months and life on athletes found guilty of using illegal steroids . ";
            var results = rouge.EvaluateFromString(reference, document);
            foreach (var result in results)
            {
                Console.WriteLine(result.ShowResult());
            }
            Console.ReadKey();
        }
    }
}
