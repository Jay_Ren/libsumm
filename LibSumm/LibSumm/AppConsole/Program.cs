﻿using Common.Utils;
using LibSumm.Entity;
using LibSumm.Features;
using LibSumm.Summarizer.Scorer;
using LibSumm.Test;
using MathNet.Numerics;
using MathNet.Numerics.Providers.LinearAlgebra.Mkl;
using NLP;
using NN4CSharp.Layers;
using ROUGEPackage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using LibSumm;
using LibSumm.Utils;
using LibSumm.Summarizer.Generator;
using MathNet.Numerics.LinearAlgebra;
using LibSumm.Summarizer.Scorer.Groundtruth;
using MathNet.Numerics.Statistics;

namespace AppConsole
{
   public class Program
    {
        static void Main(string[] args)
        {
            //LibSumm.Constant.Root = @"E:\projects\LibSumm\root";
            //LibSumm.Constant.Root = @"..\root";

            //LibSumm.Constant.Root = @"E:\rpj\root";
            //LibSumm.Constant.ROUGE_ROOT = Path.Combine(LibSumm.Constant.Root, @"Rouge\RELEASE-1.5.5");
            //LibSumm.Constant.PERL_ROOT = @"C:\Perl64\bin";

            Control.UseSingleThread();
            Control.UseNativeMKL(MklConsistency.Auto, MklPrecision.Single, MklAccuracy.Low);
            Console.WriteLine(Control.LinearAlgebraProvider);

            SRMLPTest.Test();
            //QF_SRMLPTest.Test();
        }
    }
}
