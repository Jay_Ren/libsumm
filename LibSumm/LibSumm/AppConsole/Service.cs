﻿using Common.Utils;
using LibSumm.Entity;
using LibSumm.Features;
using LibSumm.Summarizer.Generator;
using LibSumm.Summarizer.Scorer;
using MSRA.NLC.Common.NLP;
using Newtonsoft.Json;
using NLP;
using NN4CSharp.Layers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace AppConsole
{
    public class Service
    {
        public static void Run()
        {
            SocketServer service = new SocketServer();
            service.Start();
        }
    }

    public class SocketServer
    {
        SRMLP srmlp;
        TextCorpusReader reader;
        StopWordHandler stop = new StopWordHandler(Path.Combine(LibSumm.Constant.Root, @"model\stop.dict"));
        CorpusAnalyzer analyzer;
        public SocketServer() {
            EmbeddingHolder embedding = new EmbeddingHolder(Path.Combine(LibSumm.Constant.Root, @"model\glove.6B.50d.txt"));

            UniSentFeatureEngine featureEngine = new UniSentFeatureEngine();
            featureEngine.Add(new PositionFeature());
            featureEngine.Add(new LengthFeature());
            featureEngine.Add(new TFFeature(stop));
            featureEngine.Add(new DFFeature(stop));
            featureEngine.Add(new StopFeature(stop));
            //featureEngine.Add(new CoverageFeature(embedding, stop));
            featureEngine.Add(new EmbeddingFeature(embedding));

            srmlp =SRMLP.Load(Path.Combine(LibSumm.Constant.Root, "model/SRMLP.model"), featureEngine);

            var stanfordnlpDir = Path.Combine(LibSumm.Constant.Root, @"model\stanford-corenlp-models");
            string stemmerFile = Path.Combine(LibSumm.Constant.Root, @"model\wordnet.dict");
            var nlp = new StanfordCoreNLPTool(stanfordnlpDir, stemmerFile, "tokenize, ssplit, pos, lemma");
            reader = new TextCorpusReader(nlp);
              analyzer = new CorpusAnalyzer(stop);
        }


        private byte[] result = new byte[1024];
        private Socket serverSocket;
        public void Start()
        {
            try {
                //服务器IP地址
                IPAddress ip = IPAddress.Parse("127.0.0.1");
                serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                serverSocket.Bind(new IPEndPoint(ip, 8885));  //绑定IP地址：端口
                serverSocket.Listen(10);    //设定最多10个排队连接请求
                Console.WriteLine("Listen on {0}", serverSocket.LocalEndPoint.ToString());
                //通过Clientsoket发送数据
                Thread myThread = new Thread(ListenClientConnect);
                myThread.Start();
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
            
        }

        /// <summary>
        /// 监听客户端连接
        /// </summary>
        private void ListenClientConnect()
        {
            while (true)
            {
                Socket clientSocket = serverSocket.Accept();
                Thread receiveThread = new Thread(ReceiveMessage);
                receiveThread.Start(clientSocket);
            }
        }
        /// <summary>
        /// 接收消息
        /// </summary>
        /// <param name="clientSocket"></param>
        private void ReceiveMessage(object clientSocket)
        {
            Socket myClientSocket = (Socket)clientSocket;
            while (true)
            {
                try
                {
                    //通过clientSocket接收数据
                    int receiveNumber = myClientSocket.Receive(result);
                    string msg = Encoding.UTF8.GetString(result, 0, receiveNumber);
                    Console.WriteLine("Receive from {0} Msg: {1}", myClientSocket.RemoteEndPoint.ToString(), msg);

                    var input = JsonConvert.DeserializeObject<string[]>(msg);
                    var dic = Service(input);

                    msg = JsonConvert.SerializeObject(dic);

                    myClientSocket.Send(Encoding.UTF8.GetBytes(msg));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(ex.StackTrace);

                    var results = new List<Dictionary<string, string>>[1];
                    List<Dictionary<string, string>> list = new List<Dictionary<string, string>>();
                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    list.Add(dic);
                    results[0] = list;
                    dic.Add("ERROR", "Please make sure the link is valid.");

                    //myClientSocket.Shutdown(SocketShutdown.Both);
                    //myClientSocket.Close();

                    var msg = JsonConvert.SerializeObject(results);
                    myClientSocket.Send(Encoding.UTF8.GetBytes(msg));
                }


            }
        }


        public Corpus ToCorpus(string text) {
            Corpus corpus = reader.Read(text)[0];
            analyzer.Analyze(corpus);
            return corpus;
        }

        private string Heat(Document d)
        {
            StringBuilder sw = new StringBuilder();
            //sw.AppendLine("<!DOCTYPE html>");
            //sw.AppendLine("<html>");
            //sw.AppendLine("<head>");
            //sw.AppendLine("<style type =\"text/css\">");
            //sw.AppendLine("p.big { line-height: 200%}");
            //sw.AppendLine("div.word {");
            //sw.AppendLine("width: 100%;");
            //sw.AppendLine("border: 3px solid #73AD21;");
            ////sw.WriteLine("padding: 10px;");
            //sw.AppendLine("word-wrap:normal");
            //sw.AppendLine("}");
            //sw.AppendLine("</style>");
            //sw.AppendLine("<meta charset=\"utf-8\">");
            //sw.AppendLine("<title>" + d.ID + "</title>");
            //sw.AppendLine("<script src=\"~/Scripts/jquery-3.1.1.min.js\"></script>");
            //sw.AppendLine("<script src=\"~/Scripts/heatmap.js\"></script>");
            //sw.AppendLine("<script src=\"~/Scripts/heat.js\"></script>");
            //sw.AppendLine("</head>");
            //sw.AppendLine("<body>");
            sw.AppendLine("<div id=\"heatmapContainer\" class=\"word\">");
            sw.AppendLine("<p class=\"big\">");
            var scores = new List<float>();
                foreach (var s in d.Sentences)
                {
                    foreach (var w in s.Tokens)
                    {
                        sw.AppendLine("<font>"+ w.CONTENT.Replace("-", "") + " </font>");
                        scores.Add(s.PDScore * 10);
                    }
                    sw.AppendLine("&nbsp;");
                }
                sw.AppendLine();
                sw.AppendLine("</p>");
                sw.AppendLine("</div>");
            sw.AppendLine("<script>");
            sw.AppendLine("var elements = $(\"font\");");
            sw.AppendLine("var temperatures=[" + string.Join(",", scores) + "];");
            sw.AppendLine("var heatmap = h337.create({");
            sw.AppendLine("container: document.getElementById('heatmapContainer'),");
            sw.AppendLine("blur: 1,");
            sw.AppendLine("maxOpacity: .5");
            sw.AppendLine("});");
            sw.AppendLine("heat(heatmap,elements,temperatures);");
            sw.AppendLine("</script>");
            //sw.AppendLine("</body>");
            //sw.AppendLine("</html>");
            return sw.ToString();
        }

        public virtual List<Dictionary<string, string>>[] Service(string[] input)
        {
            string query = input[0];
            string modelType = input[1];
            int len = int.Parse(input[2]);

            if (WebUtility.IsHttpLink(query))
            {
                List<Dictionary<string, string>>[] results = new List<Dictionary<string, string>>[3];

                List<Dictionary<string, string>> table0 = new List<Dictionary<string, string>>();
                results[0] = table0;
                List<Dictionary<string, string>> table1 = new List<Dictionary<string, string>>();
                results[1] = table1;
                List<Dictionary<string, string>> table2 = new List<Dictionary<string, string>>();
                results[2] = table2;
                var text = WebUtility.ExtractTextContentFromLink(query);
                var corpus = ToCorpus(text);

                List<Sentence> sents = new List<Sentence>();
                if (modelType.Equals("SRMLP"))
                {
                    foreach (var doc in corpus.Documents) {
                        foreach (Sentence sent in doc.Sentences)
                        {
                            
                            if (sent == null || sent.Tokens == null || sent.Len < 1)
                                continue;

                            sents.Add(sent);

                            sent.PDScore = srmlp.Score(sent,null);

                            Dictionary<string, string> dic = new Dictionary<string, string>();
                            table2.Add(dic);
                            dic.Add("Sentence", sent.Content);
                            dic.Add("Position", sent.Position + "");
                            dic.Add("Length", sent.Len + "");
                            dic.Add("Score", sent.PDScore + "");
                            if (table2.Count >= 20)
                                break;
                        }
                    }


                    var generator = new ThresholdSummaryGenerator(len, 0.5f, 2, stop);
                    generator.Generate("temp",corpus,srmlp);
                    var summ = corpus.PDSummaries["temp"].Sentences;
                    corpus.PDSummaries.Remove("temp");
                    var fsumm = summ.OrderByDescending(s => { return -s.Position; });
                    string summary = string.Join(" ", fsumm.Select(s => { return s.Content; }));
                    Dictionary<string, string> dicc = new Dictionary<string, string>();
                    table1.Add(dicc);
                    dicc.Add("Summary", summary);

                    dicc = new Dictionary<string, string>();
                    table0.Add(dicc);
                    //sents.OrderByDescending(s => { return -s.Position; });
                    //string content = string.Join(" ", sents.Select(s => { return s.Content; }));
                    dicc.Add("Html2Text", Heat(corpus.Documents[0]));

                    return results;
                }
                else if (modelType.Equals("LexRank")) {
                    LexRank model = new LexRank();

                    foreach (var doc in corpus.Documents) {
                        foreach (Sentence sent in doc.Sentences)
                        {
                            if (sent == null || sent.Tokens == null || sent.Len < 1)
                                continue;

                            sents.Add(sent);

                            sent.PDScore = model.Score(sent,null);

                            Dictionary<string, string> dic = new Dictionary<string, string>();
                            table2.Add(dic);
                            dic.Add("Sentence", sent.Content);
                            dic.Add("Position", sent.Position + "");
                            dic.Add("Length", sent.Len + "");
                            dic.Add("Score", sent.PDScore + "");
                            if (table2.Count >= 20)
                                break;
                        }
                    }


                    var generator = new ThresholdSummaryGenerator(len, 0.5f, 2, stop);
                    generator.Generate("temp", corpus, model);
                    var summ = corpus.PDSummaries["temp"].Sentences;
                    corpus.PDSummaries.Remove("temp");
                    var fsumm = summ.OrderByDescending(s => { return -s.Position; });
                    string summary = string.Join(" ", fsumm.Select(s => { return s.Content; }));
                    Dictionary<string, string> dicc = new Dictionary<string, string>();
                    table1.Add(dicc);
                    dicc.Add("Summary", summary);

                    dicc = new Dictionary<string, string>();
                    table0.Add(dicc);
                    //sents.OrderByDescending(s => { return -s.Position; });
                    //string content = string.Join(" ", sents.Select(s => { return s.Content; }));
                    dicc.Add("Html2Text", Heat(corpus.Documents[0]));

                    return results;
                }
                else if (modelType.Equals("ContinuousLexRank"))
                {
                    ContinuousLexRank model = new ContinuousLexRank();

                    foreach (var doc in corpus.Documents)
                    {
                        foreach (Sentence sent in doc.Sentences)
                        {
                            if (sent == null || sent.Tokens == null || sent.Len < 1)
                                continue;

                            sents.Add(sent);

                            sent.PDScore=model.Score(sent, null);

                            Dictionary<string, string> dic = new Dictionary<string, string>();
                            table2.Add(dic);
                            dic.Add("Sentence", sent.Content);
                            dic.Add("Position", sent.Position + "");
                            dic.Add("Length", sent.Len + "");
                            dic.Add("Score", sent.PDScore + "");
                            if (table2.Count >= 20)
                                break;
                        }
                    }

                    var generator = new ThresholdSummaryGenerator(len, 0.5f, 2, stop);
                    generator.Generate("temp", corpus, model);
                    var summ = corpus.PDSummaries["temp"].Sentences;
                    corpus.PDSummaries.Remove("temp");
                    var fsumm = summ.OrderByDescending(s => { return -s.Position; });
                    string summary = string.Join(" ", fsumm.Select(s => { return s.Content; }));
                    Dictionary<string, string> dicc = new Dictionary<string, string>();
                    table1.Add(dicc);
                    dicc.Add("Summary", summary);

                    dicc = new Dictionary<string, string>();
                    table0.Add(dicc);
                    //sents.OrderByDescending(s => { return -s.Position; });
                    //string content = string.Join(" ", sents.Select(s => { return s.Content; }));
                    dicc.Add("Html2Text", Heat(corpus.Documents[0]));

                    return results;
                }

                return null;
            }
            else {
                List<Dictionary<string, string>>[] results = new List<Dictionary<string, string>>[1];
                List<Dictionary<string, string>> list = new List<Dictionary<string, string>>();
                Dictionary<string, string> dic = new Dictionary<string, string>();
                list.Add(dic);
                results[0] = list;
                dic.Add("ERROR","Not a html link or the link is invalid.");

                return results;
            }


        }
    }
}
