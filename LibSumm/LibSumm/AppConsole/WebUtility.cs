﻿using StanSoft;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AppConsole
{
    public class WebUtility
    {

        public static string ExtractTextContentFromLink(string link)
        {
            //var res = Microsoft.KnowledgeMining.MainArticleExtractor.Extractor.ExtractUrl(link);

            //var title = res.TitleInText;
            //var body = res.BodyInText;

            //string content = title + "\r\n" + body;

            //return content.Trim();

            string html = "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(link);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream, Encoding.GetEncoding(response.CharacterSet)))
            {
                Console.WriteLine(response.CharacterSet);
                html = reader.ReadToEnd();
            }
            Article article = Html2Article.GetArticle(html);
            string content = article.Title + ". " + article.Content;
            return content.Trim();
        }

        public static bool IsHttpLink(string link)
        {
            Uri uriResult;
            bool flag = Uri.TryCreate(link, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
            return flag;
        }

    }
}
