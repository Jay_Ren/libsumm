﻿

function heat(heatmap,elements, temperatures) {

var m_radius = 20; 
var m_x = 0;        
var m_y = 0;       
var m_xcount = 0;
var m_ycount = 0;
var data = [];

for (var i = 0; i < elements.length; i++) {
    var m= elements.eq(i);
    m_xcount = m.width() / (m_radius);
    m_ycount = m.height() / (m_radius);

    var zf_p_l = m.css("padding-left");
    var zf_p_t = m.css("padding-top");


    m_y = m.offset().top;
    
    for (var j = 0; j < m_ycount; j++) {
    m_x = m.offset().left;
    
      if (zf_p_l) {
        zf_p_l = zf_p_l.substring(0, zf_p_l.length - 2);
        zf_p_l = zf_p_l * 1;
        m_x += zf_p_l;
    }


    if (zf_p_t) {
        zf_p_t = zf_p_t.substring(0, zf_p_t.length - 2);
        zf_p_t = zf_p_t * 1;
        m_y += zf_p_t;
    }
    for (var k = 0; k < 2*m_xcount; k++) {
        data.push({ x: m_x, y: m_y, value: temperatures[i], radius: m_radius });
        m_x += 0.5*m_radius;
    }
    m_y+= m_radius;
}
}

heatmap.setData({
    max: 10,
    data: data
});
}