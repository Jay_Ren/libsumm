﻿using Common.Utils;
using LibSumm.Entity;
using LibSumm.Features;
using LibSumm.Summarizer;
using LibSumm.Summarizer.Generator;
using LibSumm.Summarizer.Scorer;
using LibSumm.Summarizer.Similarity;
using LibSumm.Test;
using LibSumm.Utils;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.Providers.LinearAlgebra.Mkl;
using MSRA.NLC.Common.NLP;
using NLP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm
{
    class Program
    {
               
        static void Main(string[] args)
        {
            Control.UseSingleThread();
            Control.UseNativeMKL(MklConsistency.Auto, MklPrecision.Single, MklAccuracy.Low);
            Console.WriteLine(Control.LinearAlgebraProvider);

            LexRankTest.Test();

            Console.WriteLine("Input anything to exit");
            Console.ReadKey();
        }
    }
}
