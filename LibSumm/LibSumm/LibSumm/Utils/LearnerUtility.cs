﻿using LibSumm.Entity;
using LibSumm.Summarizer.Generator;
using LibSumm.Summarizer.Scorer;
using LibSumm.Utils;
using MSRA.NLC.Common.NLP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Utils
{
   public class LearnerUtility
    {
        private static Random rng = new Random();
        public static void Shuffle<T>(List<T> list) {
            for (int i = 0; i < list.Count; i++) {
               var j= rng.Next(0,list.Count);
                var item = list[i];
                list[i] = list[j];
                list[j] = item;
            }
        }

        public static void ExtractiveGroundTruth(Corpus corpus, StopWordHandler stop = null, int len = 150)
        {
            Dictionary<string, double> rep = new Dictionary<string, double>();
            var summSents = new List<Sentence>();
            foreach (var p in corpus.References) {
                summSents.AddRange(p.Value.Sentences);
            }
            foreach (Sentence s in summSents)
            {
                foreach (var t in s.Tokens)
                {
                    if (stop != null)
                    {
                        if (!stop.IsStopWord(t.STEM))
                        {
                            if (!rep.ContainsKey(t.STEM))
                            {
                                rep.Add(t.STEM, 1);
                            }
                            //else
                            //{
                            //    rep[t.STEM] += 1;
                            //}
                        }
                    }
                    else
                    {
                        if (!rep.ContainsKey(t.STEM))
                        {
                            rep.Add(t.STEM, 1);
                        }
                        //else
                        //{
                        //    rep[t.STEM] += 1;
                        //}
                    }

                }
            }

            RepresentationGenerator gen = new RepresentationGenerator(rep,stop, len);
            gen.Generate("GT",corpus,null);
        }

        public static void Normalize(List<Dictionary<string, float>> coll, Dictionary<string, float> max_coll, Dictionary<string, float> min_coll)
        {

            float low = 0.01f;
            float upper = 0.99f;

            foreach (var item in coll)
            {
                var keys = item.Keys.ToList();
                foreach (var id in keys)
                {
                    List<float> copy = new List<float>();
                    float max = max_coll[id];
                    float min = min_coll[id];

                    bool onpar = false;
                    if (max - min < 0.00000001)
                        onpar = true;

                    float score = 0;
                    if (onpar)
                    {
                        score = upper;
                    }
                    else
                    {
                        score = low + (upper - low) * (item[id] - min) / (max - min);
                    }

                    item[id] = score;
                }
            }
        }

        public static void Dev(string model,string label,List<Corpus> corpus, SummaryScorer scorer, SummaryGenerator generator)
        {
            string root = Path.Combine(Constant.Root, @"model\", model);
            if (!Directory.Exists(root))
                Directory.CreateDirectory(root);
            using (var sw = new StreamWriter(Path.Combine(root, "summary_"+ label + "_dev.txt")))
            {
                Parallel.For(0, corpus.Count, i =>
                {
                    generator.Generate(label + "_Dev", corpus[i], scorer);
                    lock (sw)
                    {
                        generator.Write2File(sw, corpus[i], corpus[i].PDSummaries[label + "_Dev"].Sentences);
                        sw.Flush();
                    }
                    Console.Write(".");
                });
            }

            RougeUtility.Evaluate(Path.Combine(root, "summary_" + label + "_dev.txt"));
        }

        public static void Test(string model, string label, List<Corpus> corpus, SummaryScorer scorer, SummaryGenerator generator)
        {
            string root = Path.Combine(Constant.Root, @"model\", model);
            if (!Directory.Exists(root))
                Directory.CreateDirectory(root);
            using (var sw = new StreamWriter(Path.Combine(root, "summary_" + label + "_test.txt")))
            {
                //Parallel.For(0, corpus.Count, i =>
                //{
                for (int i = 0; i < corpus.Count; i++)
                {
                    generator.Generate(label + "_Test", corpus[i], scorer);
                    lock (sw)
                    {
                        generator.Write2File(sw, corpus[i], corpus[i].PDSummaries[label + "_Test"].Sentences);
                        sw.Flush();
                    }
                    Console.Write(".");
                }
                //});
            }
            Console.WriteLine();

            RougeUtility.Silent = false;
            RougeUtility.Evaluate(Path.Combine(root, "summary_" + label + "_test.txt"));
        }


        public static void PatK(SentenceScorer model,List<Corpus> trainColl,List<Corpus> testColl)
        {
            Console.WriteLine(trainColl.Count);
            var bestList = new List<int>();
            foreach (Corpus corpus in trainColl)
            {
                var sents = new List<Sentence>();
                foreach (var doc in corpus.Documents)
                {
                    sents.AddRange(doc.Sentences);
                }
                foreach (Sentence sent in sents)
                {
                    sent.PDScore = model.Score(new List<Sentence>() { sent });
                }
                var best = sents.OrderByDescending(s => { return s.GTScore; }).ElementAt(0);
                var list = sents.OrderByDescending(s => { return s.PDScore; }).ToList();
                var bestRank = list.IndexOf(best) + 1;
                bestList.Add(bestRank);
                //Console.Write(bestRank+", ");
            }
            //Console.WriteLine();

            int top1 = bestList.Where(s => { return s <= 1; }).Count();
            int top5 = bestList.Where(s => { return s <= 5; }).Count();
            int top10 = bestList.Where(s => { return s <= 10; }).Count();
            int top50 = bestList.Where(s => { return s <= 50; }).Count();
            int top100 = bestList.Where(s => { return s <= 100; }).Count();
            Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", top1, top5, top10, top50, top100);


            Console.WriteLine(testColl.Count);
            bestList = new List<int>();
            foreach (Corpus corpus in testColl)
            {
                var sents = new List<Sentence>();
                foreach (var doc in corpus.Documents)
                {
                    sents.AddRange(doc.Sentences);
                }
                foreach (Sentence sent in sents)
                {
                    sent.PDScore = model.Score(new List<Sentence>() { sent });
                }
                var best = sents.OrderByDescending(s => { return s.GTScore; }).ElementAt(0);
                var list = sents.OrderByDescending(s => { return s.PDScore; }).ToList();
                var bestRank = list.IndexOf(best) + 1;
                bestList.Add(bestRank);
                //Console.Write(bestRank+", ");
            }
            //Console.WriteLine();

            top1 = bestList.Where(s => { return s <= 1; }).Count();
            top5 = bestList.Where(s => { return s <= 5; }).Count();
            top10 = bestList.Where(s => { return s <= 10; }).Count();
            top50 = bestList.Where(s => { return s <= 50; }).Count();
            top100 = bestList.Where(s => { return s <= 100; }).Count();
            Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}", top1, top5, top10, top50, top100);

        }
    }

}
