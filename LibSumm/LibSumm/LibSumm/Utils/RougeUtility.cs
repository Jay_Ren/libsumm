﻿using Common.Utils;
using LibSumm.Entity;
using LibSumm.Summarizer.Generator;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace LibSumm.Utils
{
    public class RougeUtility
    {
        public static bool Silent = false;
        private static object Lock=new object();

        public static Dictionary<string, double> Evaluate(string system,List<string> references) {
            Silent = true;
            var generator = new GreedySummaryGenerator(50);
            var tempfile=CommonUtils.MD5(system+DateTime.Now.Ticks);
            StreamWriter sw = new StreamWriter(tempfile);
            generator.Write2File(sw, system,references);
            sw.Close();

            var rouges = RougeUtility.Evaluate(tempfile);

            new Thread(new ThreadStart(() => {
                string folder = Path.GetDirectoryName(tempfile);
                string rougefolder = "rouge-" + Path.GetFileNameWithoutExtension(tempfile);
                folder = Path.Combine(folder, rougefolder);
                string[] files = Directory.GetFiles(folder);
                foreach (string file in files)
                {
                    File.Delete(file);
                }
                Directory.Delete(folder);

                File.Delete(tempfile);
            })).Start();

            return rouges;
        }

        public static Dictionary<string, double> Evaluate(string system, string current,List<string> references)
        {
            Silent = true;
            var generator = new GreedySummaryGenerator(50);
            var tempfile1 = CommonUtils.MD5(current + DateTime.Now.Ticks);
            StreamWriter sw = new StreamWriter(tempfile1);
            generator.Write2File(sw, current, references);
            sw.Close();
            var rouges1 = RougeUtility.Evaluate(tempfile1);
            
            var tempfile2 = CommonUtils.MD5(current+system + DateTime.Now.Ticks);
            sw = new StreamWriter(tempfile2);
            generator.Write2File(sw, current+" " + system, references);
            sw.Close();
            var rouges2 = RougeUtility.Evaluate(tempfile2);

            new Thread(new ThreadStart(()=> {
                string folder = Path.GetDirectoryName(tempfile1);
                string rougefolder = "rouge-" + Path.GetFileNameWithoutExtension(tempfile1);
                folder = Path.Combine(folder, rougefolder);
                string[] files = Directory.GetFiles(folder);
                foreach (string file in files)
                {
                    File.Delete(file);
                }
                Directory.Delete(folder);

                folder = Path.GetDirectoryName(tempfile2);
                rougefolder = "rouge-" + Path.GetFileNameWithoutExtension(tempfile2);
                folder = Path.Combine(folder, rougefolder);
                files = Directory.GetFiles(folder);
                foreach (string file in files)
                {
                    File.Delete(file);
                }
                Directory.Delete(folder);

                File.Delete(tempfile1);
                File.Delete(tempfile2);
            })).Start();
           

            Dictionary<string, double> dic = new Dictionary<string, double>();
            foreach (var p in rouges1) {
                dic.Add(p.Key,rouges2[p.Key]-p.Value);
            }

            return dic;
        }

        public static Dictionary<string, double> Evaluate(string file)
        {
            List<IList<string>> reference_list = new List<IList<string>>();
            List<string> system_list = new List<string>();

            bool delete = true;
            using (StreamReader reader = new StreamReader(file, Encoding.UTF8))
            {
                while (!reader.EndOfStream)
                {
                    string system = reader.ReadLine();
                    system_list.Add(system);

                    List<string> references = new List<string>();
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        if (string.IsNullOrWhiteSpace(line))
                        {
                            reference_list.Add(references);
                            references = new List<string>();

                            break;
                        }
                        else
                        {
                            references.Add(line);
                        }
                    }

                    if (references.Count > 0)
                        reference_list.Add(references);
                }
            }

            string folder = Path.GetDirectoryName(file);
            string rougefolder = "rouge-" + Path.GetFileNameWithoutExtension(file);
            Dictionary<string, double> result = Evaluate(folder, reference_list, system_list, delete, rougefolder);

            if (!Silent)
            {
                Console.WriteLine();
                foreach (var item in result)
                    Console.WriteLine("{0}\t{1}", item.Key, item.Value);
            }

            return result;
        }

        public static Dictionary<string, double> Evaluate(string folder, List<IList<string>> references_list, List<string> systems_list, bool delete, string rougefolder = "rougetemp")
        {
            folder = Path.Combine(folder, rougefolder);
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
            else
            {
                if (delete)
                {
                    string[] files = Directory.GetFiles(folder);
                    foreach (string file in files)
                    {
                        File.Delete(file);
                    }
                    Directory.CreateDirectory(folder);

                    if (!Silent) {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("[LOG]: DELETE FOLDER {0}", folder);
                        Console.ResetColor();
                    }
                 
                }
            }

            List<Dictionary<string, string>> model_list = new List<Dictionary<string, string>>();
            List<KeyValuePair<string, string>> peer_list = new List<KeyValuePair<string, string>>();

            int count = systems_list.Count;
            for (int id = 0; id < count; ++id)
            {
                var references = references_list[id];
                Dictionary<string, string> models = new Dictionary<string, string>();

                char begin = 'A';
                int current = 1;
                foreach (var reference in references)
                {
                    string name = string.Format("{0}_M_{1}", id, current);
                    string file = Path.Combine(folder, name);
                    models.Add(begin.ToString(), name);

                    using (StreamWriter writer = new StreamWriter(file))
                    {
                        writer.Write(reference);
                    }

                    begin++;
                    current++;
                }

                model_list.Add(models);

                string system = systems_list[id];
                string pname = string.Format("{0}_P_{1}", id, 1);
                string system_file = Path.Combine(folder, pname);
                using (StreamWriter writer = new StreamWriter(system_file))
                    writer.Write(system);

                peer_list.Add(new KeyValuePair<string, string>("1", pname));
            }

            string config_file = Path.Combine(folder, "rouge_config.xml");

            WriteRougeConfig(config_file, folder, model_list, peer_list);

            string result = Path.Combine(folder, "result.txt");

            string content = "";

            lock (Lock) {
                content = RunROUGE(config_file, result);
            }
           
            Dictionary<string, double> dict = Parse(content);

            //foreach (var item in dict)
            //{
            //    Console.WriteLine("{0}: {1}", item.Key, item.Value);
            //}

            return dict;
        }

        public static Dictionary<string, double> Parse(string content)
        {
            Dictionary<string, double> coll = new Dictionary<string, double>();

            using (StringReader reader = new StringReader(content))
            {
                string line = null;
                while ((line = reader.ReadLine()) != null)
                {
                    foreach (string key in dict.Keys)
                    {
                        if (line.IndexOf(key) != -1)
                        {
                            string[] items = line.Split(' ');
                            double score = double.Parse(items[3]);
                            coll.Add(dict[key], score);

                            break;
                        }
                    }
                }
            }

            return coll;
        }

        public static void WriteRougeConfig(string config_file, string folder, List<Dictionary<string, string>> model_list, List<KeyValuePair<string, string>> peer_list)
        {
            XmlDocument doc = new XmlDocument();

            XmlElement root = doc.CreateElement("ROUGE_EVAL");
            XmlAttribute rootAttr = doc.CreateAttribute("version");
            rootAttr.Value = "1.0";
            root.Attributes.Append(rootAttr);
            doc.AppendChild(root);

            int count = model_list.Count;

            for (int id = 0; id < count; ++id)
            {
                XmlElement evalRoot = doc.CreateElement("EVAL");
                XmlAttribute attr = doc.CreateAttribute("ID");
                attr.Value = (id + 1).ToString();
                evalRoot.Attributes.Append(attr);
                root.AppendChild(evalRoot);

                XmlElement elment = doc.CreateElement("PEER-ROOT");
                elment.InnerText = folder;
                evalRoot.AppendChild(elment);

                elment = doc.CreateElement("MODEL-ROOT");
                elment.InnerText = folder;
                evalRoot.AppendChild(elment);

                elment = doc.CreateElement("INPUT-FORMAT");
                attr = doc.CreateAttribute("TYPE");
                attr.Value = "SPL";
                elment.Attributes.Append(attr);
                evalRoot.AppendChild(elment);

                KeyValuePair<string, string> peer = peer_list[id];

                elment = doc.CreateElement("PEERS");
                XmlElement pelement = doc.CreateElement("P");
                attr = doc.CreateAttribute("ID");
                attr.Value = peer.Key;
                pelement.Attributes.Append(attr);
                pelement.InnerText = peer.Value;
                elment.AppendChild(pelement);
                evalRoot.AppendChild(elment);

                elment = doc.CreateElement("MODELS");

                Dictionary<string, string> models = model_list[id];
                foreach (var item in models)
                {
                    XmlElement melement = doc.CreateElement("M");
                    attr = doc.CreateAttribute("ID");
                    attr.Value = item.Key;
                    melement.Attributes.Append(attr);
                    melement.InnerText = item.Value;
                    elment.AppendChild(melement);
                }

                evalRoot.AppendChild(elment);
            }

            doc.Save(config_file);
        }

        public static string ROUGE_ROOT = Constant.ROUGE_ROOT;

        public static string PERL_ROOT = Constant.PERL_ROOT;

        public static Dictionary<string, string> dict = new Dictionary<string, string>()
            {
                {"ROUGE-1 Average_R:", "ROURGE_1_R"},
                {"ROUGE-1 Average_P:", "ROURGE_1_P"},
                {"ROUGE-1 Average_F:", "ROURGE_1_F"},
                {"ROUGE-2 Average_R:", "ROURGE_2_R"},
                {"ROUGE-2 Average_P:", "ROURGE_2_P"},
                {"ROUGE-2 Average_F:", "ROURGE_2_F"},
                {"ROUGE-SU4 Average_R:", "ROURGE_SU4_R"},
                {"ROUGE-SU4 Average_P:", "ROURGE_SU4_P"},
                {"ROUGE-SU4 Average_F:", "ROURGE_SU4_F"},
            };

        public static string LengthLimit = "";

        public static string RunROUGE(string config, string file = null)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();

            process.StartInfo.FileName = Path.Combine(PERL_ROOT, @"perl.exe");
            //string param = Path.Combine(ROUGE_ROOT, "ROUGE-1.5.5.pl") + LengthLimit + " -n 2 -e " + Path.Combine(ROUGE_ROOT, "data")
            //    + " -x -m -s -2 4 -u -c 95 -r 1000 -f A -p 0.5 -t 0 -a ";
            //string param = Path.Combine(ROUGE_ROOT, "ROUGE-1.5.5.pl") + LengthLimit + " -e " + Path.Combine(ROUGE_ROOT, "data")
            //    + " -n 4 -w 1.2 -m  -2 4 -u -c 95 -r 1000 -f A -p 0.5 -t 0 -a ";
            //string param = Path.Combine(ROUGE_ROOT, "ROUGE-1.5.5.pl") + LengthLimit + " -e " + Path.Combine(ROUGE_ROOT, "data")
            //    + " -n 4 -w 1.2 -2 -1 -u -c 95 -r 1000 -f A -p 0.5 -t 0 -m -a ";

            //-n 2 -m -u -c 95 -x -r 1000 -f A -p 0.5 -t 0
            //string param = Path.Combine(ROUGE_ROOT, "ROUGE-1.5.5.pl") + LengthLimit + " -e " + Path.Combine(ROUGE_ROOT, "data")
            //  + " -n 2 -m -u -c 95 -x -r 1000 -f A -p 0.5 -t 0 -a ";
            string param = Path.Combine(ROUGE_ROOT, "ROUGE-1.5.5.pl") + LengthLimit + " -e " + Path.Combine(ROUGE_ROOT, "data")
              + " -n 2 -m -2 4 -u -c 95 -x -r 1000 -f A -p 0.5 -t 0 -a ";
            
            if (!string.IsNullOrWhiteSpace(file))
                param += "-d ";

            param += "\"" + config + "\"";

            if(!Silent)
            Console.WriteLine(process.StartInfo.FileName + " " + param);

            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.UseShellExecute = false;

            process.StartInfo.Arguments = param;
            string result = null;
            try
            {
                process.Start();
                result = process.StandardOutput.ReadToEnd();
                //Console.WriteLine(result);

                process.WaitForExit();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: "+ex.Message);
            }

            if (!string.IsNullOrWhiteSpace(file))
            {
                using (StreamWriter writer = new StreamWriter(file))
                {
                    writer.WriteLine(result);
                }
            }

            return result;
        }
    }
}
