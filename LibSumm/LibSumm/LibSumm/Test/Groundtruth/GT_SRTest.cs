﻿using Common.Utils;
using LibSumm.Entity;
using LibSumm.Summarizer.Generator;
using LibSumm.Summarizer.Generator.Groundtruth;
using LibSumm.Summarizer.Scorer.Groundtruth;
using LibSumm.Utils;
using MSRA.NLC.Common.NLP;
using NLP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Test
{
   public class GT_SRTest
    {

        public static void Test()
        {
            string duc = "duc2004";
            //RougeUtility.LengthLimit = " -l 100 ";
            RougeUtility.LengthLimit = " -b 665 ";
            int len = 250;

            StopWordHandler stop = new StopWordHandler(Path.Combine(Constant.Root, @"model\stop.dict"));
            var stanfordnlpDir = Path.Combine(Constant.Root, @"model\stanford-corenlp-models");
            string stemmerFile = Path.Combine(Constant.Root, @"model\wordnet.dict");
            var nlp = new StanfordCoreNLPTool(stanfordnlpDir, stemmerFile, "tokenize, ssplit, pos, lemma");
            var reader = new FileCorpusReader(nlp);

            var analyzer = new CorpusAnalyzer(stop);
            var testColl = reader.Read(Path.Combine(Constant.Root, @"data\" + duc + ".txt"));
            foreach (var corpus in testColl)
            {
                analyzer.Analyze(corpus);
            }

            for (float k = 0; k < 1; k += 0.05f)
            {
                Console.WriteLine(duc + " k=" + k);
                var scorer = new GT_RASR();
                var generator = new ThresholdSummaryGenerator(len, k, 2, stop);
                LearnerUtility.Test("GT_SR", duc, testColl, scorer, generator);
            }

        }
    }
}
