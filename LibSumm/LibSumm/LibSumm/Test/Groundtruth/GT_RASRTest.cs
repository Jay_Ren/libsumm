﻿using Common.Utils;
using LibSumm.Entity;
using LibSumm.Summarizer.Generator;
using LibSumm.Summarizer.Generator.Groundtruth;
using LibSumm.Summarizer.Scorer.Groundtruth;
using LibSumm.Utils;
using MSRA.NLC.Common.NLP;
using NLP;
using ROUGEPackage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ROUGEPackage.Setting;

namespace LibSumm.Test
{
   public class GT_RASRTest
    {

        public static void Test()
        {
            string duc = "duc2004";
            RougeUtility.LengthLimit = " -b 665 ";
            LengthLimitType type = Setting.LengthLimitType.BYTE;
            int evallen = 665;
            int len = 250;

            StopWordHandler stop = new StopWordHandler(Path.Combine(Constant.Root, @"model\stop.dict"));
            var stanfordnlpDir = Path.Combine(Constant.Root, @"model\stanford-corenlp-models");
            string stemmerFile = Path.Combine(Constant.Root, @"model\wordnet.dict");
            var nlp = new StanfordCoreNLPTool(stanfordnlpDir, stemmerFile, "tokenize, ssplit, pos, lemma");
            var reader = new FileCorpusReader(nlp);

            var analyzer = new CorpusAnalyzer(stop);
            var testColl = reader.Read(Path.Combine(Constant.Root, @"data\" + duc + ".txt"));

            int[] ks = new int[] {1,5,10,15,20,25,30,35,40,45,50 };
            foreach (var k in ks) {
                Console.WriteLine(duc + " k=" + k);
                var scorer = new GT_RASR();
                var generator = new GT_RASRGenerator(k, len, type, evallen);
                foreach (var corpus in testColl)
                {
                    analyzer.Analyze(corpus);
                }
                LearnerUtility.Test("GT_RASR", duc, testColl, scorer, generator);
            }

        }
    }
}
