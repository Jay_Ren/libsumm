﻿using Common.Utils;
using LibSumm.Entity;
using LibSumm.Summarizer.Generator;
using LibSumm.Summarizer.Scorer;
using LibSumm.Summarizer.Similarity;
using LibSumm.Utils;
using MSRA.NLC.Common.NLP;
using NLP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Test
{
   public class MMRTest
    {

       public static void Test()
        {

            string duc = "duc2004";
            RougeUtility.LengthLimit = " -b 665 ";

            StopWordHandler stop = new StopWordHandler(Path.Combine(Constant.Root, @"model\stop.dict"));
            EmbeddingHolder embedding = new EmbeddingHolder(Path.Combine(Constant.Root, @"model\newsvec.300"));

            //var similarity = new EmbeddingCosineSentenceSimilarity(embedding);
            var similarity = new TFDFCosineSentenceSimilarity(true);

            var scorer = new MMR(similarity, 4f);
            var generator = new GreedySummaryGenerator(200);
            generator.LenDiscount = 0.3f;

            var stanfordnlpDir = Path.Combine(Constant.Root, @"model\stanford-corenlp-models");
            string stemmerFile = Path.Combine(Constant.Root, @"model\wordnet.dict");
            var nlp = new StanfordCoreNLPTool(stanfordnlpDir, stemmerFile, "tokenize, ssplit, pos, lemma");
            //var nlp = new MSRANLPTool(stemmerFile);
            var reader = new FileCorpusReader(nlp);

            var analyzer = new CorpusAnalyzer(stop);

            var testColl = reader.Read(Path.Combine(Constant.Root, @"data\" + duc + ".txt"));
            foreach (var corpus in testColl)
            {
                analyzer.Analyze(corpus);
            }
            LearnerUtility.Test("MMR", duc, testColl, scorer, generator);
        }
    }
}
