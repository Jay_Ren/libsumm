﻿using Common.Utils;
using LibSumm.Entity;
using LibSumm.Features;
using LibSumm.Summarizer.Generator;
using LibSumm.Summarizer.Scorer;
using LibSumm.Summarizer.Similarity;
using LibSumm.Utils;
using MSRA.NLC.Common.NLP;
using NLP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Test
{
   public class ASMTest
    {
        public static void Test()
        {
            string duc = "duc2007";
            //RougeUtility.LengthLimit = " -b 665 ";
            RougeUtility.LengthLimit = " -l 250 ";
            int len =300;

            StopWordHandler stop = new StopWordHandler(Path.Combine(Constant.Root, @"model\stop.dict"));
            EmbeddingHolder embedding = new EmbeddingHolder(Path.Combine(Constant.Root, @"model\glove.6B.50d.txt"));

            UniSentFeatureEngine featureEngine = new UniSentFeatureEngine();
            featureEngine.Add(new PositionFeature());
            featureEngine.Add(new LengthFeature());
            featureEngine.Add(new TFFeature(stop));
            featureEngine.Add(new DFFeature(stop));
            featureEngine.Add(new StopFeature(stop));

            featureEngine.Add(new TitleFeature(embedding, stop));
            featureEngine.Add(new QueryFeature(embedding, stop));
            //featureEngine.Add(new EmbeddingFeature(embedding));

            //var scorer = new ASM(embedding, featureEngine);
            var scorer = ASM.Load(Path.Combine(Constant.Root, "model/ASM/ASM." + duc + ".more.model"), featureEngine);
            var generator = new ThresholdSummaryGenerator(len, 0.5f, 2, stop);


            var stanfordnlpDir = Path.Combine(Constant.Root, @"model\stanford-corenlp-models");
            string stemmerFile = Path.Combine(Constant.Root, @"model\wordnet.dict");
            var nlp = new StanfordCoreNLPTool(stanfordnlpDir, stemmerFile, "tokenize, ssplit, pos, lemma");
            //var nlp = new MSRANLPTool(stemmerFile);
            var reader = new FileCorpusReader(nlp);

            var analyzer = new CorpusAnalyzer(stop);

            //var trainColl = reader.Read(Path.Combine(Constant.Root, @"data\train-" + duc + ".rouge2r.txt"));
            //foreach (var corpus in trainColl)
            //{
            //    analyzer.Analyze(corpus);
            //}

            var testColl = reader.Read(Path.Combine(Constant.Root, @"data\" + duc + ".rouge2r.txt"));
            foreach (var corpus in testColl)
            {
                analyzer.Analyze(corpus);
            }

            //for (int i = 1; i <= 50; i++)
            //{
            //    Console.WriteLine("i = " + i);
            //    scorer.test = false;
            //    scorer.Train(i + "", trainColl);
            //    scorer.test = true;
            //    LearnerUtility.Test("ASM", i + "", testColl, scorer, generator);
            //    //LearnerUtility.PatK(scorer, trainColl, testColl);
            //}


            for (float t = 0; t < 0.91; t += 0.05f)
            {
                Console.WriteLine("t = " + t);
                generator = new ThresholdSummaryGenerator(len, t, 2, stop);
                LearnerUtility.Test("ASM", duc+"test", testColl, scorer, generator);
            }

        }
    }
}
