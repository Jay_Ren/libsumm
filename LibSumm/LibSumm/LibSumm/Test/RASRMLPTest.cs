﻿using Common.Utils;
using LibSumm.Entity;
using LibSumm.Features;
using LibSumm.Summarizer.Generator;
using LibSumm.Summarizer.Scorer;
using LibSumm.Utils;
using MSRA.NLC.Common.NLP;
using NLP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Test
{
   public class RASRMLPTest
    {

        public static void Test()
        {
            string duc = "duc2004";
            RougeUtility.LengthLimit = " -b 665 ";
            int len = 300;

            StopWordHandler stop = new StopWordHandler(Path.Combine(Constant.Root, @"model\stop.dict"));
            EmbeddingHolder embedding = new EmbeddingHolder(Path.Combine(Constant.Root, @"model\newsvec.300"));

            UniSentFeatureEngine uniFeatureEngine = new UniSentFeatureEngine();
            uniFeatureEngine.Add(new PositionFeature());
            uniFeatureEngine.Add(new LengthFeature());
            uniFeatureEngine.Add(new TFFeature(stop));
            uniFeatureEngine.Add(new DFFeature(stop));
            uniFeatureEngine.Add(new EmbeddingFeature(embedding));

            BiSentsFeatureEngine biFeatureEngine = new BiSentsFeatureEngine();
            biFeatureEngine.Add(new SimilarityFeature(stop));
            biFeatureEngine.Add(new EmbeddingFeature(embedding));
            biFeatureEngine.Add(new TFFeature(stop));
            biFeatureEngine.Add(new DFFeature(stop));
            biFeatureEngine.Add(new StopFeature(stop));

            var scorer = new RASRMLP(uniFeatureEngine, biFeatureEngine);
            //var scorer = RASRMLP.Load(Path.Combine(Constant.Root, @"model\RASRMLP\RASRMLP_" + duc + ".model"), uniFeatureEngine, biFeatureEngine);
            //var similarity = new TFDFCosineSentenceSimilarity();
            //var generator = new ILPSummaryGenerator(200, similarity,0.1f);
            //var generator = new ThresholdSummaryGenerator(len, 0.4f, 2, stop);
            var generator = new GreedySummaryGenerator(len);

           var stanfordnlpDir = Path.Combine(Constant.Root, @"model\stanford-corenlp-models");
            string stemmerFile = Path.Combine(Constant.Root, @"model\wordnet.dict");
            var nlp = new StanfordCoreNLPTool(stanfordnlpDir, stemmerFile, "tokenize, ssplit, pos, lemma");
            //var nlp = new MSRANLPTool(stemmerFile);
            var reader = new FileCorpusReader(nlp);

            var analyzer = new CorpusAnalyzer(stop);

            var trainColl = reader.Read(Path.Combine(Constant.Root, @"data\train-" + duc + ".txt"));
            foreach (var corpus in trainColl)
            {
                analyzer.Analyze(corpus);
            }
           

            var testColl = reader.Read(Path.Combine(Constant.Root, @"data\" + duc + ".txt"));
            foreach (var corpus in testColl)
            {
                analyzer.Analyze(corpus);
            }

            for (int i = 1; i <= 200; i++)
            {
                Console.WriteLine("i= " + i);
                scorer.Train(duc+i, trainColl);
                LearnerUtility.Test("RASRMLP_"+i*5, duc, testColl, scorer, generator);
            }
        }
    }
}
