﻿using Common.Utils;
using LibSumm.Entity;
using LibSumm.Features;
using LibSumm.Summarizer.Generator;
using LibSumm.Summarizer.Scorer;
using LibSumm.Utils;
using MSRA.NLC.Common.NLP;
using NLP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Test
{
   public class  QF_SRMLPTest
    {

        public static void Test()
        {
            string duc = "duc2007";
            RougeUtility.LengthLimit = " -l 250 ";

            StopWordHandler stop = new StopWordHandler(Path.Combine(Constant.Root, @"model\stop.dict"));
            EmbeddingHolder embedding = new EmbeddingHolder(Path.Combine(Constant.Root, @"model\glove.6B.50d.txt"));

            UniSentFeatureEngine featureEngine = new UniSentFeatureEngine();
            featureEngine.Add(new PositionFeature());
            featureEngine.Add(new LengthFeature());
            featureEngine.Add(new TFFeature(stop));
            featureEngine.Add(new DFFeature(stop));
            featureEngine.Add(new StopFeature(stop));
            featureEngine.Add(new EmbeddingFeature(embedding));
            //featureEngine.Add(new QueryFeature(embedding, stop));
            //featureEngine.Add(new TitleFeature(embedding, stop));
            var scorer = new SRMLP(featureEngine);
            //var scorer = SRMLP.Load(Path.Combine(Constant.Root, @"model\SRMLP\SRMLP_" + duc + ".model"), featureEngine);
            //var similarity = new TFDFCosineSentenceSimilarity();
            //var generator = new ILPSummaryGenerator(200, similarity, 0.1f);
            var generator = new ThresholdSummaryGenerator(300, 0.5f, 2, stop);

            var stanfordnlpDir = Path.Combine(Constant.Root, @"model\stanford-corenlp-models");
            string stemmerFile = Path.Combine(Constant.Root, @"model\wordnet.dict");
            var nlp = new StanfordCoreNLPTool(stanfordnlpDir, stemmerFile, "tokenize, ssplit, pos, lemma");
            //var nlp = new MSRANLPTool(stemmerFile);
            var reader = new FileCorpusReader(nlp);

            var analyzer = new CorpusAnalyzer(stop);

            var trainColl = reader.Read(Path.Combine(Constant.Root, @"data\train-" + duc + ".rouge2r.txt"));
            foreach (var corpus in trainColl)
            {
                analyzer.Analyze(corpus);
            }

            var testColl = reader.Read(Path.Combine(Constant.Root, @"data\" + duc + ".rouge2r.txt"));
            foreach (var corpus in testColl)
            {
                analyzer.Analyze(corpus);
            }

            for (int i = 1; i <= 100; i++)
            {
                Console.WriteLine("i= " + i);
                scorer.Train(duc, trainColl);
                LearnerUtility.Test("SRMLP", duc, testColl, scorer, generator);
            }
        }
    }
}
