﻿using LibSumm.Entity;
using LibSumm.Summarizer.Generator;
using LibSumm.Summarizer.Scorer;
using LibSumm.Utils;
using MSRA.NLC.Common.NLP;
using NLP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Test
{
  public class LexRankTest
    {

       public static void Test()
        {
            string duc = "duc2001";
            //RougeUtility.LengthLimit = " -b 665 ";
            RougeUtility.LengthLimit = " -l 100 ";

            StopWordHandler stop = new StopWordHandler(Path.Combine(Constant.Root, @"model\stop.dict"));

            //var scorer = new LexRank();
            var scorer = new ContinuousLexRank();

            var stanfordnlpDir = Path.Combine(Constant.Root, @"model\stanford-corenlp-models");
            string stemmerFile = Path.Combine(Constant.Root, @"model\wordnet.dict");
            var nlp = new StanfordCoreNLPTool(stanfordnlpDir, stemmerFile, "tokenize, ssplit, pos, lemma");
            //var nlp = new MSRANLPTool(stemmerFile);
            var reader = new FileCorpusReader(nlp);

            var analyzer = new CorpusAnalyzer(stop);

            var testColl = reader.Read(Path.Combine(Constant.Root, @"data\" + duc + ".txt"));
            foreach (var corpus in testColl)
            {
                analyzer.Analyze(corpus);
            }
            for (float t = 0.1f; t < 0.9f; t += 0.05f) {
                Console.WriteLine("----------------------------------------------");
                Console.WriteLine("Threshold: "+t);
                var generator = new ThresholdSummaryGenerator(150, t, 2, stop);
                LearnerUtility.Test("LexRank", duc, testColl, scorer, generator);
            }
           
        }
    }
}
