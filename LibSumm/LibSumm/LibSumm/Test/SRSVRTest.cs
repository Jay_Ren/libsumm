﻿using Common.Utils;
using LibSumm.Entity;
using LibSumm.Features;
using LibSumm.Summarizer.Generator;
using LibSumm.Summarizer.Scorer;
using LibSumm.Utils;
using MSRA.NLC.Common.NLP;
using NLP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Test
{
   public class SRSVRTest
    {

        public static void Test()
        {

            string duc = "duc2004";
            RougeUtility.LengthLimit = " -b 665 ";

            StopWordHandler stop = new StopWordHandler(Path.Combine(Constant.Root, @"model\stop.dict"));
            EmbeddingHolder embedding = new EmbeddingHolder(Path.Combine(Constant.Root, @"model\newsvec.300"));

            UniSentFeatureEngine featureEngine = new UniSentFeatureEngine();
            featureEngine.Add(new PositionFeature());
            featureEngine.Add(new LengthFeature());
            featureEngine.Add(new TFFeature(stop));
            featureEngine.Add(new DFFeature(stop));
            featureEngine.Add(new EmbeddingFeature(embedding));
            var scorer = new SRSVR(featureEngine);
            //scorer.Load(Path.Combine(Constant.Root, @"model\SRSVR\SRSVR_" + duc + ".model"));
            //var similarity = new RandomSentenceSimilarity();
            //var generator = new ILPSummaryGenerator(250, similarity);
            var generator = new ThresholdSummaryGenerator(300, 0.5f, 2, stop);
            //var generator = new GreedySummaryGenerator(250);


            var stanfordnlpDir = Path.Combine(Constant.Root, @"model\stanford-corenlp-models");
            string stemmerFile = Path.Combine(Constant.Root, @"model\wordnet.dict");
            var nlp = new StanfordCoreNLPTool(stanfordnlpDir, stemmerFile, "tokenize, ssplit, pos, lemma");
            //var nlp = new MSRANLPTool(stemmerFile);
            var reader = new FileCorpusReader(nlp);

            var analyzer = new CorpusAnalyzer(stop);

            var trainColl = reader.Read(Path.Combine(Constant.Root, @"data\train-" + duc + ".txt"));
            foreach (var corpus in trainColl)
            {
                analyzer.Analyze(corpus);
            }
            scorer.Train(duc, trainColl);

            var testColl = reader.Read(Path.Combine(Constant.Root, @"data\" + duc + ".txt"));
            foreach (var corpus in testColl)
            {
                analyzer.Analyze(corpus);
            }
            LearnerUtility.Test("SRSVR", duc, testColl, scorer, generator);

        }
    }
}
