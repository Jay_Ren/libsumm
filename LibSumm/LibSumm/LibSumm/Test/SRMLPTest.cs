﻿using Common.Utils;
using LibSumm.Entity;
using LibSumm.Features;
using LibSumm.Summarizer.Generator;
using LibSumm.Summarizer.Scorer;
using LibSumm.Summarizer.Similarity;
using LibSumm.Utils;
using MSRA.NLC.Common.NLP;
using NLP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Test
{
   public class SRMLPTest
    {

        public static void Test()
        {
            string duc = "duc2001";
            //RougeUtility.LengthLimit = " -b 665 ";
            RougeUtility.LengthLimit = " -l 100 ";
            int len =150;

            StopWordHandler stop = new StopWordHandler(Path.Combine(Constant.Root, @"model\stop.dict"));
            EmbeddingHolder embedding = new EmbeddingHolder(Path.Combine(Constant.Root, @"model\glove.6B.50d.txt"));

            UniSentFeatureEngine featureEngine = new UniSentFeatureEngine();
            featureEngine.Add(new PositionFeature());
            featureEngine.Add(new LengthFeature());
            featureEngine.Add(new TFFeature(stop));
            featureEngine.Add(new DFFeature(stop));
            //featureEngine.Add(new StopFeature(stop));
            featureEngine.Add(new EmbeddingFeature(embedding));

            //featureEngine.Add(new TFContextFeature());
            //featureEngine.Add(new EmbeddingContextFeature(embedding));

            //featureEngine.Add(new CoverageFeature(embedding, stop));

            //featureEngine.Add(new NERFeature());
            var scorer = new SRMLP(featureEngine);
            //var scorer = SRMLP.Load(Path.Combine(Constant.Root, @"model\SRMLP\SRMLP_" + duc + ".model"), featureEngine);
            //var generator = new ILPSummaryGenerator(len, new TFDFCosineSentenceSimilarity(true),0.001f,20);
            var generator = new ThresholdSummaryGenerator(len, 0.5f, 2, stop);


            var stanfordnlpDir = Path.Combine(Constant.Root, @"model\stanford-corenlp-models");
            string stemmerFile = Path.Combine(Constant.Root, @"model\wordnet.dict");
            var nlp = new StanfordCoreNLPTool(stanfordnlpDir, stemmerFile, "tokenize, ssplit, pos, lemma");
            var reader = new FileCorpusReader(nlp);

            var analyzer = new CorpusAnalyzer(stop);

            var trainColl = reader.Read(Path.Combine(Constant.Root, @"data\train-" + duc + ".rouge2r.txt"));
            foreach (var corpus in trainColl)
            {
                analyzer.Analyze(corpus);
            }
          
            var testColl = reader.Read(Path.Combine(Constant.Root, @"data\" + duc + ".rouge2r.txt"));
            foreach (var corpus in testColl)
            {
                analyzer.Analyze(corpus);
            }

            for (int i = 1; i <= 50; i++) {
                Console.WriteLine("i = " + i);
                scorer.Train(i + "", trainColl);
                LearnerUtility.Test("SRMLP", i + "", testColl, scorer, generator);
                //LearnerUtility.PatK(scorer, trainColl, testColl);
            }

        }
    }
}
