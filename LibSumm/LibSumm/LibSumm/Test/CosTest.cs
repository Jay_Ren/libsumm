﻿using Common.Utils;
using LibSumm.Entity;
using LibSumm.Features;
using LibSumm.Summarizer.Generator;
using LibSumm.Summarizer.Scorer;
using LibSumm.Summarizer.Similarity;
using LibSumm.Utils;
using MSRA.NLC.Common.NLP;
using NLP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Test
{
   public class CosTest
    {

        public static void Test()
        {
            string duc = "duc2004";
            RougeUtility.LengthLimit = " -b 665 ";
            //RougeUtility.LengthLimit = " -l 100 ";
            int len = 200;

            StopWordHandler stop = new StopWordHandler(Path.Combine(Constant.Root, @"model\stop.dict"));
            EmbeddingHolder embedding = new EmbeddingHolder(Path.Combine(Constant.Root, @"model\newsvec.300"));

            var sim = new TFDFCosineSentenceSimilarity(true);
            var scorer = new Cos(sim);
            var generator = new ILPSummaryGenerator(len, sim,0.1f);
            //var generator = new ThresholdSummaryGenerator(len, 0.5f, 2, stop);


            var stanfordnlpDir = Path.Combine(Constant.Root, @"model\stanford-corenlp-models");
            string stemmerFile = Path.Combine(Constant.Root, @"model\wordnet.dict");
            var nlp = new StanfordCoreNLPTool(stanfordnlpDir, stemmerFile, "tokenize, ssplit, pos, lemma");
            //var nlp = new MSRANLPTool(stemmerFile);
            var reader = new FileCorpusReader(nlp);

            var analyzer = new CorpusAnalyzer(stop);

            var trainColl = reader.Read(Path.Combine(Constant.Root, @"data\train-" + duc + ".rouge2r.txt"));
            foreach (var corpus in trainColl)
            {
                analyzer.Analyze(corpus);
            }
          
            var testColl = reader.Read(Path.Combine(Constant.Root, @"data\" + duc + ".rouge2r.txt"));
            foreach (var corpus in testColl)
            {
                analyzer.Analyze(corpus);
            }

            LearnerUtility.Test("Cos", duc, testColl, scorer, generator);

        }
    }
}
