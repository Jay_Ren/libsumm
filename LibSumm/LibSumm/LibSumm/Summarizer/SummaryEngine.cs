﻿using LibSumm.Entity;
using LibSumm.Summarizer.Generator;
using LibSumm.Summarizer.Processor;
using LibSumm.Summarizer.Scorer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Summarizer
{
   public class SummaryEngine
    {
       private IProcessor beforeProcessor;
       private SummaryScorer scorer;
       private SummaryGenerator generator;
       private IProcessor afterProcessor;

        public SummaryEngine(SummaryScorer scorer,SummaryGenerator generator, IProcessor beforeProcessor=null, IProcessor afterProcessor=null)
        {
           this.scorer=scorer;
           this.generator=generator;
            this.beforeProcessor = beforeProcessor;
            this.afterProcessor = afterProcessor;
       }
       public virtual void Generate(string id,Corpus corpus){
            if (beforeProcessor != null)
                beforeProcessor.Process(corpus);
          generator.Generate(id,corpus,scorer);
            if (afterProcessor != null)
                afterProcessor.Process(corpus);
        }
    }
}
