﻿using LibSumm.Entity;
using LibSumm.Summarizer.Scorer;
using LibSumm.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Summarizer.Generator
{
   public class LazyGreedySummaryGenerator : SummaryGenerator
    {
        private int len;

        public LazyGreedySummaryGenerator(int len)
        {
            this.len = len;
        }

        //public override SummaryGenerator Copy()
        //{
        //    return new LazyGreedySummaryGenerator(this.len);
        //}

        public override void Generate(string id, Corpus corpus, SummaryScorer scorer)
        {

            var sents = PreProcess(corpus);
            //Parallel.ForEach(sents, sent => {
            //    sent.PDScore = scorer.Score(new List<Sentence>() { sent });
            //});
            foreach (var sent in sents)
            {
                sent.PDScore = scorer.Score(new List<Sentence>() { sent });
            }
            sents = sents.OrderByDescending(s => { return s.PDScore; }).ToList();

            int minLen = sents.Select(s => { return s.Len; }).Min();

            BinaryHeap heap = new BinaryHeap(false);
            foreach (Sentence sent in sents)
            {
                sent.PDScore = scorer.Score(new List<Sentence>() { sent });
                heap.Add(sent);
            }

            Summary summary = new Summary();
            summary.Sentences = new List<Sentence>();
            Sentence max = heap.Remove();
            summary.Sentences.Add(max);
            int curLen = max.Len;
            float lastScore = max.PDScore;

            while (len-curLen>= minLen && heap.Count > 0)
            {
                Sentence next = null;
                Sentence s = heap.Remove();
                while (true)
                {
                    summary.Sentences.Add(s);
                    float score = scorer.Score(summary.Sentences);
                    summary.Sentences.Remove(s);

                    s.PDScore = score - lastScore;

                    next = s;
                    heap.Add(s);
                    s = heap.Remove();

                    //||Math.Abs(next.PDScore-s.PDScore)<1E-8
                    if (next.Equals(s)||Math.Abs(next.PDScore - s.PDScore) < 1E-8)
                    {
                        next = s;
                        lastScore = score;
                        break;
                    }
                }
                if (next != null)
                {
                    summary.Sentences.Add(next);
                   
                    curLen = summary.Sentences.Select(ss => { return ss.Len; }).Sum();
                    summary.PDScore = lastScore;
                    next.Rank = summary.Sentences.Count;
                }
            }

            lock (corpus.Lock)
            {
                if (corpus.PDSummaries == null)
                {
                    corpus.PDSummaries = new Dictionary<string, Summary>();
                }

                if (!corpus.PDSummaries.ContainsKey(id))
                    corpus.PDSummaries.Add(id, summary);
                else
                    corpus.PDSummaries[id] = summary;
            }
        }
    }
}
