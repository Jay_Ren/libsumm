﻿using LibSumm.Entity;
using LibSumm.Summarizer.Scorer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Summarizer.Generator
{
   public class GreedySummaryGenerator : SummaryGenerator
    {

       private int len;

       public GreedySummaryGenerator(int len) {
           this.len = len;
       }

        public float LenDiscount { get; set; }

        //public override SummaryGenerator Copy() {
        //    return new GreedySummaryGenerator(this.len);
        //}

        public override void Generate(string id, Corpus corpus, SummaryScorer scorer)
       {
            var sents = PreProcess(corpus);
            //Parallel.ForEach(sents, sent => {
            //    sent.PDScore = scorer.Score(new List<Sentence>() { sent });
            //});
            foreach (var sent in sents)
            {
                sent.PDScore = scorer.Score(new List<Sentence>() { sent });
                if (LenDiscount > 0)
                {
                    sent.PDScore = sent.PDScore / (float)Math.Pow(sent.Len, LenDiscount);
                }
            }
            sents = sents.OrderByDescending(s => { return s.PDScore; }).ToList();

            Summary summ = new Summary();
           summ.Sentences = new List<Sentence>();

           int clen = 0;
           float lastScore = 0;
           while (clen < len&&sents.Count>0) {
               float max = -1;
                float lscore = -1;
               Sentence next = null;
               foreach (var sent in sents)
               {
                   summ.Sentences.Add(sent);
                   float score = scorer.Score(summ.Sentences);

                    float NScore = score;
                    if (LenDiscount > 0) {
                        NScore = (NScore - lastScore) / (float)Math.Pow(sent.Len,LenDiscount);
                    }
                   if (NScore > max)
                   {
                       max = NScore;
                       lscore = score;
                       next = sent;
                   }
                   summ.Sentences.Remove(sent);
               }
               if (next != null) {
                    lastScore = lscore;
                    summ.Sentences.Add(next);
                    summ.PDScore = lastScore;
                    next.Rank = summ.Sentences.Count;
                    clen += next.Len;
                    sents.Remove(next);
               }
           }

            lock (corpus.Lock)
            {
                if (corpus.PDSummaries == null)
                {
                    corpus.PDSummaries = new Dictionary<string, Summary>();
                }

                if (!corpus.PDSummaries.ContainsKey(id))
                    corpus.PDSummaries.Add(id, summ);
                else
                    corpus.PDSummaries[id] = summ;
            }
            
       }
    }
}
