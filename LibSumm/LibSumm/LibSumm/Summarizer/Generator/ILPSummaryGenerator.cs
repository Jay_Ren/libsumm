﻿using LibSumm.Entity;
using LibSumm.Summarizer.Scorer;
using LibSumm.Summarizer.Similarity;
using LpSolveMilpManager.Implementation;
using MilpManager.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Summarizer.Generator
{
    /// <summary>
    /// McDonald, R. (2007). A study of global inference algorithms in multi-document summarization.
    /// </summary>
    public class ILPSummaryGenerator : SummaryGenerator
    {
        private int len;
        private ISentenceSimilarity similarity;
        private float lambda = 0.1f;
        private int n;
        public ILPSummaryGenerator(int len, ISentenceSimilarity similarity, float lambda = 0.1f,int n=50)
        {
            this.len = len;
            this.similarity = similarity;
            this.lambda = lambda;
            this.n = n;
        }

        //public override SummaryGenerator Copy()
        //{
        //    return new ILPSummaryGenerator(this.len,this.similarity,this.lambda);
        //}

        public override void Generate(string id, Corpus corpus, SummaryScorer scorer)
        {
            var sents = PreProcess(corpus);
            //Parallel.ForEach(sents, sent => {
            //    sent.PDScore = scorer.Score(new List<Sentence>() { sent });
            //});
            foreach (var sent in sents)
            {
                sent.PDScore = scorer.Score(new List<Sentence>() { sent });
            }
            sents = sents.OrderByDescending(s => { return s.PDScore; }).Take(n).ToList();

            var solver = new LpSolveMilpSolver(10);

            var sentIndicators = new IVariable[sents.Count];
            var sentImps = new IVariable[sents.Count];
            var sentLens = new IVariable[sents.Count];
            var sentSims= new IVariable[sents.Count][];
            var sentSimIndicators = new IVariable[sents.Count][];
            for (int i = 0; i < sents.Count; i++)
            {
                sentIndicators[i] = solver.Create("indicator-" + sents[i].ID, Domain.BinaryInteger);
                sentImps[i] = solver.FromConstant(sents[i].PDScore, Domain.AnyConstantReal);
                sentLens[i]= solver.FromConstant(sents[i].Len, Domain.AnyConstantInteger);
                sentSims[i] = new IVariable[sents.Count];
                sentSimIndicators[i]= new IVariable[sents.Count];
                for (int j = 0; j < sents.Count; j++)
                {
                    sentSimIndicators[i][j] = solver.Create("indicator-" + sents[i].ID+"-"+ sents[j].ID, Domain.BinaryInteger);
                }
                for (int j = i + 1; j < sents.Count; j++)
                {
                    sentSims[i][j] = solver.FromConstant(similarity.Similarity(sents[i], sents[j]), Domain.AnyConstantReal);
                }
            }

            var imp = solver.FromConstant(0, Domain.AnyReal);
            var lenConstraints = solver.FromConstant(0, Domain.AnyInteger);
            for (int i = 0; i < sents.Count; i++) {
                imp = solver.Operation(OperationType.Addition,imp, solver.Operation(OperationType.Multiplication, sentImps[i], sentIndicators[i]));
                lenConstraints = solver.Operation(OperationType.Addition, lenConstraints, solver.Operation(OperationType.Multiplication, sentLens[i], sentIndicators[i]));
            }
            solver.Set(ConstraintType.LessOrEqual, lenConstraints, solver.FromConstant(len+10, Domain.AnyConstantInteger));
            solver.Set(ConstraintType.LessOrEqual, solver.FromConstant(len-10, Domain.AnyConstantInteger), lenConstraints);

            var sim = solver.FromConstant(0, Domain.AnyReal);
            int count = 0;
            for (int i = 0; i < sents.Count; i++)
            {
                for (int j = i + 1; j < sents.Count; j++)
                {
                    sim = solver.Operation(OperationType.Addition, sim, solver.Operation(OperationType.Multiplication, sentSims[i][j], sentSimIndicators[i][j]));
                    solver.Set(ConstraintType.LessOrEqual, sentSimIndicators[i][j], sentIndicators[i]);
                    solver.Set(ConstraintType.LessOrEqual, sentSimIndicators[i][j], sentIndicators[j]);
                    solver.Set(ConstraintType.LessOrEqual, solver.Operation(OperationType.Subtraction, solver.Operation(OperationType.Addition, sentIndicators[i], sentIndicators[j]), sentSimIndicators[i][j]), solver.FromConstant(1, Domain.AnyConstantInteger));
                    count++;
                }
            }
            sim=solver.Operation(OperationType.Multiplication, sim, solver.FromConstant(1.0f/count, Domain.AnyConstantReal));

            var goal = solver.Operation(OperationType.Subtraction, imp, solver.Operation(OperationType.Multiplication, solver.FromConstant(lambda,Domain.AnyConstantReal),sim));
            solver.AddGoal("McDonald_ILP_Goal", goal);

            solver.Solve();

            Console.WriteLine("Len: {0} Goal: {1}", solver.GetValue(lenConstraints), solver.GetValue(goal));

            Summary summ = new Summary();
            summ.Sentences = new List<Sentence>();

            for (int i = 0; i < sents.Count; i++)
            {
                if (solver.GetValue(sentIndicators[i]) == 1) {
                    summ.Sentences.Add(sents[i]);
                    summ.PDScore = sents[i].PDScore;
                }
            }

            summ.Sentences = summ.Sentences.OrderByDescending(s => { return s.PDScore; }).ToList();

            lock (corpus.Lock)
            {
                if (corpus.PDSummaries == null)
                {
                    corpus.PDSummaries = new Dictionary<string, Summary>();
                }

                if (!corpus.PDSummaries.ContainsKey(id))
                    corpus.PDSummaries.Add(id, summ);
                else
                    corpus.PDSummaries[id] = summ;

            }
        }
    }
}
