﻿using Common.Utils;
using LibSumm.Entity;
using LibSumm.Summarizer.Scorer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Summarizer.Generator
{
   public abstract class SummaryGenerator
    {

        //public abstract SummaryGenerator Copy();

        public void Write2File(StreamWriter sw, string system, List<string> references)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(system);
            sb.Append("\r\n");
            foreach (var refsumm in references)
            {
                sb.Append(refsumm);
                sb.Append("\r\n");
            }
            sb.Append("\r\n");
            sw.Write(sb.ToString().Replace("  ", " ").Replace("  ", " "));
            sw.Flush();
        }

        public void Write2File(StreamWriter sw, Corpus corpus, List<Sentence> summ)
        {
            StringBuilder sb = new StringBuilder();
            foreach (Sentence sent in summ)
            {
                sb.Append(sent.Content + " ");
            }
            sb.Append("\r\n");
            foreach (var pair in corpus.References)
            {
                foreach (Sentence sent in pair.Value.Sentences)
                {
                    sb.Append(sent.Content + " ");
                }
                sb.Append("\r\n");
            }
            sb.Append("\r\n");
            sw.Write(sb.ToString().Replace("  ", " ").Replace("  ", " "));
            sw.Flush();
        }
        public abstract void Generate(string id,Corpus corpus,SummaryScorer scorer);

       protected List<Sentence> PreProcess(Corpus corpus)
       {
           List<Sentence> sents = new List<Sentence>();
           foreach (var doc in corpus.Documents) {
               foreach (var sent in doc.Sentences) {
                   if (sent.Len < 5) {
                       continue;
                   }
                   if (!CommonUtils.ContainsCharacter(sent.Content)) {
                       continue;
                   }
                   sents.Add(sent);
               }
           }
           return sents;
       }
    }
}
