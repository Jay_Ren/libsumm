﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibSumm.Entity;
using LibSumm.Summarizer.Scorer;
using ROUGEPackage;
using static ROUGEPackage.Setting;
using LibSumm.Utils;

namespace LibSumm.Summarizer.Generator.Groundtruth
{
    public class GT_RASRGenerator : SummaryGenerator
    {
        private int k;
        private ROUGEUtils rouge;
        private int len;
        public GT_RASRGenerator(int k,int len, LengthLimitType type,int evallen)
        {
            this.k = k;
            this.len = len;
            Setting setting = new Setting();
            setting.NgramSize = 2;
            setting.CurrentLengthType = type;
            setting.SummaryLength = evallen;

            rouge = new ROUGEUtils(setting);
        }

        public override void Generate(string id, Corpus corpus, SummaryScorer scorer)
        {
            var sents_ = PreProcess(corpus);

            var references = new List<string>();
            foreach (var summary in corpus.References.Values)
            {
                references.Add(string.Join(" ", summary.Sentences.Select(s => { return s.Content; })));
            }

            foreach (var sent in sents_)
            {
                sent.GTScore = (float)rouge.Recall(sent.Content, references)[1];
                //sent.GTScore = (float)RougeUtility.Evaluate(sent.Content,references)["ROURGE_2_R"];
            }

            sents_ =sents_.OrderByDescending(s=> { return s.GTScore; }).ToList();

            Parallel.For(0, k, i =>
            {
                var idi = id + i;
                var sents = new List<Sentence>(sents_);

                Summary summ = new Summary();
                summ.Sentences = new List<Sentence>();
                summ.Sentences.Add(sents[i]);

                int clen = sents[i].Len;
                sents.Remove(sents[i]);

                while (clen < len && sents.Count > 0)
                {
                    float max = -1;
                    float lscore = -1;
                    Sentence next = null;
                    foreach (var sent in sents)
                    {
                        summ.Sentences.Add(sent);
                        float score = scorer.Score(summ.Sentences);

                        float NScore = score;
                        if (NScore > max)
                        {
                            max = NScore;
                            lscore = score;
                            next = sent;
                        }
                        summ.Sentences.Remove(sent);
                    }
                    if (next != null)
                    {
                        summ.Sentences.Add(next);
                        summ.PDScore = lscore;
                        next.Rank = summ.Sentences.Count;
                        clen += next.Len;
                        sents.Remove(next);
                    }
                }

                lock (corpus.Lock)
                {
                    if (corpus.PDSummaries == null)
                    {
                        corpus.PDSummaries = new Dictionary<string, Summary>();
                    }

                    if (!corpus.PDSummaries.ContainsKey(idi))
                        corpus.PDSummaries.Add(idi, summ);
                    else
                        corpus.PDSummaries[idi] = summ;
                }
            });


            float fmax = float.MinValue;
            Summary fsum = null;
            foreach (var summ in corpus.PDSummaries) {
              var summstr=  string.Join(" ",summ.Value.Sentences.Select(s=> { return s.Content; }));
              //var score =  (float)rouge.Recall(summstr, references)[1];
              var score= (float)RougeUtility.Evaluate(summstr, references)["ROURGE_2_R"];
                if (score > fmax) {
                    fmax = score;
                    fsum = summ.Value;
                }
            }

            lock (corpus.Lock)
            {
                if (corpus.PDSummaries == null)
                {
                    corpus.PDSummaries = new Dictionary<string, Summary>();
                }

                if (!corpus.PDSummaries.ContainsKey(id))
                    corpus.PDSummaries.Add(id, fsum);
                else
                    corpus.PDSummaries[id] = fsum;
            }
            
        }
    }
}
