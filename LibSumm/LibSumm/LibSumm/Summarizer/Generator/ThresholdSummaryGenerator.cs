﻿using LibSumm.Entity;
using LibSumm.Summarizer.Scorer;
using MSRA.NLC.Common.NLP;
using NLP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Summarizer.Generator
{
   public class ThresholdSummaryGenerator: SummaryGenerator
    {
        private StopWordHandler stop;
        private int len;
        private int N=200;
        private int ngram;
        private float threshold;

       public ThresholdSummaryGenerator(int len,float threshold, int ngram,string stopFile) {
           this.len = len;
            this.stop = new StopWordHandler(stopFile);
            this.ngram = ngram;
            this.threshold = threshold;
       }

        //public override SummaryGenerator Copy()
        //{
        //    return new ThresholdSummaryGenerator(this.len, this.threshold, this.ngram,this.stop);
        //}

        public ThresholdSummaryGenerator(int len, float threshold, int ngram, StopWordHandler stop)
        {
            this.len = len;
            this.stop = stop;
            this.ngram = ngram;
            this.threshold = threshold;
        }
        public override void Generate(string id, Corpus corpus, SummaryScorer scorer)
       {
            NgramRedundancyChecker checker = new NgramRedundancyChecker(stop, ngram, threshold);
            var sents = PreProcess(corpus);
            //Parallel.ForEach(sents,sent=> {
            //    sent.PDScore = scorer.Score(new List<Sentence>() { sent });
            //});
            foreach (var sent in sents)
            {
                sent.PDScore = scorer.Score(new List<Sentence>() { sent });
            }

            sents = sents.OrderByDescending(s => { return s.PDScore; }).Take(N).ToList();

           Summary summ = new Summary();
           summ.Sentences=new List<Sentence>();

           int clen = 0;
           for(int i=0;i<sents.Count;i++)
           {
               var sent = sents[i];
               string[] tokens = sent.Tokens.Select(x => x.LOWER).ToArray();
               if (checker != null && checker.IsRedendant(tokens))
               {
                   continue;
               }
               else
               {
                   summ.Sentences.Add(sent);
                   summ.PDScore += sent.PDScore;
                   sent.Rank = summ.Sentences.Count;
                   checker.AddToSummary(tokens);
                   clen += sent.Len;
               }
               if (clen >= len) {
                   break;
               }
           }
         
            lock (corpus.Lock)
            {
                if (corpus.PDSummaries == null)
                {
                    corpus.PDSummaries = new Dictionary<string, Summary>();
                }

                if (!corpus.PDSummaries.ContainsKey(id))
                    corpus.PDSummaries.Add(id, summ);
                else
                    corpus.PDSummaries[id] = summ;
            }

       }
    }
}
