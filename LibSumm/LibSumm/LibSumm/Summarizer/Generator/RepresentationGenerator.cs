﻿using LibSumm.Entity;
using LibSumm.Summarizer.Scorer;
using MSRA.NLC.Common.NLP;
using NLP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Summarizer.Generator
{
    public class RepresentationGenerator : SummaryGenerator
    {
        public override void Generate(string id, Corpus corpus, SummaryScorer scorer)
        {
            if (SummaryRepresentation == null)
            {
                throw new Exception("Set Corresponding Summary Representation First");
            }

            Summary summary = new Summary();
            List<Sentence> summ = new List<Sentence>();
            summary.Sentences = summ;

            var candidates = new List<Sentence>();

            foreach (var doc in corpus.Documents) {
                candidates.AddRange(doc.Sentences);
            }

            int count = candidates.Count;

            int len = 0;
            do
            {
                double max = -1;
                Sentence next = null;
                for (int i = 0; i < count; ++i)
                {

                    string[] tokens = candidates[i].Tokens.Select(x => x.LOWER).ToArray();
                    if (checker != null)
                    {
                        if (checker.IsRedendant(tokens))
                        {
                            continue;
                        }
                    }

                    summ.Add(candidates[i]);

                    Dictionary<string, double> sentRepresentation = new Dictionary<string, double>();
                    foreach (Sentence sent in summ)
                    {
                        foreach (var token in sent.Tokens)
                        {
                            if (stop != null)
                            {
                                if (!stop.IsStopWord(token.LOWER))
                                {
                                    if (!sentRepresentation.ContainsKey(token.STEM))
                                    {
                                        sentRepresentation[token.STEM] = 1.0;
                                    }
                                    //else
                                    //{
                                    //    sentRepresentation[token.Stem] += 1.0;
                                    //}
                                }
                            }
                            else {
                                if (!sentRepresentation.ContainsKey(token.STEM))
                                {
                                    sentRepresentation[token.STEM] = 1.0;
                                }
                                //else
                                //{
                                //    sentRepresentation[token.Stem] += 1.0;
                                //}
                            }
                        }
                    }

                    double score = MSRA.NLC.Common.NLP.Similarity.Consine(sentRepresentation, SummaryRepresentation);
                    if (score > max)
                    {
                        max = score;
                        next = candidates[i];
                    }

                    summ.Remove(candidates[i]);
                }

                if (next != null)
                {
                    summ.Add(next);
                    string[] tokens = next.Tokens.Select(x => x.LOWER).ToArray();
                    if (checker != null)
                    {
                        checker.AddToSummary(tokens);
                    }
                }

                len = summ.Select(s => { return s.Len; }).Sum();
            } while (len < this.len);

            lock (corpus.Lock)
            {
                if (corpus.PDSummaries == null)
                {
                    corpus.PDSummaries = new Dictionary<string, Summary>();
                }

                if (!corpus.PDSummaries.ContainsKey(id))
                    corpus.PDSummaries.Add(id, summary);
                else
                    corpus.PDSummaries[id] = summary;
            }
        }


        public RepresentationGenerator(Dictionary<string, double> SummaryRepresentation,StopWordHandler handler,int len=150)
        {
            this.stop = handler;
            if (handler != null)
                checker = new NgramRedundancyChecker(handler, 2, 0.7);
            this.len = len;
            this.SummaryRepresentation = SummaryRepresentation;
        }
        private NgramRedundancyChecker checker = null;
        private Dictionary<string, double> SummaryRepresentation { get; set; }
        private StopWordHandler stop = null;
        private int len = 150;
    }
}
