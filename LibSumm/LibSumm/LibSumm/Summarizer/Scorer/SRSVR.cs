﻿using de.bwaldvogel.liblinear;
using LibSumm.Entity;
using LibSumm.Features;
using LibSumm.Summarizer.Generator;
using LibSumm.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Summarizer.Scorer
{
    /// <summary>
    /// (Tested) Sentence Regression with SVR implementation
    /// </summary>
    [Serializable]
   public class SRSVR: SentenceScorer
    {
        private Model model;
        private Parameter param;
        private UniSentFeatureEngine featureEngine;
        //private Dictionary<string, float> max_coll;
        //private Dictionary<string, float> min_coll;


        public SRSVR(UniSentFeatureEngine featureEngine,double C =1,double eps=1E-5) {
            this.featureEngine = featureEngine;
            this.param = new Parameter(SolverType.L2R_L2LOSS_SVR, C, eps);
        }

        public void Load(string modelFile) {
            this.model = Model.load(new java.io.File(modelFile));
           
            //if (maxFile != null && minFile != null) {
            //    this.max_coll = new Dictionary<string, float>();
            //    this.min_coll = new Dictionary<string, float>();
            //    var lines=File.ReadAllLines(maxFile);
            //    foreach (var line in lines) {
            //        var temp = line.Split(new char[] { ' '},StringSplitOptions.None);
            //        this.max_coll.Add(temp[0],float.Parse(temp[1]));
            //    }
            //    lines = File.ReadAllLines(minFile);
            //    foreach (var line in lines)
            //    {
            //        var temp = line.Split(new char[] { ' ' }, StringSplitOptions.None);
            //        this.min_coll.Add(temp[0], float.Parse(temp[1]));
            //    }
            //}
           
        }

        public void Train(string label,List<Corpus> corpus) {

            var modelName = this.GetType().Name;
            string root = Path.Combine(Constant.Root, @"model\", modelName);
            if (!Directory.Exists(root))
                Directory.CreateDirectory(root);

            var dic = featureEngine.FeatureMap();
            using (var sw = new StreamWriter(Path.Combine(root,"feature_"+ label + "_list.txt"))) {
                foreach (var pair in dic) {
                    sw.WriteLine("{0} {1}",pair.Key,pair.Value);
                }
            }


            int count = 0;
            List<float> scores = new List<float>();
            List<Dictionary<string, float>> feature_coll = new List<Dictionary<string, float>>();
            //max_coll = new Dictionary<string, float>();
            //min_coll = new Dictionary<string, float>();
            Parallel.For(0, corpus.Count, i => {
                foreach (var doc in corpus[i].Documents)
                {
                    var sents = doc.Sentences;
                    foreach (var sent in sents)
                    {
                        Dictionary<string, float> features = featureEngine.Extract(sent);
                        //foreach (var item in features)
                        //{
                        //    lock (max_coll)
                        //    {
                        //        if (!max_coll.ContainsKey(item.Key))
                        //            max_coll.Add(item.Key, item.Value);
                        //        else
                        //        {
                        //            if (max_coll[item.Key] < item.Value)
                        //                max_coll[item.Key] = item.Value;
                        //        }
                        //    }

                        //    lock (min_coll)
                        //    {
                        //        if (!min_coll.ContainsKey(item.Key))
                        //            min_coll.Add(item.Key, item.Value);
                        //        else
                        //        {
                        //            if (min_coll[item.Key] > item.Value)
                        //                min_coll[item.Key] = item.Value;
                        //        }
                        //    }

                        //}
                        lock (feature_coll) {
                            feature_coll.Add(features);
                            scores.Add(sent.GTScore);
                            count++;
                            if (count % 100 == 0)
                            {
                                Console.Write("[Log] Extract Features {0}\r", count);
                            }
                        }
                    }
                }
            });
            Console.WriteLine();


            //LearnerUtility.Normalize(feature_coll, max_coll, min_coll);

            //using (var sw = new StreamWriter(Path.Combine(root, "feature_" + label + "_max.txt")))
            //{
            //    foreach (var pair in max_coll)
            //    {
            //        sw.WriteLine("{0} {1}", pair.Key, pair.Value);
            //    }
            //}

            //using (var sw = new StreamWriter(Path.Combine(root, "feature_" + label + "_min.txt")))
            //{
            //    foreach (var pair in min_coll)
            //    {
            //        sw.WriteLine("{0} {1}", pair.Key, pair.Value);
            //    }
            //}

            using (var sw = new StreamWriter(Path.Combine(root, "feature_"+label+"_value.txt")))
            {
                for (int i = 0; i < feature_coll.Count; i++) {
                    sw.WriteLine("{0} {1}", scores[i], featureEngine.Format2String(feature_coll[i]));
                }
            }
            Problem prob = Problem.readFromFile(new java.io.File(Path.Combine(root, "feature_" + label + "_value.txt")),-1);

            model=Linear.train(prob,param);

            string modelFilePath = Path.Combine(root, modelName+"_" +label+".model");

            model.save(new java.io.File(modelFilePath));
        }

        public override float Score(Sentence sent, List<Sentence> currentSummary) {
            //float low = 0.01f;
            //float upper = 0.99f;

            var temp = featureEngine.Extract(sent);
            //if (max_coll != null && min_coll != null)
            //{
            //    var keys = new HashSet<string>(temp.Keys);
            //    foreach (var key in keys)
            //    {
            //        temp[key] = low + (upper - low) * (temp[key] - min_coll[key]) / (max_coll[key] - min_coll[key]);
            //    }
            //}
            var features= featureEngine.Format2Array(temp);
            FeatureNode[] nodes = new FeatureNode[features.Length];
            for (int i = 0; i < nodes.Length; i++) {
                nodes[i] = new FeatureNode(i + 1, features[i]);
            }
            return (float)Linear.predict(model, nodes);
        }
    }
}
