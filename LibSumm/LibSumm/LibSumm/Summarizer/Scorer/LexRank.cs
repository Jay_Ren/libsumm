﻿using Common.Utils;
using LibSumm.Entity;
using MathNet.Numerics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Summarizer.Scorer
{
   public class LexRank : SentenceScorer
    {

        
        public override float Score(Sentence sent_, List<Sentence> currentSummary)
        {
            var modelName = this.GetType().Name;
            lock (sent_.Lock) {
                if (sent_.PDScores != null && sent_.PDScores.ContainsKey(modelName))
                {
                    return sent_.PDScores[modelName];
                }
            }
            

            Corpus corpus = sent_.Doc.Corpus;
            Dictionary<string, int> tf = corpus.TF;

            List<Sentence> sents = new List<Sentence>();
            List<double> priorList = new List<double>();


            Dictionary<Sentence, Dictionary<string, double>> vecs = new Dictionary<Sentence, Dictionary<string, double>>();
            foreach (var doc in corpus.Documents)
            {
                if (doc.Title != null)
                {
                    vecs.Add(doc.Title, GraphRanker.Convert(doc.Title, tf, null));
                }
                foreach (var sent in doc.Sentences)
                {
                    vecs.Add(sent, GraphRanker.Convert(sent, tf, null));
                }
            }

            foreach (var doc in corpus.Documents)
            {
                foreach (var sent in doc.Sentences)
                {
                    if (vecs.ContainsKey(sent))
                    {
                        sents.Add(sent);
                        if (doc.Title != null)
                        {
                            double sim = MSRA.NLC.Common.NLP.Similarity.Consine(vecs[sent], GraphRanker.Convert(doc.Title, tf, null));
                            priorList.Add(sim);
                        }
                        else
                        {
                            priorList.Add(0);
                        }
                    }
                }


            }

            int len = sents.Count;
            if (len == 1)
                return 1;

            double[,] matrix = new double[len, len];

            double[] csum = new double[len];

            for (int i = 0; i < len; ++i)
            {
                matrix[i, i] = 1; // set this to 1
                csum[i] += matrix[i, i];
                for (int j = i + 1; j < len; ++j)
                {
                    double sim = MSRA.NLC.Common.NLP.Similarity.Consine(vecs[sents[i]], vecs[sents[j]]);
                    matrix[i, j] = sim;
                    matrix[j, i] = sim;

                    csum[i] += matrix[j, i];
                    csum[j] += matrix[i, j];
                }
            }

            for (int i = 0; i < len; ++i)
                for (int j = 0; j < len; ++j)
                    if (csum[i] != 0)
                        matrix[j, i] /= csum[i];

            double[] rank = GraphRanker.PageRank(matrix, priorList.ToArray());
            double max = rank.Max();
            double min = rank.Min();
            double delta = max - min;
            if (delta == 0)
                delta = 1;

            for (int k = 0; k < len; ++k)
            {
                double score = rank[k];
                //score = (rank[k] - min) / delta;
                //sents[k].PDScore = (float)score;

                lock (sents[k].Lock) {
                    if (sents[k].PDScores == null)
                        sents[k].PDScores = new Dictionary<string, float>();
                    sents[k].PDScores.Add(modelName, (float)score);
                }
                
            }

            return sent_.PDScores[modelName];
        }
     }
}
