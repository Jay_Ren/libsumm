﻿using LibSumm.Entity;
using LibSumm.Features;
using LibSumm.Summarizer.Generator;
using LibSumm.Utils;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;
using MSRA.NLC.Common.NLP;
using NN4CSharp.Layers;
using NN4CSharp.Layers.LossLayers;
using NN4CSharp.Params;
using ROUGEPackage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Summarizer.Scorer
{
    /// <summary>
    /// (Tested) Redundancy Aware Sentence Regression with MLP implementation
    /// </summary>
    [Serializable]
    public class RASRMLP : SentenceScorer
    {
        private ParamPackManager ppm;
        private int hiddenDim = 100;
        private int decoderHidden1Index;
        private int decoderHidden2Index;
        private int decoderHidden3Index;
        private int decoderHidden4Index;
        private ParamPackManager tempPPM;

        [NonSerialized]
        private UniSentFeatureEngine uniFeatureEngine;
        [NonSerialized]
        private BiSentsFeatureEngine biFeatureEngine;

        object _lock = new object();
        public RASRMLP(UniSentFeatureEngine uniFeatureEngine, BiSentsFeatureEngine biFeatureEngine)
        {

            ppm = new ParamPackManager();
            tempPPM = new ParamPackManager();
            this.uniFeatureEngine = uniFeatureEngine;
            this.biFeatureEngine = biFeatureEngine;

            ParamPack WHid = ppm.NewParamPack();
            decoderHidden1Index = WHid.ID;
            WHid.NewW(Matrix<float>.Build.Random(3 * hiddenDim, this.uniFeatureEngine.Dimension + this.biFeatureEngine.Dimension + 1, new ContinuousUniform(-1.0 / (float)(this.uniFeatureEngine.Dimension + this.biFeatureEngine.Dimension), 1.0 / (float)(this.uniFeatureEngine.Dimension + this.biFeatureEngine.Dimension))));

            WHid = ppm.NewParamPack();
            decoderHidden2Index = WHid.ID;
            WHid.NewW(Matrix<float>.Build.Random(2 * hiddenDim, 3 * hiddenDim + 1, new ContinuousUniform(-1.0 / (float)(3 * hiddenDim), 1.0 / (float)(3 * hiddenDim))));

            WHid = ppm.NewParamPack();
            decoderHidden3Index = WHid.ID;
            WHid.NewW(Matrix<float>.Build.Random(hiddenDim, 2 * hiddenDim + 1, new ContinuousUniform(-1.0 / (float)(2 * hiddenDim), 1.0 / (float)(2 * hiddenDim))));

            WHid = ppm.NewParamPack();
            decoderHidden4Index = WHid.ID;
            WHid.NewW(Matrix<float>.Build.Random(1, hiddenDim + 1, new ContinuousUniform(-1.0 / (float)(hiddenDim), 1.0 / (float)(hiddenDim))));

        }

        public Layer Encoder(Sentence sent1, Sentence sent2 = null)
        {
            ParamPack pp = new ParamPack(0, tempPPM);
            pp.NewV(Vector<float>.Build.Dense(uniFeatureEngine.Format2Array(uniFeatureEngine.Extract(sent1))));
            LookupLayer uniLayer = new LookupLayer(uniFeatureEngine.Dimension, pp);
            uniLayer.IsUpdatable = false;

            LookupLayer biLayer = null;
            if (sent2 == null)
            {
                pp = new ParamPack(0, tempPPM);
                pp.NewV(Vector<float>.Build.Dense(biFeatureEngine.Dimension,0));
                biLayer = new LookupLayer(biFeatureEngine.Dimension, pp);
                biLayer.IsUpdatable = false;
            }
            else {
                pp = new ParamPack(0, tempPPM);
                pp.NewV(Vector<float>.Build.Dense(biFeatureEngine.Format2Array(biFeatureEngine.Extract(sent1, sent2))));
                biLayer = new LookupLayer(biFeatureEngine.Dimension, pp);
                biLayer.IsUpdatable = false;
            }

            ConcatLayer layer = new ConcatLayer(uniLayer, biLayer);
            layer.IsUpdatable = false;
            return layer;
        }
        public LossLayer Decoder(Layer layer)
        {
            LinearLayer hiddenLayer1 = new LinearLayer(layer, 3 * hiddenDim, ppm.Get(decoderHidden1Index), Nonlinearity.NonLinearityType.tanh, true);
            LinearLayer hiddenLayer2 = new LinearLayer(hiddenLayer1, 2 * hiddenDim, ppm.Get(decoderHidden2Index), Nonlinearity.NonLinearityType.tanh, true);
            LinearLayer hiddenLayer3 = new LinearLayer(hiddenLayer2, hiddenDim, ppm.Get(decoderHidden3Index), Nonlinearity.NonLinearityType.tanh, true);
            LinearLayer hiddenLayer4 = new LinearLayer(hiddenLayer3, 1, ppm.Get(decoderHidden4Index), Nonlinearity.NonLinearityType.tanh, true);
            var outputLayer = new SquaresLossLayer(hiddenLayer4);
            return outputLayer;
        }


        public static RASRMLP Load(string modelFile, UniSentFeatureEngine uniFeatureEngine, BiSentsFeatureEngine biFeatureEngine)
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(Path.Combine(modelFile), FileMode.Open, FileAccess.Read, FileShare.None);
            var model = (RASRMLP)formatter.Deserialize(stream);
            model.uniFeatureEngine = uniFeatureEngine;
            model.biFeatureEngine = biFeatureEngine;
            stream.Close();
            return model;
        }

        private void Save(string modelFile)
        {
            IFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(modelFile, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, this);
            stream.Close();
        }
        public void Train(string label, List<Corpus> trainColl)
        {
            var modelName = this.GetType().Name;
            string root = Path.Combine(Constant.Root, @"model\", modelName);
            if (!Directory.Exists(root))
                Directory.CreateDirectory(root);

            using (var sw = new StreamWriter(Path.Combine(root, "feature_" + label + "_list.txt")))
            {
                var dic = uniFeatureEngine.FeatureMap();
                foreach (var pair in dic)
                {
                    sw.WriteLine("{0} {1}", pair.Key, pair.Value);
                }

                dic = biFeatureEngine.FeatureMap();
                foreach (var pair in dic)
                {
                    sw.WriteLine("{0} {1}", pair.Key, pair.Value);
                }
            }

            int batchSize = 20;
            object _lock = new object();

            Setting setting = new Setting();
            setting.NgramSize = 2;
            setting.CurrentLengthType = ROUGEPackage.Setting.LengthLimitType.WORD;
            setting.SummaryLength = 200;

            ROUGEUtils rouge = new ROUGEUtils(setting);

            Dictionary<Corpus, Tuple<double, double>> scale = new Dictionary<Corpus, Tuple<double, double>>();
            Dictionary<Sentence, float> scores = new Dictionary<Sentence, float>();
            foreach (Corpus corpus in trainColl)
            {
                double max = -1;
                double min = 100;
                var sents = new List<Sentence>();
                foreach (var doc in corpus.Documents)
                {
                    sents.AddRange(doc.Sentences);
                }
                foreach (Sentence sent in sents)
                {
                    var references = new List<string>();
                    foreach (var summary in corpus.References.Values)
                    {
                        references.Add(string.Join(" ", summary.Sentences.Select(s => { return s.Content; })));
                    }
                    var rougeScore = rouge.Recall(sent.Content, references);
                    scores.Add(sent, (float)rougeScore[1]);
                    if (scores.Count % 1000 == 0)
                        Console.Write("[Log] Load Rouge Score {0}\r", scores.Count);

                    if (max < rougeScore[1])
                    {
                        max = rougeScore[1];
                    }
                    if (min > rougeScore[1])
                    {
                        min = rougeScore[1];
                    }
                }
                scale.Add(corpus, new Tuple<double, double>(max, min));
            }
            Console.WriteLine();



            Random ran = new Random();
            StopWordHandler stop = new StopWordHandler(Path.Combine(Constant.Root, @"model\stop.dict"));

            for (int n = 1; n <= 2; n++)
            {
                DateTime begin = DateTime.Now;
                Vector<float> error = Vector<float>.Build.Dense(4);
                int count = 0;

                for (int c = 0; c < trainColl.Count; c++)
                {
                    Corpus corpus = trainColl[c];
                    var sents = new List<Sentence>();
                    foreach (var doc in corpus.Documents)
                    {
                        sents.AddRange(doc.Sentences);
                    }
                    sents = sents.Where(s => { return s.Len > 0; }).ToList();

                    List<Sentence> uniBatch = new List<Sentence>();
                    List<Tuple<Sentence, Sentence>> biBatch = new List<Tuple<Sentence, Sentence>>();

                    //var generator = new GreedySummaryGenerator(200);
                    //generator.Generate("train", corpus, this);

                    //var generator = new ThresholdSummaryGenerator(200, 0.4f, 2, stop);
                    //generator.Generate("train", corpus, this);

                    for (int i = 0; i < sents.Count; i++)
                    {
                        uniBatch.Add(sents[i]);

                        if (uniBatch.Count == batchSize || i == sents.Count - 1)
                        {
                            Parallel.ForEach(uniBatch, sent =>
                            {
                                Layer sentLayer = this.Encoder(sent);
                                LossLayer lossLayer = this.Decoder(sentLayer);
                                lossLayer.ForwardPropagate();

                                lossLayer.BackwardPropagate(Vector<float>.Build.Dense(new float[] { (float)((scores[sent] - scale[corpus].Item2) / (scale[corpus].Item1 - scale[corpus].Item2)) }));

                                lock (_lock)
                                {
                                    error += lossLayer.Loss;
                                    count++;
                                }
                            });
                            ppm.Update();
                            ppm.Clear();
                            uniBatch.Clear();
                        }


                        var conditions = new List<Sentence>();

                        for (int j = 0; j < sents.Count; j++)
                        {
                            if (j == i)
                                continue;
                            double p = ran.NextDouble();
                            if (p > sents[j].GTScore * 0.05 + 0.05)
                                continue;

                            if (!conditions.Contains(sents[j]))
                                conditions.Add(sents[j]);
                        }

                        foreach (var sent in conditions)
                        {
                            biBatch.Add(new Tuple<Sentence, Sentence>(sents[i], sent));
                            if (biBatch.Count == batchSize||i==sents.Count-1)
                            {
                                Parallel.ForEach(biBatch, pair =>
                                {
                                    Layer sentLayer = this.Encoder(pair.Item1, pair.Item2);
                                    LossLayer lossLayer = this.Decoder(sentLayer);
                                    lossLayer.ForwardPropagate();

                                    var references = new List<string>();
                                    foreach (var summary in corpus.References.Values)
                                    {
                                        references.Add(string.Join(" ", summary.Sentences.Select(s => { return s.Content; })));
                                    }
                                    var rougeScore = rouge.Recall(pair.Item1.Content, pair.Item2.Content, references);
                                    lossLayer.BackwardPropagate(Vector<float>.Build.Dense(new float[] { (float)((rougeScore[1] - scale[corpus].Item2) / (scale[corpus].Item1 - scale[corpus].Item2)) }));

                                    lock (_lock)
                                    {
                                        error += lossLayer.Loss;
                                        count++;
                                    }
                                });
                                ppm.Update();
                                ppm.Clear();
                                biBatch.Clear();
                                Console.Write("[Log] Round {0} Corpus {1} Pair {2}\r", n, c + 1, count);
                            }

                        }
                    }

                    DateTime end_ = DateTime.Now;
                }

                Console.WriteLine();
                Console.Write("Count: "+count+" Loss: [");
                error = error / count;
                foreach (float e in error)
                {
                    Console.Write(e + ",");
                }

                DateTime end = DateTime.Now;

                Console.Write("] Time: " + (end.Subtract(begin).TotalSeconds) + "s");
                Console.WriteLine();

            }

            string modelFilePath = Path.Combine(root, modelName+"_" + label + ".model");
            Save(modelFilePath);
        }

        public override float Score(Sentence sent, List<Sentence> currentSummary)
        {
            float min = float.MaxValue;

            if (currentSummary.Count > 0)
            {
                foreach (var sent2 in currentSummary)
                {
                    Layer sentLayer = this.Encoder(sent,sent2);
                    var layer = this.Decoder(sentLayer);
                    layer.ForwardPropagate();
                    var score = layer.Output[0];
                    if (score < min)
                    {
                        min = score;
                    }
                }
            }
            else {
                Layer sentLayer = this.Encoder(sent);
                var layer = this.Decoder(sentLayer);
                layer.ForwardPropagate();
                min = layer.Output[0];
            }

            return min;
        }
    }
}
