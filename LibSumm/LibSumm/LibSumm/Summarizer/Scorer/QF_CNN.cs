﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibSumm.Entity;
using Common.Utils;
using NN4CSharp.Layers;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.Distributions;
using NN4CSharp.Layers.SimilarityLayers;
using NN4CSharp.Layers.LossLayers;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using NN4CSharp.Params;
using NN4CSharp.Utils;

namespace LibSumm.Summarizer.Scorer
{
    [Serializable]
    public class QF_CNN : SentenceScorer
    {
        private int queryFiltMatrixWin1;
        private int queryFiltMatrixWin2;
        //private int queryFiltMatrixWin3;

        private int sentFiltMatrixWin1;
        private int sentFiltMatrixWin2;
        //private int sentFiltMatrixWin3;

        //private int bilinear;

        private ParamPackManager ppm;
        private LookupTable table;
        private int hiddenDim;
        public bool test = true;

        public QF_CNN(EmbeddingHolder embedding) {
            hiddenDim = embedding.Dimension;
            ppm = new ParamPackManager();
            table = new LookupTable(hiddenDim,ppm,true);
            foreach (var t in embedding.Vocab) {
                table.AddVoc(t,Vector<float>.Build.Dense(embedding.Vectorize(t)));
            }

            ParamPack WHid = ppm.NewParamPack();
            queryFiltMatrixWin1 = WHid.ID;
            WHid.NewW(Matrix<float>.Build.Random(hiddenDim, hiddenDim + 1, new ContinuousUniform(-1.0 / (float)(hiddenDim), 1.0 / (float)(hiddenDim))));

            WHid = ppm.NewParamPack();
            queryFiltMatrixWin2 = WHid.ID;
            WHid.NewW(Matrix<float>.Build.Random(hiddenDim, 2 * hiddenDim + 1, new ContinuousUniform(-1.0 / (float)(2 * hiddenDim), 1.0 / (float)(2 * hiddenDim))));

            //WHid = ppm.NewParamPack();
            //queryFiltMatrixWin3 = WHid.ID;
            //WHid.NewW(Matrix<float>.Build.Random(hiddenDim, 3 * hiddenDim + 1, new ContinuousUniform(-1.0 / (float)(3 * hiddenDim), 1.0 / (float)(3 * hiddenDim))));


            WHid = ppm.NewParamPack();
            sentFiltMatrixWin1 = WHid.ID;
            WHid.NewW(Matrix<float>.Build.Random(hiddenDim, hiddenDim + 1, new ContinuousUniform(-1.0 / (float)(hiddenDim), 1.0 / (float)(hiddenDim))));

            WHid = ppm.NewParamPack();
            sentFiltMatrixWin2 = WHid.ID;
            WHid.NewW(Matrix<float>.Build.Random(hiddenDim, 2 * hiddenDim + 1, new ContinuousUniform(-1.0 / (float)(2 * hiddenDim), 1.0 / (float)(2 * hiddenDim))));

            //WHid = ppm.NewParamPack();
            //sentFiltMatrixWin3 = WHid.ID;
            //WHid.NewW(Matrix<float>.Build.Random(hiddenDim, 3 * hiddenDim + 1, new ContinuousUniform(-1.0 / (float)(3 * hiddenDim), 1.0 / (float)(3 * hiddenDim))));

            //WHid = ppm.NewParamPack();
            //bilinear = WHid.ID;
            //WHid.NewW(Matrix<float>.Build.Random(hiddenDim + 1, hiddenDim + 1, new ContinuousUniform(-1.0 / (float)(hiddenDim), 1.0 / (float)(hiddenDim))));

        }

        public Layer QueryModel(Sentence query) {
            var terms = query.Tokens.Select(t => { return t.LOWER; }).ToArray();
            Layer l1 = NNUtils.CNN(terms, ppm.Get(queryFiltMatrixWin1), table, hiddenDim, 1, true, true,test);
            Layer l2 = NNUtils.CNN(terms, ppm.Get(queryFiltMatrixWin2), table, hiddenDim, 2, true, true,test);
            //Layer l3 = NNUtils.CNN(terms, ppm.Get(queryFiltMatrixWin3), table, hiddenDim, 3, true, true);
            var layers = new List<Layer>() { l1, l2 };
            return new MaxPoolingLayer(layers);

            //List<Layer> layers = new List<Layer>();
            //foreach (var t in terms)
            //{
            //    layers.Add(table.CreateLookupLayer(t));
            //}
            //return new AvgPoolingLayer(layers, true);
        }

        public Layer AnswerModel(Sentence sent)
        {
            var terms = sent.Tokens.Select(t => { return t.LOWER; }).ToArray();
            Layer l1 = NNUtils.CNN(terms, ppm.Get(sentFiltMatrixWin1), table, hiddenDim, 1, true, true,test);
            Layer l2 = NNUtils.CNN(terms, ppm.Get(sentFiltMatrixWin2), table, hiddenDim, 2, true, true,test);
            //Layer l3 = NNUtils.CNN(terms, ppm.Get(sentFiltMatrixWin3), table, hiddenDim, 3, true, true);
            var layers = new List<Layer>() { l1, l2 };
            return new MaxPoolingLayer(layers);

            //List<Layer> layers = new List<Layer>();
            //foreach (var t in terms)
            //{
            //    layers.Add(table.CreateLookupLayer(t));
            //}
            //return new AvgPoolingLayer(layers, true);
        }

        public Layer Build(Sentence query, Sentence answer) {
            var queryModel = QueryModel(query);
            var answerModel = AnswerModel(answer);
            //return new BilinearSimilarityLayer(queryModel,answerModel,ppm.Get(bilinear));
            return new PairwiseSimilarityLayer(queryModel,answerModel);
        }

        public Layer Build(Sentence query, List<Sentence> answers)
        {
            var layers = new List<Layer>();
            foreach (var answer in answers) {
                layers.Add(AnswerModel(answer));
            }
            var answerModel = new MaxPoolingLayer(layers);
            var queryModel = QueryModel(query);
            //return new BilinearSimilarityLayer(queryModel, answerModel, ppm.Get(bilinear));
            return new PairwiseSimilarityLayer(queryModel, answerModel);
        }


        public LossLayer Loss(Layer layer) {
            return new SquaresLossLayer(layer);
        }

        public LossLayer Loss(Layer player, Layer nlayer,float margin) {
            return new PairwiseLossLayer(player,nlayer,margin);
        }

        public static QF_CNN Load(string modelFile)
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(Path.Combine(modelFile), FileMode.Open, FileAccess.Read, FileShare.None);
            var model = (QF_CNN)formatter.Deserialize(stream);
            stream.Close();
            return model;
        }

        private void Save(string modelFile)
        {
            IFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(modelFile, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, this);
            stream.Close();
        }

        public void Train(string label, List<Corpus> trainColl)
        {
            var modelName = this.GetType().Name;
            string root = Path.Combine(Constant.Root, @"model\", modelName);
            if (!Directory.Exists(root))
                Directory.CreateDirectory(root);

            int batchSize = 20;
            object _lock = new object();

            Random rng = new Random();
            List<Tuple<Document, Document>> batch = new List<Tuple<Document, Document>>();

            for (int n = 1; n <= 2; n++)
            {
                float error = 0;
                int count = 0;
                DateTime begin = DateTime.Now;

                for (int c = 0; c < trainColl.Count; c++)
                {
                    Corpus corpus = trainColl[c];

                    var positive= corpus.Documents.Where(d => { return d.Sentences[0].GTScore > 0.9; }).ToList();
                    var negative = corpus.Documents.Where(d => { return d.Sentences[0].GTScore < 0.1; }).ToList();

                    if (positive.Count == 0 || negative.Count == 0)
                        continue;

                    foreach (var doc in negative)
                    {
                        int j = rng.Next(0, positive.Count);
                        batch.Add(new Tuple<Document, Document>(positive[j], doc));

                        if (batch.Count == batchSize)
                        {
                            Parallel.ForEach(batch, pair =>
                            {
                                var player = Build(corpus.Query, pair.Item1.Sentences);
                                var nlayer = Build(corpus.Query, pair.Item2.Sentences);
                                var lossLayer = Loss(player, nlayer, 0.1f);
                                lossLayer.ForwardPropagate();
                                lossLayer.BackwardPropagate(null);
                                lock (_lock)
                                {
                                    error += lossLayer.Loss[0];
                                    count++;
                                }
                            });
                            ppm.Update();
                            ppm.Clear();
                            batch.Clear();
                        }
                        Console.Write("[Log] Round {0} Corpus {1} Sentence {2} Loss {3}\r", n, c + 1, count, error / count);
                    }

                }
                Console.WriteLine();
                Console.Write("Loss: [");
                error = error / count;
                Console.Write(error);
                DateTime end = DateTime.Now;

                Console.Write("] Time: " + (end.Subtract(begin).TotalSeconds) + "s");
                Console.WriteLine();

            }

            //string modelFilePath = Path.Combine(root, modelName + "_" + label + ".model");
            //Save(modelFilePath);
        }

        public override float Score(Sentence sent, List<Sentence> currentSummary)
        {
            if (sent.Len == 0)
                return 0;
            var layer = Build(sent.Doc.Corpus.Query,sent);
            layer.ForwardPropagate();

            return layer.Output[0];
        }
    }
}
