﻿using de.bwaldvogel.liblinear;
using LibSumm.Entity;
using LibSumm.Features;
using LibSumm.Utils;
using ROUGEPackage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Summarizer.Scorer
{
    [Serializable]
    public class RASRSVR : SentenceScorer
    {

        private Model model;
        private Parameter param;
        private UniSentFeatureEngine uniFeatureEngine;
        private BiSentsFeatureEngine biFeatureEngine;
        private FeatureEngineWrapper featureEngines;
        //private Dictionary<string, float> max_coll;
        //private Dictionary<string, float> min_coll;

        public RASRSVR(UniSentFeatureEngine uniFeatureEngine, BiSentsFeatureEngine biFeatureEngine, double C = 1, double eps = 0.1, double p = 0.1)
        {
            this.uniFeatureEngine = uniFeatureEngine;
            this.biFeatureEngine = biFeatureEngine;
            featureEngines = new FeatureEngineWrapper(uniFeatureEngine,biFeatureEngine);
            this.param = new Parameter(SolverType.L2R_L2LOSS_SVR, C, eps, p);
        }

        public void Load(string modelFile)
        {
            this.model = Model.load(new java.io.File(modelFile));

            //if (maxFile != null && minFile != null)
            //{
            //    this.max_coll = new Dictionary<string, float>();
            //    this.min_coll = new Dictionary<string, float>();
            //    var lines = File.ReadAllLines(maxFile);
            //    foreach (var line in lines)
            //    {
            //        var temp = line.Split(new char[] { ' ' }, StringSplitOptions.None);
            //        this.max_coll.Add(temp[0], float.Parse(temp[1]));
            //    }
            //    lines = File.ReadAllLines(minFile);
            //    foreach (var line in lines)
            //    {
            //        var temp = line.Split(new char[] { ' ' }, StringSplitOptions.None);
            //        this.min_coll.Add(temp[0], float.Parse(temp[1]));
            //    }
            //}

        }

        public void Train(string label, List<Corpus> trainColl)
        {
            var modelName = this.GetType().Name;
            string root = Path.Combine(Constant.Root, @"model\", modelName);
            if (!Directory.Exists(root))
                Directory.CreateDirectory(root);

            Setting setting = new Setting();
            setting.NgramSize = 2;
            setting.CurrentLengthType = ROUGEPackage.Setting.LengthLimitType.WORD;
            setting.SummaryLength = 200;

            ROUGEUtils rouge = new ROUGEUtils(setting);


            Dictionary<Corpus, Tuple<double, double>> scale = new Dictionary<Corpus, Tuple<double, double>>();
            foreach (Corpus corpus in trainColl)
            {
                double max = -1;
                double min = 100;
                var sents = new List<Sentence>();
                foreach (var doc in corpus.Documents)
                {
                    sents.AddRange(doc.Sentences);
                }
                foreach (Sentence sent in sents)
                {
                    var references = new List<string>();
                    foreach (var summary in corpus.References.Values)
                    {
                        references.Add(string.Join(" ", summary.Sentences.Select(s => { return s.Content; })));
                    }
                    var rougeScore = rouge.Recall(sent.Content, references);

                    Console.Write("[Log] Load Rouge Score {0}\r", scale.Count);

                    if (max < rougeScore[1])
                    {
                        max = rougeScore[1];
                    }
                    if (min > rougeScore[1])
                    {
                        min = rougeScore[1];
                    }
                }
                scale.Add(corpus, new Tuple<double, double>(max, min));
            }
            Console.WriteLine();


            using (var sw = new StreamWriter(Path.Combine(root, "feature_" + label + "_list.txt")))
            {
                var dic = featureEngines.FeatureMap();
                foreach (var pair in dic)
                {
                    sw.WriteLine("{0} {1}", pair.Key, pair.Value);
                }
            }

            int count = 0;
            object obj = new object();
            Random rng = new Random();
            using (var sw = new StreamWriter(Path.Combine(root, "feature_" + label + "_value.txt")))
            {
                Parallel.For(0, trainColl.Count, i => {
                    var sents = new List<Sentence>();

                    foreach (var doc in trainColl[i].Documents)
                    {
                        sents.AddRange(doc.Sentences);
                    }

                    foreach (var sent1 in sents)
                    {
                        Dictionary<string, float> features = uniFeatureEngine.Extract(sent1);
                        lock (obj)
                        {
                            var references = new List<string>();
                            foreach (var summary in sent1.Doc.Corpus.References.Values)
                            {
                                references.Add(string.Join(" ", summary.Sentences.Select(s => { return s.Content; })));
                            }
                            var rougeScore = rouge.Recall(sent1.Content, references);

                            var score = (rougeScore[1] - scale[sent1.Doc.Corpus].Item2) / (scale[sent1.Doc.Corpus].Item1 - scale[sent1.Doc.Corpus].Item2);

                            sw.WriteLine("{0} {1}", score, featureEngines.Format2String(features));

                            count++;
                            if (count % 100 == 0)
                            {
                                Console.Write("[Log] Extract Features {0}\r", count);
                                sw.Flush();
                            }
                        }

                        foreach (var sent2 in sents)
                        {
                            if (sent2.Equals(sent1))
                                continue;

                            if (rng.NextDouble() > 0.05 * sent2.GTScore + 0.05)
                                continue;

                             features = featureEngines.Extract(sent1, sent2);

                            lock (obj)
                            {

                                var references = new List<string>();
                                foreach (var summary in sent1.Doc.Corpus.References.Values)
                                {
                                    references.Add(string.Join(" ", summary.Sentences.Select(s => { return s.Content; })));
                                }
                                var rougeScore = rouge.Recall(sent1.Content, sent2.Content, references);

                                var score = (rougeScore[1] - scale[sent1.Doc.Corpus].Item2) / (scale[sent1.Doc.Corpus].Item1 - scale[sent1.Doc.Corpus].Item2);

                                sw.WriteLine("{0} {1}", score, featureEngines.Format2String(features));

                                count++;
                                if (count % 100 == 0)
                                {
                                    Console.Write("[Log] Extract Features {0}\r", count);
                                    sw.Flush();
                                }
                            }
                        }
                    }
                });
                Console.WriteLine();

            }


            Problem prob = Problem.readFromFile(new java.io.File(Path.Combine(root, "feature_" + label + "_value.txt")), -1);

            model = Linear.train(prob, param);

            string modelFilePath = Path.Combine(root, modelName + "_" + label + ".model");

            model.save(new java.io.File(modelFilePath));
        }

        public override float Score(Sentence sent, List<Sentence> currentSummary)
        {
            //float low = 0.01f;
            //float upper = 0.99f;

            float minScore = float.MaxValue;
            if (currentSummary.Count > 0)
            {
                foreach (var sent2 in currentSummary) {
                    var temp = featureEngines.Extract(sent, sent2);

                    //if (max_coll != null && min_coll != null)
                    //{
                    //    var keys = new HashSet<string>(temp.Keys);
                    //    foreach (var key in keys)
                    //    {
                    //        temp[key] = low + (upper - low) * (temp[key] - min_coll[key]) / (max_coll[key] - min_coll[key]);
                    //    }
                    //}
                    var features = featureEngines.Format2Array(temp);
                    FeatureNode[] nodes = new FeatureNode[features.Length];
                    for (int i = 0; i < nodes.Length; i++)
                    {
                        nodes[i] = new FeatureNode(i + 1, features[i]);
                    }

                    var score = (float)Linear.predict(model, nodes);
                    if (score > minScore)
                        minScore = score;
                }
                
            }
            else {
                var temp = featureEngines.Extract(sent);

                //if (max_coll != null && min_coll != null)
                //{
                //    var keys = new HashSet<string>(temp.Keys);
                //    foreach (var key in keys)
                //    {
                //        temp[key] = low + (upper - low) * (temp[key] - min_coll[key]) / (max_coll[key] - min_coll[key]);
                //    }
                //}
                var features = featureEngines.Format2Array(temp);
                FeatureNode[] nodes = new FeatureNode[features.Length];
                for (int i = 0; i < nodes.Length; i++)
                {
                    nodes[i] = new FeatureNode(i + 1, features[i]);
                }

                minScore = (float)Linear.predict(model, nodes);
            }

            return minScore;
        }

    }
}
