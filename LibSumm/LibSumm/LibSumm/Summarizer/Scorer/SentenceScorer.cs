﻿using LibSumm.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Summarizer.Scorer
{
    [Serializable]
   public abstract class SentenceScorer:SummaryScorer
    {

       public override float Score(List<Sentence> summary)
       {

           var currentSummary = new List<Sentence>(summary);
           Sentence sent = currentSummary[currentSummary.Count - 1];
           currentSummary.Remove(sent);
           return Score(sent, currentSummary);
       }

       public abstract float Score(Sentence sent, List<Sentence> currentSummary);
    }
}
