﻿using LibSumm.Entity;
using LibSumm.Summarizer.Generator;
using LibSumm.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Summarizer.Scorer
{
    [Serializable]
    public abstract class SummaryScorer
    {
        public abstract float Score(List<Sentence> summary);
    }
}
