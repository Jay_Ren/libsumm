﻿using LibSumm.Entity;
using LibSumm.Features;
using LibSumm.Summarizer.Generator;
using LibSumm.Utils;
using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;
using NN4CSharp.Layers;
using NN4CSharp.Layers.LossLayers;
using NN4CSharp.Params;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Summarizer.Scorer
{
    /// <summary>
    /// (Tested) Sentence Regression with MLP implementation
    /// </summary>
    [Serializable]
    public class SRMLP: SentenceScorer
    {
        [NonSerialized]
        private UniSentFeatureEngine featureEngine;

        private ParamPackManager ppm;
        private int decoderHidden1Index;
        private int decoderHidden2Index;
        private int decoderHidden3Index;
        //private int decoderHidden4Index;
        private ParamPackManager tempPPM;

        public SRMLP(UniSentFeatureEngine featureEngine) {
            this.featureEngine = featureEngine;

            ppm = new ParamPackManager();
            tempPPM = new ParamPackManager();

            ParamPack WHid = ppm.NewParamPack();
            decoderHidden1Index = WHid.ID;
            WHid.NewW(Matrix<float>.Build.Random(100, featureEngine.Dimension + 1, new ContinuousUniform(-1.0 / (float)(featureEngine.Dimension), 1.0 / (float)(featureEngine.Dimension))));

            WHid = ppm.NewParamPack();
            decoderHidden2Index = WHid.ID;
            WHid.NewW(Matrix<float>.Build.Random(50, 100 + 1, new ContinuousUniform(-1.0 / (float)(100), 1.0 / (float)(100))));

            WHid = ppm.NewParamPack();
            decoderHidden3Index = WHid.ID;
            WHid.NewW(Matrix<float>.Build.Random(1, 50 + 1, new ContinuousUniform(-1.0 / (float)(50), 1.0 / (float)(50))));

            //WHid = ppm.NewParamPack();
            //decoderHidden4Index = WHid.ID;
            //WHid.NewW(Matrix<float>.Build.Random(1, hiddenDim + 1, new ContinuousUniform(-1.0 / (float)(hiddenDim), 1.0 / (float)(hiddenDim))));

        }


        public Layer Encoder(Sentence sent)
        {
            ParamPack pp = new ParamPack(0, tempPPM);
            pp.NewV(Vector<float>.Build.Dense(featureEngine.Format2Array(featureEngine.Extract(sent))));
            LookupLayer fLayer = new LookupLayer(featureEngine.Dimension, pp);
            fLayer.IsUpdatable = false;
            return fLayer;
        }

        public Layer Decoder(Layer sentLayer)
        {
            LinearLayer hiddenLayer1 = new LinearLayer(sentLayer, 100, ppm.Get(decoderHidden1Index), Nonlinearity.NonLinearityType.tanh, true);
            LinearLayer hiddenLayer2 = new LinearLayer(hiddenLayer1, 50, ppm.Get(decoderHidden2Index), Nonlinearity.NonLinearityType.tanh, true);
            LinearLayer hiddenLayer3 = new LinearLayer(hiddenLayer2, 1, ppm.Get(decoderHidden3Index), Nonlinearity.NonLinearityType.tanh, true);
            //LinearLayer hiddenLayer4 = new LinearLayer(hiddenLayer3, 1, ppm.Get(decoderHidden4Index), Nonlinearity.NonLinearityType.tanh, true);
            return hiddenLayer3;
        }

        public LossLayer Loss(Layer layer) {
            var outputLayer = new SquaresLossLayer(layer);

            return outputLayer;
        }

        public LossLayer Loss(Layer player, Layer nlayer, float margin)
        {
            return new PairwiseLossLayer(player, nlayer, margin);
        }

        public static SRMLP Load(string modelFile, UniSentFeatureEngine featureEngine) {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(Path.Combine(modelFile), FileMode.Open, FileAccess.Read, FileShare.None);
            var model=(SRMLP)formatter.Deserialize(stream);
            model.featureEngine = featureEngine;
            stream.Close();
            return model;
        }

        private void Save(string modelFile)
        {
            IFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(modelFile, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, this);
            stream.Close();
        }

        public void Train(string label,List<Corpus> trainColl) {

            var modelName = this.GetType().Name;
            string root = Path.Combine(Constant.Root, @"model\", modelName);
            if (!Directory.Exists(root))
                Directory.CreateDirectory(root);

            var dic = featureEngine.FeatureMap();
            using (var sw = new StreamWriter(Path.Combine(root,"feature_"+ label + "_list.txt"))) {
                foreach (var pair in dic) {
                    sw.WriteLine("{0} {1}",pair.Key,pair.Value);
                }
            }

            int batchSize = 20;
            object _lock = new object();
            Random rng = new Random();
            List<Sentence> singleBatch = new List<Sentence>();
            List<Tuple<Sentence, Sentence>> pairBatch = new List<Tuple<Sentence, Sentence>>();
            for (int n = 1; n <= 1; n++)
            {
                float error = 0;
                int count = 0;
                DateTime begin = DateTime.Now;

                for(int c=0;c<trainColl.Count;c++)
                {
                    Corpus corpus = trainColl[c];
                    List<Sentence> sents = new List<Sentence>();
                    foreach (var doc in corpus.Documents) {
                        sents.AddRange(doc.Sentences);
                    }

                   
                    for (int i = 0; i < sents.Count; i++)
                    {
                        singleBatch.Add(sents[i]);
                        if (singleBatch.Count == batchSize || i == sents.Count - 1)
                        {
                            Parallel.ForEach(singleBatch, sent =>
                            {
                                var layer = Decoder(Encoder(sent));
                                var lossLayer = Loss(layer);

                                lossLayer.ForwardPropagate();
                                lossLayer.BackwardPropagate(Vector<float>.Build.Dense(new float[] { sent.GTScore }));
                                lock (_lock)
                                {
                                    error += lossLayer.Loss[0];
                                    count++;
                                }
                            });
                            ppm.Update();
                            ppm.Clear();
                            singleBatch.Clear();

                            DateTime tempend = DateTime.Now;
                            Console.Write("[Log] Round {0} Corpus {1} Sentence {2} RegressionLoss {3} Time {4}s\r", n, c + 1, count, string.Format("{0:0.00000}", error / count), tempend.Subtract(begin).TotalSeconds);
                        }


                        //int j = rng.Next(sents.Count);
                        //while (j == i || Math.Abs(sents[i].GTScore - sents[j].GTScore) < 0.01)
                        //{
                        //    j = rng.Next(sents.Count);
                        //}

                        //if (sents[i].GTScore > sents[j].GTScore)
                        //{
                        //    pairBatch.Add(new Tuple<Sentence, Sentence>(sents[i], sents[j]));
                        //}
                        //else if (sents[i].GTScore < sents[j].GTScore)
                        //{
                        //    pairBatch.Add(new Tuple<Sentence, Sentence>(sents[j], sents[i]));
                        //}

                        //if (pairBatch.Count == batchSize)
                        //{
                        //    Parallel.ForEach(pairBatch, p =>
                        //    {
                        //        var layer1 = Decoder(Encoder(p.Item1));
                        //        var layer2 = Decoder(Encoder(p.Item2));
                        //        var lossLayer = Loss(layer1, layer2, 0.01f);
                        //        lossLayer.ForwardPropagate();
                        //        lossLayer.BackwardPropagate(null);

                        //        lock (_lock)
                        //        {
                        //            error += lossLayer.Loss[0];
                        //            count++;
                        //        }
                        //    });
                        //    ppm.Update();
                        //    ppm.Clear();
                        //    pairBatch.Clear();

                        //    DateTime tempend = DateTime.Now;
                        //    Console.Write("[Log] Round {0} Corpus {1} Sentence {2} MarginLoss {3} Time {4}s\r", n, c + 1, count, string.Format("{0:0.00000}", error / count), tempend.Subtract(begin).TotalSeconds);
                        //}

                    }
                }
                Console.WriteLine();
                Console.Write("Loss: [");
                error = error / count;
                Console.Write(error);
                DateTime end = DateTime.Now;

                Console.Write("] Time: " + (end.Subtract(begin).TotalSeconds) + "s");
                Console.WriteLine();

            }

            string modelFilePath = Path.Combine(root, modelName+"." + label + ".model");
            Save(modelFilePath);
        }

        public override float Score(Sentence sent, List<Sentence> currentSummary) {
            Layer sentLayer = this.Encoder(sent);
            var layer = this.Decoder(sentLayer);
            layer.ForwardPropagate();
            return layer.Output[0];
        }
    }
}
