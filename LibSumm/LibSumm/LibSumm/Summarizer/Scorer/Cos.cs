﻿using LibSumm.Summarizer.Similarity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibSumm.Entity;
using MSRA.NLC.Common.NLP;

namespace LibSumm.Summarizer.Scorer
{
    [Serializable]
   public class Cos : SentenceScorer
    {
        private ISentenceSimilarity sim;
        public Cos(ISentenceSimilarity sim) {
            this.sim = sim;
        }
        public override float Score(Sentence sent, List<Sentence> currentSummary)
        {
            float score = 0;
            int count = 0;
            var sents=sent.Doc.Sentences.Where(s => { return s.Len > 1; });
            foreach (var sent_ in sents) {
                score += sim.Similarity(sent,sent_);
                count++;
            }
            return score / count;
        }
    }
}
