﻿using LibSumm.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Summarizer.Scorer
{
    public class GraphRanker
    {

        public static Dictionary<string, double> Convert(Sentence sent, Dictionary<string, int> tf,
         Dictionary<string, float> idf)
        {
            Dictionary<string, double> vec = new Dictionary<string, double>();

            foreach (var token in sent.Tokens)
            {
                string word = token.LOWER;
                if (!vec.ContainsKey(word))
                    word = token.STEM;
                if (!vec.ContainsKey(word))
                {

                    if (!tf.ContainsKey(word))
                        continue;

                    float v = tf[word];
                    if (idf != null && idf.ContainsKey(word))
                        v *= idf[word];
                    else
                    {

                    }

                    vec.Add(word, v);
                }
            }

            return vec;
        }

        public static double[] PageRank(double[,] matrix, double[] prior = null)
        {
            int len = matrix.GetLength(0);

            double[] rank = new double[len];
            double[] prev = new double[len];

            for (int k = 0; k < len; ++k)
            {
                prev[k] = 1.0 / len;
            }
            if (prior == null || prior.Sum() == 0)
            {
                prior = prev.Clone() as double[];
            }
            int round = 1;
            double loss = 0;
            while (true)
            {
                for (int i = 0; i < len; ++i)
                {
                    double sum = 0;
                    for (int j = 0; j < len; ++j)
                    {
                        sum += matrix[i, j] * prev[j];
                    }

                    double score = sum * Delta + (1 - Delta) * prior[i];
                    rank[i] = score;
                }

                loss = CalculateLoss(rank, prev);
                round++;
                //Console.WriteLine("[LEXRANK]: ROUND: {0}, LOSS: {1}", round, loss);

                if (loss < Threshold || round > MaxRound)
                {
                    break;
                }

                for (int k = 0; k < len; ++k)
                {
                    prev[k] = rank[k];
                }
            }
            //Console.WriteLine("[LEXRANK]: ROUND: {0}, LOSS: {1}", round, loss);
            return rank;
        }

        private static double CalculateLoss(double[] rank, double[] prev)
        {
            double max = 0;
            for (int k = 0; k < rank.Length; ++k)
            {
                double gap = Math.Abs(rank[k] - prev[k]);
                if (max < gap)
                    max = gap;
            }

            return max;
        }

        public static int MaxRound = 500;
        public static double Threshold = 1e-4;
        public static double Delta = 0.85;
    }
}
