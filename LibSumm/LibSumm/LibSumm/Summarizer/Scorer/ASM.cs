﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibSumm.Entity;
using LibSumm.Features;
using NN4CSharp.Params;
using Common.Utils;
using NN4CSharp.Layers;
using MathNet.Numerics.LinearAlgebra;
using NN4CSharp.Utils;
using MathNet.Numerics.Distributions;
using NN4CSharp.Layers.SimilarityLayers;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Runtime.Serialization;
using NN4CSharp.Layers.LossLayers;

namespace LibSumm.Summarizer.Scorer
{
    /// <summary>
    /// article structure model
    /// </summary>
    [Serializable]
    public class ASM : SentenceScorer
    {

        [NonSerialized]
        private UniSentFeatureEngine featureEngine;

        private ParamPackManager ppm;
        private int numHid;
        private LookupTable lookupTable;

        private LSTM lstm;
        private int cnnPP;
        private int mlp1PP;
        private int mlp2PP;
        private int mlp3PP;

        private int numContext=5;

        public bool test = false;

        private int queryBilinear;
        private int titleBilinear;

        public ASM(EmbeddingHolder embedding, UniSentFeatureEngine featureEngine) {
            this.featureEngine = featureEngine;
            ppm = new ParamPackManager();
            numHid = embedding.Dimension;

            lookupTable = new LookupTable(numHid,ppm);
            foreach (var t in embedding.Vocab)
            {
                lookupTable.AddVoc(t, Vector<float>.Build.Dense(embedding.Vectorize(t)));
            }

            lstm = new LSTM(numHid,ppm,lookupTable);

            ParamPack WHid = ppm.NewParamPack();
            cnnPP = WHid.ID;
            WHid.NewW(Matrix<float>.Build.Random(numHid, 2* numHid+ 1, new ContinuousUniform(-1.0 / (float)(2 * numHid), 1.0 / (float)(2 * numHid))));


            WHid = ppm.NewParamPack();
            mlp1PP = WHid.ID;
            WHid.NewW(Matrix<float>.Build.Random(100, 2+numHid+featureEngine.Dimension + 1, new ContinuousUniform(-1.0 / (float)(1 + featureEngine.Dimension), 1.0 / (float)(1 + featureEngine.Dimension))));

            WHid = ppm.NewParamPack();
            mlp2PP = WHid.ID;
            WHid.NewW(Matrix<float>.Build.Random(50, 100 + 1, new ContinuousUniform(-1.0 / (float)(100), 1.0 / (float)(100))));

            WHid = ppm.NewParamPack();
            mlp3PP = WHid.ID;
            WHid.NewW(Matrix<float>.Build.Random(1, 50 + 1, new ContinuousUniform(-1.0 / (float)(50), 1.0 / (float)(50))));

            //WHid = ppm.NewParamPack();
            //mlp4PP = WHid.ID;
            //WHid.NewW(Matrix<float>.Build.Random(1, numHid + 1, new ContinuousUniform(-1.0 / (float)(numHid), 1.0 / (float)(numHid))));

            WHid = ppm.NewParamPack();
            queryBilinear = WHid.ID;
            WHid.NewW(Matrix<float>.Build.Random(numHid + 1, numHid + 1, new ContinuousUniform(-1.0 / (float)(numHid), 1.0 / (float)(numHid))));

            WHid = ppm.NewParamPack();
            titleBilinear = WHid.ID;
            WHid.NewW(Matrix<float>.Build.Random(numHid + 1, numHid + 1, new ContinuousUniform(-1.0 / (float)(numHid), 1.0 / (float)(numHid))));

        }


        public Layer SentenceModel(Sentence sent) {

            //return NNUtils.CNN(sent.Tokens.Select(t => { return t.LOWER; }).ToArray(), ppm.Get(cnnPP), lookupTable, numHid, 2, false, true,test);

            List<Layer> layers = new List<Layer>();
            foreach (var t in sent.Tokens)
            {
                layers.Add(new DropoutLayer(lookupTable.CreateLookupLayer(t.LOWER), 0.5f, test));
            }
            return new AvgPoolingLayer(layers, true);
        }

        //public Layer SentenceModel(Sentence sent,Layer layer) {
        //    List<Layer> weights = new List<Layer>();
        //    List<Layer> layers = new List<Layer>();
        //    foreach (var t in sent.Tokens)
        //    {
        //        var wlayer = new DropoutLayer(lookupTable.CreateLookupLayer(t.LOWER), 0.5f, test);
        //        weights.Add(new PairwiseSimilarityLayer(wlayer,layer,NN4CSharp.Activations.Similarity.SimilarityType.cos));
        //        layers.Add(wlayer);
        //    }
        //    return new WeightedAvgPoolingLayer(layers, new ConcatLayer(weights));
        //}

        public Layer ContextRelationFeatures(Sentence sent) {
            List<Layer> layers = new List<Layer>();
            var sentModel = SentenceModel(sent);
            var sents = sent.Doc.Sentences.OrderByDescending(s=> { return -s.Position; }).ToList();


            List<Layer> sentModels = new List<Layer>();
            for (int i = Math.Max(0, sent.Position -1 - numContext); i < sent.Position-1; i++) {
                sentModels.Add(SentenceModel(sents[i]));
                //sentModels.Add(SentenceModel(sents[i], sentModel));
            }
            if (sentModels.Count > 0) {
               var context= lstm.List2LSTM(sentModels);
                layers.Add(new PairwiseSimilarityLayer(context,sentModel));
            }
            else
            {
                var pp = new ParamPack(0, new ParamPackManager());
                pp.NewV(Vector<float>.Build.Dense(1, 1));
                var l=new LookupLayer(1, pp);
                l.IsUpdatable = false;
                layers.Add(l);
            }

            sentModels = new List<Layer>();
            for (int i = sent.Position; i < Math.Min(sents.Count, sent.Position + numContext); i++)
            {
                sentModels.Add(SentenceModel(sents[i]));
                //sentModels.Add(SentenceModel(sents[i],sentModel));
            }
            if (sentModels.Count > 0)
            {
                var context = lstm.List2LSTM(sentModels);
                layers.Add(new PairwiseSimilarityLayer(context, sentModel));
            }
            else
            {
                var pp = new ParamPack(0, new ParamPackManager());
                pp.NewV(Vector<float>.Build.Dense(1,1));
                var l = new LookupLayer(1, pp);
                l.IsUpdatable = false;
                layers.Add(l);
            }

            //var layer = new MaxPoolingLayer(layers);
            layers.Add(sentModel);
            return new ConcatLayer(layers);
        }

        public Layer AttentionBasedContextRelationFeatures(Sentence sent)
        {
            List<Layer> layers = new List<Layer>();

            var sentModel = SentenceModel(sent);
            var sents = sent.Doc.Sentences.OrderByDescending(s => { return -s.Position; }).ToList();

            List<Layer> sentModels = new List<Layer>();
            List<Layer> weights = new List<Layer>();
            for (int i = Math.Max(0, sent.Position - 1 - numContext); i < sent.Position - 1; i++)
            {
                var temp=SentenceModel(sents[i]);
                sentModels.Add(temp);
                weights.Add(new PairwiseSimilarityLayer(temp, sentModel));
            }
            if (sentModels.Count > 0)
            {
                var contexts = lstm.List2LSTMs(sentModels);
                layers.Add(new PairwiseSimilarityLayer(new WeightedAvgPoolingLayer(contexts, new NormalizeLayer(new ConcatLayer(weights), NormalizeType.Softmax)), sentModel));
            }
            //else
            //{
            //    var pp = new ParamPack(0, new ParamPackManager());
            //    pp.NewV(Vector<float>.Build.Dense(1));
            //    layers.Add(new LookupLayer(1, pp));
            //}

            sentModels = new List<Layer>();
            weights = new List<Layer>();
            for (int i = sent.Position; i < Math.Min(sents.Count, sent.Position + numContext); i++)
            {
                var temp = SentenceModel(sents[i]);
                sentModels.Add(temp);
                weights.Add(new PairwiseSimilarityLayer(temp, sentModel));
            }
            if (sentModels.Count > 0)
            {
                var contexts = lstm.List2LSTMs(sentModels);
                layers.Add(new PairwiseSimilarityLayer(new WeightedAvgPoolingLayer(contexts, new NormalizeLayer(new ConcatLayer(weights),NormalizeType.Softmax)),sentModel));
            }
            //else {
            //    var pp = new ParamPack(0,new ParamPackManager());
            //    pp.NewV(Vector<float>.Build.Dense(1));
            //    layers.Add(new LookupLayer(1,pp));
            //}

            //var layer = new MaxPoolingLayer(layers);
            layers.Add(sentModel);
            return new ConcatLayer(layers);
        }

        public Layer SurfaceFeatures(Sentence sent) {
            ParamPack pp = new ParamPack(0, new ParamPackManager());
            pp.NewV(Vector<float>.Build.Dense(featureEngine.Format2Array(featureEngine.Extract(sent))));
            var f2 = new LookupLayer(featureEngine.Dimension, pp);
            f2.IsUpdatable = false;
            return f2;
        }

        public Layer QueryFeature(Sentence sent) {
            var sentModel = SentenceModel(sent);
            List<Layer> layers = new List<Layer>();
            foreach (var query in sent.Doc.Corpus.Queries)
            {
                var queryModel = SentenceModel(query);
                layers.Add(new BilinearSimilarityLayer(queryModel, sentModel, ppm.Get(queryBilinear)));
            }
            var qc = new AvgPoolingLayer(layers, true);
            return qc;
        }

        public Layer TitleFeature(Sentence sent)
        {
            var sentModel = SentenceModel(sent);
            var titleModel = SentenceModel(sent.Doc.Title);
            var tc = new BilinearSimilarityLayer(titleModel, sentModel, ppm.Get(titleBilinear));
            return tc;
        }

        public Layer Build(Sentence sent) {
            var f1 = ContextRelationFeatures(sent);
            //var f1 = AttentionBasedContextRelationFeatures(sent);
            //return f1;

            var f2 = SurfaceFeatures(sent);

            Layer f = new ConcatLayer(new List<Layer>(new[] {  f1,f2}));

            //f = new DropoutLayer(f, 0.5f, test);

            LinearLayer hiddenLayer1 = new LinearLayer(f, 100, ppm.Get(mlp1PP), Nonlinearity.NonLinearityType.tanh, true);
            LinearLayer hiddenLayer2 = new LinearLayer(hiddenLayer1,  50, ppm.Get(mlp2PP), Nonlinearity.NonLinearityType.tanh, true);
            LinearLayer hiddenLayer3 = new LinearLayer(hiddenLayer2, 1, ppm.Get(mlp3PP), Nonlinearity.NonLinearityType.tanh, true);
            //LinearLayer hiddenLayer4 = new LinearLayer(hiddenLayer3, 1, ppm.Get(mlp4PP), Nonlinearity.NonLinearityType.tanh, true);

            return hiddenLayer3;
        }

        public override float Score(Sentence sent, List<Sentence> currentSummary)
        {
            var layer = Build(sent);
            layer.ForwardPropagate();
            return layer.Output[0];
        }

        public LossLayer Loss(Layer layer)
        {
            var outputLayer = new SquaresLossLayer(layer);

            return outputLayer;
        }

        public LossLayer Loss(Layer player, Layer nlayer, float margin)
        {
            return new PairwiseLossLayer(player, nlayer, margin);
        }


        public static ASM Load(string modelFile, UniSentFeatureEngine featureEngine)
        {
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(Path.Combine(modelFile), FileMode.Open, FileAccess.Read, FileShare.None);
            var model = (ASM)formatter.Deserialize(stream);
            model.lstm.lookupTable = model.lookupTable;
            model.featureEngine = featureEngine;
            stream.Close();
            return model;
        }

        private void Save(string modelFile)
        {
            IFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(modelFile, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, this);
            stream.Close();
        }

        public void Train(string label, List<Corpus> trainColl)
        {

            var modelName = this.GetType().Name;
            string root = Path.Combine(Constant.Root, @"model\", modelName);
            if (!Directory.Exists(root))
                Directory.CreateDirectory(root);

            var dic = featureEngine.FeatureMap();
            using (var sw = new StreamWriter(Path.Combine(root, "feature_" + label + "_list.txt")))
            {
                foreach (var pair in dic)
                {
                    sw.WriteLine("{0} {1}", pair.Key, pair.Value);
                }
            }

            int batchSize = 20;
            object _lock = new object();
            Random rng = new Random();
            List<Sentence> singleBatch = new List<Sentence>();
            List<Tuple<Sentence, Sentence>> pairBatch = new List<Tuple<Sentence, Sentence>>();
            for (int n = 1; n <= 1; n++)
            {
                float error = 0;
                int count = 0;
                DateTime begin = DateTime.Now;

                for (int c = 0; c < trainColl.Count; c++)
                {
                    Corpus corpus = trainColl[c];
                    List<Sentence> sents = new List<Sentence>();
                    foreach (var doc in corpus.Documents)
                    {
                        sents.AddRange(doc.Sentences);
                    }


                    for (int i = 0; i < sents.Count; i++)
                    {
                        singleBatch.Add(sents[i]);
                        if (singleBatch.Count == batchSize || i == sents.Count - 1)
                        {
                            Parallel.ForEach(singleBatch, sent =>
                            {
                                var layer = Build(sent);
                                var lossLayer = Loss(layer);

                                lossLayer.ForwardPropagate();
                                lossLayer.BackwardPropagate(Vector<float>.Build.Dense(new float[] { sent.GTScore }));
                                lock (_lock)
                                {
                                    error += lossLayer.Loss[0];
                                    count++;
                                }
                            });
                            ppm.Update();
                            ppm.Clear();
                            singleBatch.Clear();

                            DateTime tempend = DateTime.Now;
                            Console.Write("[Log] Round {0} Corpus {1} Sentence {2} RegressionLoss {3} Time {4}s\r", n, c + 1, count, string.Format("{0:0.00000}", error / count), tempend.Subtract(begin).TotalSeconds);
                        }


                        //int j = rng.Next(sents.Count);
                        //while (j == i || Math.Abs(sents[i].GTScore - sents[j].GTScore) < 0.01)
                        //{
                        //    j = rng.Next(sents.Count);
                        //}

                        //if (sents[i].GTScore > sents[j].GTScore)
                        //{
                        //    pairBatch.Add(new Tuple<Sentence, Sentence>(sents[i], sents[j]));
                        //}
                        //else if (sents[i].GTScore < sents[j].GTScore)
                        //{
                        //    pairBatch.Add(new Tuple<Sentence, Sentence>(sents[j], sents[i]));
                        //}

                        //if (pairBatch.Count == batchSize)
                        //{
                        //    Parallel.ForEach(pairBatch, p =>
                        //    {
                        //        var layer1 = Decoder(Encoder(p.Item1));
                        //        var layer2 = Decoder(Encoder(p.Item2));
                        //        var lossLayer = Loss(layer1, layer2, 0.01f);
                        //        lossLayer.ForwardPropagate();
                        //        lossLayer.BackwardPropagate(null);

                        //        lock (_lock)
                        //        {
                        //            error += lossLayer.Loss[0];
                        //            count++;
                        //        }
                        //    });
                        //    ppm.Update();
                        //    ppm.Clear();
                        //    pairBatch.Clear();

                        //    DateTime tempend = DateTime.Now;
                        //    Console.Write("[Log] Round {0} Corpus {1} Sentence {2} MarginLoss {3} Time {4}s\r", n, c + 1, count, string.Format("{0:0.00000}", error / count), tempend.Subtract(begin).TotalSeconds);
                        //}

                    }
                }
                Console.WriteLine();
                Console.Write("Loss: [");
                error = error / count;
                Console.Write(error);
                DateTime end = DateTime.Now;

                Console.Write("] Time: " + (end.Subtract(begin).TotalSeconds) + "s");
                Console.WriteLine();

            }

            string modelFilePath = Path.Combine(root, modelName + "." + label + ".model");
            Save(modelFilePath);
        }
    }
}
