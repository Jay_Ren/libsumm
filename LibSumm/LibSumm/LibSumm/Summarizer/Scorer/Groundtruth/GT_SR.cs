﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibSumm.Entity;
using ROUGEPackage;
using LibSumm.Utils;

namespace LibSumm.Summarizer.Scorer.Groundtruth
{
    [Serializable]
    public class GT_SR : SentenceScorer
    {
        private ROUGEUtils rouge;
        public GT_SR() {
            Setting setting = new Setting();
            setting.NgramSize = 2;
            setting.CurrentLengthType = ROUGEPackage.Setting.LengthLimitType.WORD;
            setting.SummaryLength = 300;

            rouge = new ROUGEUtils(setting);
        }

        public override float Score(Sentence sent, List<Sentence> currentSummary)
        {
            var references = new List<string>();
            foreach (var summary in sent.Doc.Corpus.References.Values)
            {
                references.Add(string.Join(" ", summary.Sentences.Select(s => { return s.Content; })));
            }

            return (float)rouge.Recall(sent.Content, references)[1];

            //return (float)RougeUtility.Evaluate(sent.Content, references)["ROURGE_2_R"];
        }
    }
}
