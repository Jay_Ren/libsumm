﻿using LibSumm.Entity;
using LibSumm.Utils;
using MSRA.NLC.Common.NLP;
using NLP;
using ROUGEPackage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static ROUGEPackage.Setting;

namespace LibSumm.Summarizer.Scorer.Groundtruth
{
   public class GT_Potential
    {

        public static void Run() {
            string duc = "duc2001";
            RougeUtility.LengthLimit = " -l 100 ";
            LengthLimitType type = Setting.LengthLimitType.WORD;
            int evallen = 100;
            int len = 150;

            StopWordHandler stop = new StopWordHandler(Path.Combine(Constant.Root, @"model\stop.dict"));
            var stanfordnlpDir = Path.Combine(Constant.Root, @"model\stanford-corenlp-models");
            string stemmerFile = Path.Combine(Constant.Root, @"model\wordnet.dict");
            var nlp = new StanfordCoreNLPTool(stanfordnlpDir, stemmerFile, "tokenize, ssplit, pos, lemma");
            var reader = new FileCorpusReader(nlp);

            var analyzer = new CorpusAnalyzer(stop);
            var trainColl = reader.Read(Path.Combine(Constant.Root, @"data\train-" + duc + ".txt"));
            var testColl = reader.Read(Path.Combine(Constant.Root, @"data\" + duc + ".txt"));

            var sw = new StreamWriter(Path.Combine(Constant.Root, @"data\train-" + duc + ".potential.rouge2r.txt"));
            foreach (var coll in trainColl) {
                GT_Potential gt = new GT_Potential(len, type, evallen);
                GT_RASR scorer = new GT_RASR();
                analyzer.Analyze(coll);
                gt.Potential("potential",coll, scorer);

                StringBuilder sb = new StringBuilder();
                foreach (var p in coll.References) {
                    sb.Append(string.Join(" ", p.Value.Sentences.Select(s => { return s.Content; })));
                    sb.Append("\r\n");
                }
                sw.WriteLine(sb.ToString().TrimEnd(new char[] { '\r','\n'}));
                sw.Flush();

                float max = -1;
                float min = 10;
                foreach (var doc in coll.Documents)
                {
                    var sents = doc.Sentences.OrderByDescending(s => { return -s.Position; });
                    foreach (var sent in sents)
                    {
                        if (sent.GTScore > max)
                        {
                            max = sent.GTScore;
                        }
                        if (sent.GTScore < min)
                        {
                            min = sent.GTScore;
                        }
                    }
                }

                int docid = 0;
                foreach (var doc in coll.Documents) {
                   var sents= doc.Sentences.OrderByDescending(s => { return -s.Position; });
                    foreach (var sent in sents) {
                        sw.WriteLine("{0}\t{1}\t{2}", (sent.GTScore - min) / (max - min), docid, sent.Content);
                        sw.Flush();
                    }
                    docid++;
                }
                Console.Write(".");
                sw.WriteLine();
            }
            sw.Close();

            sw = new StreamWriter(Path.Combine(Constant.Root, @"data\" + duc + ".potential.rouge2r.txt"));
            foreach (var coll in testColl)
            {
                GT_Potential gt = new GT_Potential(len, type, evallen);
                GT_RASR scorer = new GT_RASR();
                analyzer.Analyze(coll);
                gt.Potential("potential", coll, scorer);

                StringBuilder sb = new StringBuilder();
                foreach (var p in coll.References)
                {
                    sb.Append(string.Join(" ", p.Value.Sentences.Select(s => { return s.Content; })));
                    sb.Append("\r\n");
                }
                sw.WriteLine(sb.ToString().TrimEnd(new char[] { '\r', '\n' }));
                sw.Flush();

                float max = -1;
                float min = 10;
                foreach (var doc in coll.Documents)
                {
                    var sents = doc.Sentences.OrderByDescending(s => { return -s.Position; });
                    foreach (var sent in sents)
                    {
                        if (sent.GTScore > max) {
                            max = sent.GTScore;
                        }
                        if (sent.GTScore < min)
                        {
                            min = sent.GTScore;
                        }
                    }
                }

                int docid = 0;
                foreach (var doc in coll.Documents)
                {
                    var sents = doc.Sentences.OrderByDescending(s => { return -s.Position; });
                    foreach (var sent in sents)
                    {
                        sw.WriteLine("{0}\t{1}\t{2}", (sent.GTScore-min)/(max-min), docid, sent.Content);
                        sw.Flush();
                    }
                    docid++;
                }
                Console.Write(".");
                sw.WriteLine();
            }
            sw.Close();
        }


        private ROUGEUtils rouge;
        private int len;
        public GT_Potential(int len, LengthLimitType type, int evallen)
        {
            this.len = len;
            Setting setting = new Setting();
            setting.NgramSize = 2;
            setting.CurrentLengthType = type;
            setting.SummaryLength = evallen;

            rouge = new ROUGEUtils(setting);
        }

        public void Potential(string id, Corpus corpus, SummaryScorer scorer)
        {
            var sents_ =new List<Sentence>();
            foreach (var doc in corpus.Documents) {
                sents_.AddRange(doc.Sentences);
            }

            var references = new List<string>();
            foreach (var summary in corpus.References.Values)
            {
                references.Add(string.Join(" ", summary.Sentences.Select(s => { return s.Content; })));
            }

            foreach (var sent in sents_)
            {
                sent.GTScore = (float)rouge.Recall(sent.Content, references)[1];
                //sent.GTScore = (float)RougeUtility.Evaluate(sent.Content,references)["ROURGE_2_R"];
            }

            sents_ = sents_.OrderByDescending(s => { return s.GTScore; }).ToList();

            Parallel.For(0, sents_.Count, i =>
            {
                var idi = id + i;
                var sents = new List<Sentence>(sents_);

                var firstSent = sents[i];
                Summary summ = new Summary();
                summ.Sentences = new List<Sentence>();
                summ.Sentences.Add(firstSent);

                int clen = firstSent.Len;
                sents.Remove(firstSent);

                while (clen < len && sents.Count > 0)
                {
                    float max = -1;
                    float lscore = -1;
                    Sentence next = null;
                    foreach (var sent in sents)
                    {
                        summ.Sentences.Add(sent);
                        float score = scorer.Score(summ.Sentences);

                        float NScore = score;
                        if (NScore > max)
                        {
                            max = NScore;
                            lscore = score;
                            next = sent;
                        }
                        summ.Sentences.Remove(sent);
                    }
                    if (next != null)
                    {
                        summ.Sentences.Add(next);
                        summ.PDScore = lscore;
                        next.Rank = summ.Sentences.Count;
                        clen += next.Len;
                        sents.Remove(next);
                    }
                }
                var summstr = string.Join(" ", summ.Sentences.Select(s => { return s.Content; }));
                //var score =  (float)rouge.Recall(summstr, references)[1];
                firstSent.GTScore = (float)RougeUtility.Evaluate(summstr, references)["ROURGE_2_R"];

            });


        }
    }
}
