﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibSumm.Entity;
using LibSumm.Summarizer.Similarity;

namespace LibSumm.Summarizer.Scorer
{
    public class MMR : SummaryScorer
    {
        private ISentenceSimilarity similarity;
        private float lambda = 0.1f;
        public MMR(ISentenceSimilarity similarity, float lambda = 0.1f) {
            this.similarity = similarity;
            this.lambda = lambda;
        }

        public override float Score(List<Sentence> summary)
        {
            var modelName = this.GetType().Name;

            Corpus corpus = summary[0].Doc.Corpus;
            var sents = new List<Sentence>();
            foreach (var doc in corpus.Documents)
            {
                sents.AddRange(doc.Sentences);
            }
            sents = sents.Where(s => { return s.Len > 0&&!summary.Contains(s); }).ToList();

            float x = 0;
            float y = 0;
            //int numx = 0;
            //int numy = 0;
            foreach (var summSent in summary) {
                foreach (var sent_ in sents)
                {
                    //numx++;
                    lock (summSent.Lock)
                    {
                        if (summSent.PDScores == null)
                        {
                            summSent.PDScores = new Dictionary<string, float>();
                        }
                        if (summSent.PDScores.ContainsKey(modelName))
                        {
                            x += summSent.PDScores[modelName];
                        }
                        else
                        {
                            var s = similarity.Similarity(summSent, sent_);
                            summSent.PDScores.Add(modelName, s);
                            x += s;
                        }
                    }
                }
                foreach (var summSent_ in summary) {
                    if (summSent != summSent_) {
                        //numy++;
                        lock (summSent.Lock) {
                            if (summSent.PDScores == null)
                            {
                                summSent.PDScores = new Dictionary<string, float>();
                            }
                            if (summSent.PDScores.ContainsKey(modelName))
                            {
                                y += summSent.PDScores[modelName];
                            }
                            else
                            {
                                var s = similarity.Similarity(summSent, summSent_);
                                summSent.PDScores.Add(modelName, s);
                                y += s;
                            }
                        }
                        
                    }
                }
            }


            return x - lambda * y;

            //if (numy > 0) {
            //    return x / numx - lambda * y / numy;
            //}
            //else
            //    return x / numx;
        }
    }
}
