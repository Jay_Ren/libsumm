﻿using LibSumm.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Summarizer.Processor
{
   public interface IProcessor
    {

        void Process(Corpus corpus);
    }
}
