﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibSumm.Entity;
using Common.Utils;
using MathNet.Numerics;
using MSRA.NLC.Common.NLP;

namespace LibSumm.Summarizer.Similarity
{
    public class TFDFCosineSentenceSimilarity : ISentenceSimilarity
    {

        private bool df = false;
        private StopWordHandler stop;
        public TFDFCosineSentenceSimilarity(bool df = false,StopWordHandler stop=null) {
            this.stop = stop;
            this.df = df;
        }

        public float Similarity(Sentence sent1, Sentence sent2)
        {
            var t1 = sent1.Tokens;
            var t2 = sent2.Tokens;

            var dic1 = new Dictionary<string, double>();
            foreach (var t in t1)
            {
                if (stop!=null&&stop.IsStopWord(t.LOWER))
                    continue;
                if (!dic1.ContainsKey(t.STEM))
                {
                    dic1.Add(t.STEM, 1);
                }
                else
                {
                    dic1[t.STEM] += 1;
                }
            }

            var dic2 = new Dictionary<string, double>();
            foreach (var t in t2)
            {
                if (stop != null && stop.IsStopWord(t.LOWER))
                    continue;
                if (!dic2.ContainsKey(t.STEM))
                {
                    dic2.Add(t.STEM, 1);
                }
                else
                {
                    dic2[t.STEM] += 1;
                }
            }

            if (df)
            {
                int N = 0;
                foreach (var doc in sent1.Doc.Corpus.Documents)
                {
                    N += doc.Sentences.Count;
                }

                var dfs = sent1.Doc.Corpus.StemSF;
                var keys = dic1.Keys.ToList();
                foreach (var k in keys)
                {
                    if (dfs.ContainsKey(k))
                    {
                        dic1[k] *= Math.Log((N+1) /(dfs[k] + 1));
                    }
                }
                keys = dic2.Keys.ToList();
                foreach (var k in keys)
                {
                    if (dfs.ContainsKey(k))
                    {
                        dic2[k] *= Math.Log((N + 1) / (dfs[k] + 1));
                    }
                }

            }

            return (float)MSRA.NLC.Common.NLP.Similarity.Consine(dic1, dic2);
        }
    }
}
