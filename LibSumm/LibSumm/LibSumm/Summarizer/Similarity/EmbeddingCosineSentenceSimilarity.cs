﻿using Common.Utils;
using LibSumm.Entity;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Summarizer.Similarity
{
   public class EmbeddingCosineSentenceSimilarity :ISentenceSimilarity
    {

        private EmbeddingHolder embedding;
        public EmbeddingCosineSentenceSimilarity(EmbeddingHolder embedding) {
            this.embedding = embedding;
        }

        public float Similarity(Sentence sent1, Sentence sent2)
        {
            var t1 = sent1.Tokens;
            var t2 = sent2.Tokens;


            Vector<float> vec1 = Vector<float>.Build.Dense(embedding.Dimension, 0);
            Vector<float> vec2 = Vector<float>.Build.Dense(embedding.Dimension, 0);

            lock (embedding) {
                foreach (var t in t1)
                {
                    if (embedding.Contains(t.LOWER))
                        vec1 += Vector<float>.Build.Dense(embedding.Vectorize(t.LOWER));
                }
                vec1 /= t1.Count;

                foreach (var t in t2)
                {
                    if (embedding.Contains(t.LOWER))
                        vec2 += Vector<float>.Build.Dense(embedding.Vectorize(t.LOWER));
                }
                vec2 /= t2.Count;
            }
           

            return 1.0f-Distance.Cosine(vec1.ToArray(),vec2.ToArray());
        }
    }
}
