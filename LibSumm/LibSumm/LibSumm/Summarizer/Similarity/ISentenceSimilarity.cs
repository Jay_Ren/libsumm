﻿using LibSumm.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Summarizer.Similarity
{
    public interface ISentenceSimilarity
    {

        float Similarity(Sentence sent1, Sentence sent2);
    }
}
