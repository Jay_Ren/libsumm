﻿using LibSumm.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Features
{
   public class NERFeature : IUniSentFeature
    {
        private string[] ners = new string[] { "PERSON", "ORGANIZATION", "NUMBER", "LOCATION", "DATE", "MONEY", "MISC", "ORDINAL" };
        private string label = "NER";
        public List<string> UniLabels()
        {
            var list= new List<string>();
            foreach (var ner in ners) {
                list.Add("UniSent_" + label + "_"+ner);
            }
            return list;
        }
        public Dictionary<string, float> Extract(Sentence sent)
        {
            Dictionary<string, float> features = new Dictionary<string, float>();
            foreach (var t in sent.Tokens)
            {
                if (ners.Contains(t.NER) && !features.ContainsKey("UniSent_" + label + "_" + t.NER))
                {
                    features.Add("UniSent_" + label + "_" + t.NER, 1.0f);
                }
            }
            return features;
        }
    }
}
