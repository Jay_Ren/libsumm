﻿using LibSumm.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Features
{
    public class BiSentsFeatureEngine : FeatureEngine
    {

        private List<IBiSentsFeature> features = new List<IBiSentsFeature>();

        public void Add(IBiSentsFeature feature)
        {
            lock (features)
            {
                if (!features.Contains(feature))
                {
                    features.Add(feature);
                    var labels = feature.BiLabels();
                    foreach (var label in labels)
                    {
                        base.Add(label);
                    }
                }
            }

        }

        public override Dictionary<string, float> Extract(params Sentence[] sents)
        {
            Dictionary<string, float> results = new Dictionary<string, float>();

            var sent1 = sents[0];

            if (sents.Length < 2) {
                return results;
            }

            var sent2 = sents[1];

            Parallel.ForEach(features, f => {
                var temp = f.Extract(sent1, sent2);

                lock (results)
                {
                    foreach (var pair in temp)
                    {
                        results.Add(pair.Key, pair.Value);
                    }
                }
            });

            return results;
        }
    }
}
