﻿using LibSumm.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Features
{
    public class UniSentFeatureEngine : FeatureEngine
    {

        private List<IUniSentFeature> features = new List<IUniSentFeature>();

        public void Add(IUniSentFeature feature)
        {
            lock (features)
            {
                if (!features.Contains(feature))
                {
                    features.Add(feature);
                    var labels = feature.UniLabels();
                    foreach (var label in labels)
                    {
                        base.Add(label);
                    }
                }
            }

        }

        public override Dictionary<string, float> Extract(params Sentence[] sents)
        {
            var sent = sents[0];
            Dictionary<string, float> results = new Dictionary<string, float>();

            Parallel.ForEach(features, f => {
                var temp = f.Extract(sent);
                if (temp == null)
                {
                    Console.WriteLine(f);
                }

                lock (results)
                {
                    foreach (var pair in temp)
                    {
                        results.Add(pair.Key, pair.Value);
                    }
                }

            });

            return results;
        }
    }
}
