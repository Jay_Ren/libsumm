﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibSumm.Entity;
using Common.Utils;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics;

namespace LibSumm.Features
{
    public class EmbeddingContextFeature : IUniSentFeature
    {
        private string label = "EmbCF";

        private EmbeddingHolder embedding = null;
        private int embeddingDimension;
        private int numContext = 5;

        public EmbeddingContextFeature(EmbeddingHolder embedding,int numContext = 5)
        {
            this.embedding = embedding;
            embeddingDimension = this.embedding.Dimension;
            this.numContext = numContext;
        }

        private Vector<float> SentenceModel(Sentence sent)
        {
            Vector<float> v = Vector<float>.Build.Dense(embeddingDimension,0);
            int count = 0;
            foreach (var t in sent.Tokens)
            {
                if (embedding.Contains(t.LOWER))
                {
                    v += Vector<float>.Build.Dense(embedding.Vectorize(t.LOWER));
                    count++;
                }
            }
            if (count == 0)
                return v;
            else
                return v / count;
        }
        private Vector<float> SentenceModel(List<string> words) {
            Vector<float> v = Vector<float>.Build.Dense(embeddingDimension, 0);
            int count = 0;
            foreach (var t in words)
            {
                if (embedding.Contains(t))
                {
                    v += Vector<float>.Build.Dense(embedding.Vectorize(t));
                    count++;
                }
            }
            if (count == 0)
                return v;
            else
                return v / count;
        }
       
        public Dictionary<string, float> Extract(Sentence sent)
        {
            var sents = sent.Doc.Sentences.OrderByDescending(s => { return -s.Position; }).ToList();
            var v = SentenceModel(sent);
            Dictionary<string, float> features = new Dictionary<string, float>();

            List<string> pwords = new List<string>();
            for (int i = Math.Max(0, sent.Position - 1 - numContext); i < sent.Position - 1; i++)
            {
                pwords.AddRange(sents[i].Tokens.Select(t=> { return t.LOWER; }));
            }
            if (pwords.Count > 0)
            {
                var p = SentenceModel(pwords);
                features.Add("P_" + label, 1.0f - Distance.Cosine(v.ToArray(), p.ToArray()));
            }
            else {
                features.Add("P_" + label, 1);
            }

            List<string> fwords = new List<string>();
            for (int i = sent.Position; i < Math.Min(sents.Count, sent.Position + numContext); i++)
            {
                fwords.AddRange(sents[i].Tokens.Select(t => { return t.LOWER; }));
            }
            if (fwords.Count > 0)
            {
                var f = SentenceModel(fwords);
                features.Add("F_" + label, 1.0f - Distance.Cosine(v.ToArray(), f.ToArray()));
            }
            else
            {
                features.Add("F_" + label, 1);
            }

            return features;
        }

        public List<string> UniLabels()
        {
            return new List<string>() { "P_" + label, "F_" + label };
        }
    }
}
