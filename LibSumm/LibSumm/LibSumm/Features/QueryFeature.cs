﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibSumm.Entity;
using Common.Utils;
using MSRA.NLC.Common.NLP;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics;

namespace LibSumm.Features
{
    public class QueryFeature : IUniSentFeature
    {
        private string label = "Query";
        private EmbeddingHolder embedding;
        private StopWordHandler stop;

        public QueryFeature(EmbeddingHolder embedding, StopWordHandler stop)
        {
            this.embedding = embedding;
            this.stop = stop;
        }
        
        public Dictionary<string, float> Extract(Sentence sent)
        {
            Dictionary<string, float> features = new Dictionary<string, float>();

            var qvec = Vector<float>.Build.Dense(embedding.Dimension,0);
            var qdic = new Dictionary<string, double>();
            int count = 0;
            foreach (var q in sent.Doc.Corpus.Queries) {
                foreach (var t in q.Tokens) {

                    if (stop.IsStopWord(t.LOWER))
                        continue;

                    if (embedding.Contains(t.LOWER))
                    {
                        qvec += Vector<float>.Build.Dense(embedding.Vectorize(t.LOWER));
                        count++;
                    }
                    if (!qdic.ContainsKey(t.LOWER))
                    {
                        qdic.Add(t.LOWER, 1);
                    }
                    else {
                        qdic[t.LOWER] += 1;
                    }
                }
            }

            if (count == 0)
            {
                features.Add("UniSent_" + label + "_Cos_TF", 0);
                features.Add("UniSent_" + label + "_Cos_Emb", 0);
                features.Add("UniSent_" + label + "_P", 0);
                features.Add("UniSent_" + label + "_R", 0);
                return features;
            }

            qvec = qvec / count;


            var svec = Vector<float>.Build.Dense(embedding.Dimension, 0);
            var sdic = new Dictionary<string, double>();
            count = 0;
            foreach (var t in sent.Tokens)
            {
                if (stop.IsStopWord(t.LOWER))
                    continue;
                if (embedding.Contains(t.LOWER))
                {
                    svec += Vector<float>.Build.Dense(embedding.Vectorize(t.LOWER));
                    count++;
                }

                if (!sdic.ContainsKey(t.LOWER))
                {
                    sdic.Add(t.LOWER, 1);
                }
                else
                {
                    sdic[t.LOWER] += 1;
                }
            }

            if (count == 0)
            {
                features.Add("UniSent_" + label + "_Cos_TF", 0);
                features.Add("UniSent_" + label + "_Cos_Emb", 0);
                features.Add("UniSent_" + label + "_P", 0);
                features.Add("UniSent_" + label + "_R", 0);
                return features;
            }

            svec = svec / count;

            features.Add("UniSent_" + label + "_Cos_TF", (float)Similarity.Consine(qdic, sdic));
            features.Add("UniSent_" + label + "_Cos_Emb", (float)(1.0f-Distance.Cosine(qvec.ToArray(),svec.ToArray())));

            var dic= Match(qdic.Keys, sdic.Keys);
            foreach (var p in dic) {
                features.Add(p.Key,p.Value);
            }

            return features;
        }

        public List<string> UniLabels()
        {
            return new List<string>() { "UniSent_" + label + "_Cos_TF", "UniSent_" + label + "_Cos_Emb","UniSent_" + label + "_P", "UniSent_" + label + "_R" };
        }

        private Dictionary<string, float> Match(ICollection<string> sent, ICollection<string> reference)
        {
            Dictionary<string, float> simMap = new Dictionary<string, float>();
            if (sent.Count == 0 || reference.Count == 0)
            {
                simMap.Add("UniSent_" + label + "_P", 0);
                simMap.Add("UniSent_" + label + "_R", 0);
                return simMap;
            }

            int matchCount = 0;
            foreach (string word in sent)
            {
                if (reference.Contains(word))
                {
                    matchCount++;
                }
            }
            float matchPrecision = matchCount / (float)sent.Count;
            simMap.Add("UniSent_" + label + "_P", matchPrecision);

            float matchRecall = matchCount / (float)reference.Count;
            simMap.Add("UniSent_" + label + "_R", matchRecall);

            return simMap;
        }
    }
}
