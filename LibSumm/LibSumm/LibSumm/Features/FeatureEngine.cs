﻿using LibSumm.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Features
{
    public abstract class FeatureEngine
    {

        protected Dictionary<string, int> feature_map1 = new Dictionary<string, int>();
        protected Dictionary<int, string> feature_map2 = new Dictionary<int, string>();

        public abstract Dictionary<string, float> Extract(params Sentence[] sents);

        public int Dimension { get { return feature_map1.Count; } }

        //public static string Format2String(List<string> keySequence,Dictionary<string, float> features)
        //{
        //    StringBuilder sbr = new StringBuilder();
        //    for (int i = 0; i < keySequence.Count; i++) {
        //        var id = keySequence[i];
        //        sbr.Append(i + ":" + features[id] + " ");
        //    }
        //    return sbr.ToString().TrimEnd();
        //}

        public string Format2String(Dictionary<string, float> features)
        {
            StringBuilder sbr = new StringBuilder();
            lock (feature_map1)
            {
                for (int i = 1; i <= feature_map1.Count; i++) {
                    var key = feature_map2[i];
                    if (features.ContainsKey(key)&& features[key]!=0) {
                        sbr.Append(i + ":" + features[key] + " ");
                    }
                }
                //foreach (var pair in features)
                //{
                //    if (feature_map1.ContainsKey(pair.Key))
                //    {
                //        var id = feature_map1[pair.Key];
                //        sbr.Append(id + ":" + pair.Value + " ");
                //    }
                //}
            }

            return sbr.ToString().TrimEnd();
        }

        public float[] Format2Array(Dictionary<string, float> features)
        {
            float[] values = new float[feature_map1.Count];
            lock (feature_map1)
            {
                foreach (var pair in features)
                {
                    if (feature_map1.ContainsKey(pair.Key))
                    {
                        var id = feature_map1[pair.Key];
                        values[id - 1] = pair.Value;
                    }
                }
            }

            return values;
        }

        public Dictionary<string, int> FeatureMap()
        {
            return feature_map1;
        }

        protected void Add(string featureLabel)
        {
            lock (feature_map1)
            {
                if (!feature_map1.ContainsKey(featureLabel))
                {
                    var id = feature_map1.Count + 1;
                    feature_map1.Add(featureLabel, id);
                    feature_map2.Add(id, featureLabel);
                }
            }

        }

        public int Index(string featureLabel)
        {
            if (!feature_map1.ContainsKey(featureLabel))
            {
                return feature_map1[featureLabel];
            }
            return -1;
        }

        public string Name(int index)
        {
            if (!feature_map2.ContainsKey(index))
            {
                return feature_map2[index];
            }
            return null;

        }

    }


    public class FeatureEngineWrapper : FeatureEngine
    {

        private List<FeatureEngine> engines = new List<FeatureEngine>();

        public FeatureEngineWrapper(params FeatureEngine[] engines)
        {
            this.engines.AddRange(engines);

            for (int i = 0; i < this.engines.Count; i++)
            {
               var features= this.engines[i].FeatureMap();
                foreach (var f in features) {
                    base.Add(f.Key);
                }
            }
         }
        public override Dictionary<string, float> Extract(params Sentence[] sents)
        {
            Dictionary<string, float> results = new Dictionary<string, float>();
            for (int i = 0; i < engines.Count; i++) {
                var features= engines[i].Extract(sents);
                foreach (var p in features) {
                    results.Add(p.Key,p.Value);
                }
            }

            return results;
        }
    }
}
