﻿using LibSumm.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Features
{
   public class LengthFeature : IUniSentFeature
    {
        private string label = "Len";
        public List<string> UniLabels()
        {
            return new List<string>() { "UniSent_" + label };
        }
        public Dictionary<string, float> Extract(Sentence sent)
        {
            Dictionary<string, float> features = new Dictionary<string, float>();
            features.Add("UniSent_" + label, 1.0f / sent.Len);
            return features;
        }
    }
}
