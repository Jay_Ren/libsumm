﻿using LibSumm.Entity;
using MSRA.NLC.Common.NLP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Features
{
   public class StopFeature:IUniSentFeature,IBiSentsFeature
    {
        private StopWordHandler stop = null;
        private string label = "Stop";

        public StopFeature(StopWordHandler stop) {
            this.stop = stop;
        }

        public List<string> UniLabels()
        {
            return new List<string>() { "UniSent_" + label };
        }
        public Dictionary<string, float> Extract(Sentence sent)
        {
            Dictionary<string, float> features = new Dictionary<string, float>();

            int score = 0;
            foreach (var token in sent.Tokens)
            {
                string word = token.LOWER;
                if (stop.IsStopWord(word))
                {
                    score += 1;
                }
            }

            float value = 1.0f - score / (float)sent.Len;
            features.Add("UniSent_" + label, value);

            return features;
        }

        public List<string> BiLabels()
        {
            return new List<string>() { "BiSent_" + label };
        }
        public Dictionary<string, float> Extract(Sentence sent1, Sentence sent2) {
            Dictionary<string, float> features = new Dictionary<string, float>();
            

            var words2 = sent2.Tokens.Select(t => { return t.LOWER; });
            var overlap = sent1.Tokens.Select(t => { return t.LOWER; }).Where(w => words2.Contains(w)).ToArray();

            int score = 0;
            foreach (var word in overlap)
            {
                if (stop.IsStopWord(word))
                {
                    score += 1;
                }
            }

            if (overlap.Length > 0)
            {
                float value = 1.0f - score / (float)overlap.Length;
                features.Add("BiSent_" + label, value);
            }
            else {
                features.Add("BiSent_" + label, 0);
            }
           

            return features;
        }
    }
}
