﻿using LibSumm.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Features
{
    public class ListSentsFeatureEngine : FeatureEngine
    {

        private List<IListSentsFeature> features = new List<IListSentsFeature>();

        public void Add(IListSentsFeature feature)
        {
            if (!features.Contains(feature))
            {
                features.Add(feature);
                var labels = feature.ListLabels();
                foreach (var label in labels)
                {
                    base.Add(label);
                }
            }
        }

        public override Dictionary<string, float> Extract(params Sentence[] sents)
        {
            Dictionary<string, float> results = new Dictionary<string, float>();

            Parallel.ForEach(features, f =>
            {
                var temp = f.Extract(sents.ToList());

                lock (results)
                {
                    foreach (var pair in temp)
                    {
                        results.Add(pair.Key, pair.Value);
                    }
                }
            });

            return results;
        }
    }
}
