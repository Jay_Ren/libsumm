﻿using LibSumm.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Features
{
    
    public interface IUniSentFeature
    {
        List<string> UniLabels();
        Dictionary<string, float> Extract(Sentence sent);

    }

    public interface IBiSentsFeature
    {
        List<string> BiLabels();
        Dictionary<string, float> Extract(Sentence sent1,Sentence sent2);
    }

    public interface IListSentsFeature
    {
        List<string> ListLabels();
        Dictionary<string, float> Extract(IList<Sentence> sents);
    }
}
