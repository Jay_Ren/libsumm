﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibSumm.Entity;
using MSRA.NLC.Common.NLP;
using Common.Utils;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics;

namespace LibSumm.Features
{
    public class CoverageFeature : IUniSentFeature
    {
        private string label = "Cov";

        private StopWordHandler stop;
        private EmbeddingHolder embedding;

        public CoverageFeature(EmbeddingHolder embedding, StopWordHandler stop) {
            this.embedding = embedding;
            this.stop = stop;
        }

        public Dictionary<string, float> Extract(Sentence sent)
        {

            Dictionary<string, float> features = new Dictionary<string, float>();

            int count = 0;
            Dictionary<string, double> dic1 = new Dictionary<string, double>();
            HashSet<string> set1 = new HashSet<string>();
            var vec1=Vector<float>.Build.Dense(embedding.Dimension,0);

            foreach (var p in sent.Doc.StemTF)
            {
                if (stop.IsStopWord(p.Key))
                    continue;
                if (!dic1.ContainsKey(p.Key))
                    dic1.Add(p.Key, p.Value);
            }
            foreach (var p in sent.Doc.TF)
            {
                if (stop.IsStopWord(p.Key))
                    continue;
                if (!set1.Contains(p.Key))
                {
                    set1.Add(p.Key);
                }
            }

            foreach (var p in set1)
            {
                if (embedding.Contains(p))
                {
                    vec1 += Vector<float>.Build.Dense(embedding.Vectorize(p));
                    count++;
                }
            }

            if (count > 0)
                vec1 /= count;

            if (count == 0) {
                features.Add("UniSent_" + label + "_Bow",0);
                features.Add("UniSent_" + label + "_Emb",0);
                return features;
            }

            count = 0;
            Dictionary<string, double> dic2 = new Dictionary<string, double>();
            var vec2 = Vector<float>.Build.Dense(embedding.Dimension, 0);
            foreach (var t in sent.Tokens) {

                if (stop.IsStopWord(t.LOWER))
                    continue;

                if (!dic2.ContainsKey(t.STEM)) {
                    dic2.Add(t.STEM,1);
                }
                if (embedding.Contains(t.LOWER))
                {
                    vec2 += Vector<float>.Build.Dense(embedding.Vectorize(t.LOWER));
                    count++;
                }
            }
            if (count > 0)
                vec2 /= count;

            if (count == 0)
            {
                features.Add("UniSent_" + label + "_Bow", 0);
                features.Add("UniSent_" + label + "_Emb", 0);
                return features;
            }

            features.Add("UniSent_" + label + "_Bow", (float)Similarity.Consine(dic1,dic2));
            features.Add("UniSent_" + label + "_Emb", 1.0f-Distance.Cosine(vec1.ToArray(), vec2.ToArray()));
            return features;
        }

        public List<string> UniLabels()
        {
            return new List<string>() { "UniSent_" + label+"_Bow", "UniSent_" + label + "_Emb" };
        }
    }
}
