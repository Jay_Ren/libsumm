﻿using Common.Utils;
using LibSumm.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Features
{
   public class EmbeddingFeature : IUniSentFeature, IBiSentsFeature
    {
        private string label = "Emb";

        private EmbeddingHolder embedding = null;
        private int embeddingDimension;

        public EmbeddingFeature(EmbeddingHolder embedding)
        {
            this.embedding = embedding;
            embeddingDimension = this.embedding.Dimension;
        }
        private bool IsPunctuation(string word)
        {
            foreach (var ch in word)
                if (char.IsLetterOrDigit(ch))
                    return false;

            return true;
        }

        public List<string> UniLabels()
        {
            var list= new List<string>();

            for (int k = 0; k < embeddingDimension; ++k)
                list.Add("UniSent_" + label + "_S_" + k);
            //for (int k = 0; k < embeddingDimension; ++k)
            //    list.Add("UniSent_" + label + "_D_" + k);
            return list;
        }
        public Dictionary<string, float> Extract(Sentence sent)
        {
            Dictionary<string, float> features = new Dictionary<string, float>();

            IList<NLP.Token> tokens = sent.Tokens;
            if (tokens == null || tokens.Count == 0)
                return features;

            float[] mean = new float[embeddingDimension];
            int real_count = 0;
            int i = 0;
            while (i < tokens.Count)
            {
                string wordInOriginalForm = tokens[i].LOWER;

                string key = wordInOriginalForm;

                if (!embedding.Contains(key))
                {
                    ++i;
                    continue;
                }

                ++real_count;

                float[] vec = null;
                lock (embedding)
                    vec = embedding.Vectorize(key);

                for (int k = 0; k < embeddingDimension; ++k)
                {
                    float value = vec[k];
                    mean[k] += value;
                }
                ++i;
            }

            if (real_count <= 0)
                return features;

            for (int k = 0; k < embeddingDimension; ++k)
                mean[k] /= real_count;

            for (int k = 0; k < embeddingDimension; ++k)
                features.Add("UniSent_" + label+"_S_" + k, mean[k]);


           //var words = sent.Doc.TF.Keys.ToArray();
           // if (tokens == null || tokens.Count == 0)
           //     return features;

           // mean = new float[embeddingDimension];
           // real_count = 0;
           // i = 0;
           // while (i < words.Length)
           // {
           //     string wordInOriginalForm = words[i];

           //     string key = wordInOriginalForm;

           //     if (!embedding.Contains(key))
           //     {
           //         ++i;
           //         continue;
           //     }

           //     ++real_count;

           //     float[] vec = null;
           //     lock (embedding)
           //         vec = embedding.Vectorize(key);

           //     for (int k = 0; k < embeddingDimension; ++k)
           //     {
           //         float value = vec[k];
           //         mean[k] += value;
           //     }
           //     ++i;
           // }

           // if (real_count <= 0)
           //     return features;

           // for (int k = 0; k < embeddingDimension; ++k)
           //     mean[k] /= real_count;

           // for (int k = 0; k < embeddingDimension; ++k)
           //     features.Add("UniSent_" + label + "_D_" + k, mean[k]);



            return features;
        }

        public List<string> BiLabels()
        {
            return new List<string>() { "BiSent_" + label };
        }
        public Dictionary<string, float> Extract(Sentence sent1, Sentence sent2)
        {
            Dictionary<string, float> features = new Dictionary<string, float>();

            var lemmas1 = sent1.Tokens.Select(x => x.LOWER);
            var lemmas2 = sent2.Tokens.Select(x => x.LOWER);

            float[] vectorS = embedding.Vectorize(lemmas1);
            float[] vectorR = embedding.Vectorize(lemmas2);
            float product = 0;
            for (int i = 0; i < vectorS.Length; i++)
            {
                product += vectorS[i] * vectorR[i];
            }
            if (product == 0)
            {
                features.Add("BiSent_" + label, 0);
                return features;
            }

            float squareS = vectorS.Sum(x => x * x);
            float squareR = vectorR.Sum(x => x * x);
            float cos = product / (float)Math.Sqrt(squareS * squareR);
            features.Add("BiSent_" + label, cos);
            return features;
        }
    }
}
