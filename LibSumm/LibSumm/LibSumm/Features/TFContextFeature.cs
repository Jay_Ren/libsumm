﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibSumm.Entity;
using Common.Utils;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics;
using MSRA.NLC.Common.NLP;

namespace LibSumm.Features
{
    public class TFContextFeature : IUniSentFeature
    {
        private string label = "TFCF";

        private int numContext = 5;

        public TFContextFeature(int numContext = 5)
        {
            this.numContext = numContext;
        }

        private Dictionary<string,double> SentenceModel(Sentence sent)
        {
            Dictionary<string, double> dic = new Dictionary<string, double>();
            foreach (var t in sent.Tokens)
            {
                if (!dic.ContainsKey(t.LOWER))
                {
                    dic.Add(t.LOWER, 1);
                }
                else
                {
                    dic[t.LOWER] += 1;
                }
            }
           return dic;
        }
        private Dictionary<string, double> SentenceModel(List<string> words) {
            Dictionary<string, double> dic = new Dictionary<string, double>();
            foreach (var t in words)
            {
                if (!dic.ContainsKey(t))
                {
                    dic.Add(t, 1);
                }
                else
                {
                    dic[t] += 1;
                }
            }
            return dic;
        }
       
        public Dictionary<string, float> Extract(Sentence sent)
        {
            var sents = sent.Doc.Sentences.OrderByDescending(s => { return -s.Position; }).ToList();
            var v = SentenceModel(sent);
            Dictionary<string, float> features = new Dictionary<string, float>();

            List<string> pwords = new List<string>();
            for (int i = Math.Max(0, sent.Position - 1 - numContext); i < sent.Position - 1; i++)
            {
                pwords.AddRange(sents[i].Tokens.Select(t=> { return t.LOWER; }));
            }
            if (pwords.Count > 0)
            {
                var p = SentenceModel(pwords);
                features.Add("P_" + label, (float)Similarity.Consine(v, p));
            }
            else {
                features.Add("P_" + label, 1);
            }

            List<string> fwords = new List<string>();
            for (int i = sent.Position; i < Math.Min(sents.Count, sent.Position + numContext); i++)
            {
                fwords.AddRange(sents[i].Tokens.Select(t => { return t.LOWER; }));
            }
            if (fwords.Count > 0)
            {
                var f = SentenceModel(fwords);
                features.Add("F_" + label, (float)Similarity.Consine(v, f));
            }
            else
            {
                features.Add("F_" + label, 1);
            }

            return features;
        }

        public List<string> UniLabels()
        {
            return new List<string>() { "P_" + label, "F_" + label };
        }
    }
}
