﻿using Common.Utils;
using LibSumm.Entity;
using MSRA.NLC.Common.NLP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Features
{
   public class SimilarityFeature : IBiSentsFeature
    {
         private string label = "Sim";

        private StopWordHandler stop = null;

        public SimilarityFeature(StopWordHandler stop)
        {
            this.stop = stop;
        }

        private bool IsPunctuation(string word)
        {
            foreach (var ch in word)
                if (char.IsLetterOrDigit(ch))
                    return false;

            return true;
        }
        public List<string> BiLabels()
        {
            return new List<string>() { "BiSent_" + label+"_Cos", "BiSent_" + label + "_P", "BiSent_" + label + "_R" };
        }
        public Dictionary<string, float> Extract(Sentence sent1, Sentence sent2)
        {
            Dictionary<string, float> features = new Dictionary<string, float>();
           

            Dictionary<string, int> tf = sent1.Doc.Corpus.StemTF;
            Dictionary<string, int> sf = sent1.Doc.Corpus.StemSF;

            Dictionary<string, double> vec1 = new Dictionary<string, double>();
            foreach (var token in sent1.Tokens)
            {
                string word = token.LOWER;
                if (stop.IsStopWord(word))
                    continue;

                if (IsPunctuation(word))
                    continue;

                word = token.STEM;
                vec1[word] = tf[word] / (float)Math.Log(sf[word] + 1);
            }


            Dictionary<string, double> vec2 = new Dictionary<string, double>();
            foreach (var token in sent2.Tokens)
            {
                string word = token.LOWER;
                if (stop.IsStopWord(word))
                    continue;

                if (IsPunctuation(word))
                    continue;

                word = token.STEM;
                vec2[word] = tf[word] / (float)Math.Log(sf[word] + 1);
            }

            float score = (float)MSRA.NLC.Common.NLP.Similarity.Consine(vec1, vec2);
            features.Add("BiSent_" + label+"_Cos", score);


            string[] lemmas1 = sent1.Tokens.Select(x => x.LOWER).Where(x => !stop.IsStopWord(x)).ToArray();
            string[] lemmas2 = sent2.Tokens.Select(x => x.LOWER).Where(x => !stop.IsStopWord(x)).ToArray();
            var simMap = Match(lemmas1, lemmas2);
            foreach (var p in simMap)
            {
                features.Add(p.Key, p.Value);
            }

            return features;
        }

        
        private Dictionary<string, float> Match(string[] sent, string[] reference)
        {
            Dictionary<string, float> simMap = new Dictionary<string, float>();
            if (sent.Length == 0 || reference.Length == 0)
            {
                simMap.Add("BiSent_" + label+"_P", 0);
                simMap.Add("BiSent_" + label + "_R", 0);
                return simMap;
            }

            HashSet<string> refSet = new HashSet<string>(reference);
            int matchCount = 0;
            foreach (string word in sent)
            {
                if (refSet.Contains(word))
                {
                    matchCount++;
                }
            }
            float matchPrecision = matchCount / (float)sent.Length;
            simMap.Add("BiSent_" + label + "_P", matchPrecision);

            float matchRecall = matchCount / (float)reference.Length;
            simMap.Add("BiSent_" + label + "_R", matchRecall);

            return simMap;
        }
    }
}
