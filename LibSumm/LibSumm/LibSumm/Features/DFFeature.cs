﻿using LibSumm.Entity;
using MSRA.NLC.Common.NLP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Features
{
   public class DFFeature : IUniSentFeature, IBiSentsFeature
    {
        private string label = "DF";

        private StopWordHandler stop = null;

        public DFFeature(StopWordHandler stop)
        {
            this.stop = stop;
        }
        private bool IsPunctuation(string word)
        {
            foreach (var ch in word)
                if (char.IsLetterOrDigit(ch))
                    return false;

            return true;
        }

        public List<string> UniLabels()
        {
            return new List<string>() { "UniSent_" + label };
        }
        public Dictionary<string, float> Extract(Sentence sent)
        {
            Dictionary<string, float> features = new Dictionary<string, float>();

            Dictionary<string, int> df = sent.Doc.Corpus.StemDF;

            float df_sum = 0;
            float score = 0;

            foreach (var token in sent.Tokens)
            {
                string word = token.LOWER;
                if (stop.IsStopWord(word))
                    continue;

                if (IsPunctuation(word))
                    continue;

                word = token.STEM;
                if (!df.ContainsKey(word))
                    continue;

                df_sum += df[word];
            }

            int count = sent.Len;
            if (count > 0)
                score = df_sum / count;

            features.Add("UniSent_" + label, score);

            return features;
        }

        public List<string> BiLabels()
        {
            return new List<string>() { "BiSent_" + label };
        }
        public Dictionary<string, float> Extract(Sentence sent1, Sentence sent2)
        {
            Dictionary<string, float> features = new Dictionary<string, float>();

            var words2 = sent2.Tokens.Select(t => { return t.STEM; });
            var overlap = sent1.Tokens.Select(t => { return t.STEM; }).Where(w => words2.Contains(w)).ToArray();
            Dictionary<string, int> df = sent1.Doc.Corpus.StemDF;

            float df_sum = 0;
            float score = 0;

            foreach (var word in overlap)
            {
                if (!df.ContainsKey(word))
                    continue;

                df_sum += df[word];
            }

            int count = overlap.Length;
            if (count > 0)
                score = df_sum / count;

            if (score > 0)
                score = 1 / score;

            features.Add("BiSent_" + label, score);

            return features;
        }
    }
}
