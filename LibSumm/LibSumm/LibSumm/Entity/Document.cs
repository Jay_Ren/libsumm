﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Entity
{
    public class Document
    {

        public Dictionary<string, int> StemTF { get; set; }
        public Dictionary<string, int> StemSF { get; set; }

        public Dictionary<string, int> TF { get; set; }
        public Dictionary<string, int> SF { get; set; }

        public List<Sentence> Sentences { get; set; }

        public string ID { get; set; }

        public Sentence Title { get; set; }

        public Corpus Corpus { get; set; }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
