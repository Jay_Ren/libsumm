﻿using Newtonsoft.Json;
using NLP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Entity
{

    public class Sentence
    {
        public string ID;
        public string Content { get; set; }
        public List<Token> Tokens { get; set; }
        public float GTScore { get; set; }
        public float PDScore { get; set; }
        public int Rank { get; set; }
        public int Position { get; set; }
        public int Len { get; set; }
        public string ReferenceID { get; set; }
        public string ParserTree { get; set; }

        //
        public object Lock = new object();
        public Document Doc { get; set; }
        public Dictionary<string, float> PDScores { get; set; }

        public Sentence(string id, string content, List<Token> tokens, float gtscore, float pdscore, int rank, int position, int len, string reference)
        {
            this.ID = id;
            this.Content = content;
            this.Tokens = tokens;
            this.GTScore = gtscore;
            this.PDScore = pdscore;
            this.Rank = rank;
            this.Position = position;
            this.Len = len;
            this.ReferenceID = reference;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

}
