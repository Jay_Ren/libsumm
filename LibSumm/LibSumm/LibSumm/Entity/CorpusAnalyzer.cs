﻿using MSRA.NLC.Common.NLP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Entity
{
    public class CorpusAnalyzer
    {
        private StopWordHandler stop = null;
        public CorpusAnalyzer(StopWordHandler stop)
        {
            this.stop = stop;
        }

        public void Analyze(Corpus corpus)
        {
            Dictionary<string, int> stemtf = new Dictionary<string, int>();
            Dictionary<string, int> stemdf = new Dictionary<string, int>();
            Dictionary<string, int> stemsf = new Dictionary<string, int>();
            Dictionary<string, int> tf = new Dictionary<string, int>();
            Dictionary<string, int> df = new Dictionary<string, int>();
            Dictionary<string, int> sf = new Dictionary<string, int>();

            int N = 0;
            foreach (var doc in corpus.Documents)
            {
                Analyze(doc);

                foreach (var item in doc.StemTF)
                {
                    Add(stemtf, item.Key, item.Value);
                    Add(stemdf, item.Key, 1);
                }
                foreach (var item in doc.TF)
                {
                    Add(tf, item.Key, item.Value);
                    Add(df, item.Key, 1);
                }

                foreach (var item in doc.StemSF) {
                    Add(stemsf, item.Key, item.Value);
                }
                foreach (var item in doc.SF)
                {
                    Add(sf, item.Key, item.Value);
                }

                N += doc.Sentences.Count;
            }

            corpus.StemTF = stemtf;
            corpus.StemDF = stemdf;
            corpus.StemSF = stemsf;
            corpus.TF = tf;
            corpus.DF = df;
            corpus.SF = sf;
        }

        public void Analyze(Document doc)
        {
            Dictionary<string, int> stemtf = new Dictionary<string, int>();
            Dictionary<string, int> stemsf = new Dictionary<string, int>();
            Dictionary<string, int> tf = new Dictionary<string, int>();
            Dictionary<string, int> sf = new Dictionary<string, int>();

            IList<Sentence> sents = doc.Sentences;
            int scount = sents.Count;

            foreach (var sent in doc.Sentences)
            {
                //if (sent.Doc == null)
                //{
                //    Console.WriteLine(sent.ID);
                //}

                HashSet<string> stem = new HashSet<string>();
                HashSet<string> words = new HashSet<string>();
                foreach (var token in sent.Tokens)
                {
                    string word = token.LOWER;
                    if (stop.IsStopWord(word))
                        continue;

                    bool valid = IsValidWord(word);
                    if (!valid)
                        continue;

                    Add(tf, word, 1);
                    words.Add(word);

                    word = token.STEM;
                    Add(stemtf, word, 1);
                    stem.Add(word);
                }
                foreach (var word in stem)
                {
                    Add(stemsf, word, 1);
                }
                foreach (var word in words)
                {
                    Add(sf, word, 1);
                }

                //sent.Vec = local;//just tf here
            }

            if (doc.Title != null)
            {
                foreach (var token in doc.Title.Tokens)
                {
                    Add(stemtf, token.STEM, 1);
                    Add(tf, token.LOWER, 1);
                }
            }

            doc.StemTF = stemtf;
            doc.StemSF = stemsf;
            doc.TF = tf;
            doc.SF = sf;
        }

        private void Add(Dictionary<string, double> coll, string word, double value)
        {
            if (!coll.ContainsKey(word))
                coll.Add(word, value);
            else
                coll[word] += value;
        }

        private void Add(Dictionary<string, int> coll, string word, int value)
        {
            if (!coll.ContainsKey(word))
                coll.Add(word, value);
            else
                coll[word] += value;
        }

        private bool IsValidWord(string word)
        {
            foreach (char ch in word)
            {
                if (char.IsLetterOrDigit(ch))
                    return true;
            }

            return false;
        }
    }
}
