﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Entity
{
    public class Corpus
    {
        public Dictionary<string, int> StemTF { get; set; }

        public Dictionary<string, int> StemDF { get; set; }

        public Dictionary<string, int> StemSF { get; set; }

        public Dictionary<string, int> TF { get; set; }

        public Dictionary<string, int> DF { get; set; }

        public Dictionary<string, int> SF { get; set; }

        public List<Document> Documents { get; set; }

        public List<Sentence> Queries { get; set; }

        public Sentence Query { get; set; }

        public Dictionary<string, Summary> References { get; set; }

        public object Lock = new object();
        public Dictionary<string, Summary> PDSummaries { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
