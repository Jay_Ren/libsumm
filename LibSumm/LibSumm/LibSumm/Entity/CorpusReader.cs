﻿using LibSumm.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Entity
{
   public interface CorpusReader
    {
       List<Corpus> Read(string str, int limit = 10000); 
    }
}
