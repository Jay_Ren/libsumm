﻿using Common.Utils;
using NLP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Entity
{
    public class TextCorpusReader : CorpusReader
    {
        private INLPTool nlp;
        public TextCorpusReader(INLPTool nlp)
        {
            this.nlp = nlp;
        }
        public List<Corpus> Read(string str, int limit = 10000)
        {
            List<Corpus> results = new List<Corpus>();
            Corpus corpus = new Corpus();
            Document doc = new Document();
            doc.Corpus = corpus;
            doc.Sentences = new List<Sentence>();
            corpus.Documents = new List<Document>();
            corpus.Documents.Add(doc);
            results.Add(corpus);
           var sents= nlp.SplitSentence(str);

            int count = 0;
            foreach (var sent in sents) {
                count++;
               var tokens= nlp.Tokenize(sent);
                Sentence s = new Sentence(CommonUtils.MD5(sent+ count),sent,tokens,0,0,0,count,tokens.Count,null);
                s.Doc = doc;
                doc.Sentences.Add(s);
            }


            return results;
        }
    }
}
