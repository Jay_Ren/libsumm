﻿using Common.Utils;
using NLP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Entity
{
   public class FileCorpusReader : CorpusReader
    {
        private INLPTool nlp;
        public FileCorpusReader(INLPTool nlp)
        {
            this.nlp = nlp;
        }

        public List<Corpus> Read(string str,int limit=10000)
        {
            var coll = new List<Corpus>();
            using (var reader = new StreamReader(str)) {
                Corpus curCorpus =null;
                Document curDoc = null;
                int position=0;
                int corpusCount = 1;
                int count = 0;
                while (!reader.EndOfStream) {

                    if (coll.Count > limit)
                        break;

                    string line = reader.ReadLine().TrimEnd();
                    
                    count++;
                    if (count % 100 == 0) {
                        Console.Write("[Read] {0} {1}\r",str,count);
                    }

                    if (curCorpus == null) {
                        coll.Add(new Corpus());
                        curCorpus = coll[coll.Count - 1];

                        corpusCount++;
                        curCorpus.Documents = new List<Document>();
                        curCorpus.Queries = new List<Sentence>();
                        curCorpus.References = new Dictionary<string, Summary>();
                    }

                    if (line.Length == 0)
                    {
                        coll.Add(new Corpus());
                        curCorpus = coll[coll.Count - 1];
                        
                        corpusCount++;
                        curCorpus.Documents = new List<Document>();
                        curCorpus.Queries = new List<Sentence>();
                        curCorpus.References = new Dictionary<string, Summary>();
                    }
                    else {
                        if (line.StartsWith("#QUERY#"))
                        {
                            var temp = line.Split(new char[] { '\t' }, StringSplitOptions.None);
                            var querySent = temp[1];
                            var querySents = querySent.Split(new string[] { "#S#" }, StringSplitOptions.RemoveEmptyEntries);
                            querySent = querySent.Replace("#S#"," ");

                            string[] queryParses = null;
                            if (temp.Length >= 3)
                            {
                                queryParses = temp[2].Split(new string[] { "#S#" }, StringSplitOptions.RemoveEmptyEntries);
                            }

                            var id = CommonUtils.MD5(str + "-query-" + corpusCount + "-" + querySent);
                            var tokens = nlp.Tokenize(querySent);
                            Sentence query = new Sentence(id, querySent, tokens, -1f, -1f, -1, -1, tokens.Count, "NULL");
                            curCorpus.Query = query;

                            for(int i=0;i<querySents.Length;i++)
                            {
                                var sent = querySents[i];
                                id = CommonUtils.MD5(str + "-query-" + corpusCount + "-" + sent);
                                tokens = nlp.Tokenize(sent);
                                query = new Sentence(id, sent, tokens, -1f, -1f, -1, -1, tokens.Count, "NULL");
                                if (queryParses != null)
                                {
                                    query.ParserTree = queryParses[i];
                                }
                                curCorpus.Queries.Add(query);
                            }
                        }
                        else if (line.StartsWith("#TITLE#"))
                        {
                            var temp = line.Split(new char[] { '\t' }, StringSplitOptions.None);
                            var docid = int.Parse(temp[1]);
                            var strdocid = CommonUtils.MD5(str + "-" + docid);
                            if (curDoc == null || curDoc.ID != strdocid)
                            {
                                curDoc = new Document();
                                curDoc.ID = strdocid;
                                curDoc.Corpus = curCorpus;
                                curCorpus.Documents.Add(curDoc);
                                curDoc.Sentences = new List<Sentence>();
                                position = 0;
                            }

                            var id = CommonUtils.MD5(str + "-title-" + corpusCount + "-" + temp[2]);
                            var tokens = nlp.Tokenize(temp[2]);
                            Sentence title = new Sentence(id, temp[2], tokens, -1f, -1f, -1, -1, tokens.Count, "NULL");
                            if (temp.Length >= 4)
                            {
                                title.ParserTree = temp[3];
                            }
                            curDoc.Title = title;
                            title.Doc = curDoc;
                        }
                        else {
                            var temp = line.Split(new char[] { '\t' }, StringSplitOptions.None);

                            if (temp.Length == 1)
                            {
                                var sents = nlp.SplitSentence(line);

                                Summary summary = new Summary();
                                summary.Sentences = new List<Sentence>();
                                var RID = "Reference-" + (curCorpus.References.Count + 1);
                                curCorpus.References.Add(RID, summary);

                                foreach (var sent in sents)
                                {
                                    var sentid = CommonUtils.MD5(str + "-reference-" + curCorpus.References.Count);
                                    var tokens = nlp.Tokenize(sent);
                                    Sentence reference = new Sentence(sentid, sent, tokens, -1f, -1f, -1, -1, tokens.Count, RID);
                                    //reference.ParserTree = nlp.Parse(string.Join(" ", tokens.Select(t => { return t.LOWER; })));
                                    summary.Sentences.Add(reference);
                                }
                            }
                            else
                            {
                                var gtscore = float.Parse(temp[0]);
                                var docid = int.Parse(temp[1]);
                                var strdocid = CommonUtils.MD5(str + "-" + docid);
                                var content = temp[2];
                               
                                if (curDoc == null || curDoc.ID != strdocid)
                                {
                                    curDoc = new Document();
                                    curDoc.ID = strdocid;
                                    curDoc.Corpus = curCorpus;
                                    curCorpus.Documents.Add(curDoc);
                                    curDoc.Sentences = new List<Sentence>();
                                    position = 0;
                                }

                                position++;

                                var sentid = str + "-sent-" + position + "-" + temp[2];
                                var tokens = nlp.Tokenize(temp[2]);
                                Sentence sent = new Sentence(sentid, temp[2], tokens, gtscore, -1f, -1, position, tokens.Count, "NULL");
                                if (temp.Length >= 4) {
                                    sent.ParserTree = temp[3];
                                }
                                sent.Doc = curDoc;
                                curDoc.Sentences.Add(sent);
                            }
                        }

                     
                    }

                }
            }

            if (coll[coll.Count - 1].Documents.Count == 0) {
                coll.RemoveAt(coll.Count - 1);
            }
            

            Console.WriteLine();
            Console.WriteLine("Corpus Count: "+coll.Count);
            return coll;
        }

    }
}
