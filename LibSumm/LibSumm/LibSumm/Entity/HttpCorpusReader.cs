﻿using Common.Utils;
using LibSumm.Entity;
using NLP;
using StanSoft;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm.Entity
{
   public class HttpCorpusReader:CorpusReader
    {
       private INLPTool nlp;
       public HttpCorpusReader(INLPTool nlp)
       {
           this.nlp = nlp;
       }

       private Article LinkToArticle(string link)
        {
            string html = "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(link);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                html = reader.ReadToEnd();
            }
            Article article = Html2Article.GetArticle(html);
            return article;
        }

        private bool IsHttpLink(string link)
        {
            Uri uriResult;
            return Uri.TryCreate(link, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
        }
       public List<Corpus> Read(string str, int limit = 10000) {
           if (!IsHttpLink(str)) {
               throw new Exception("Not a http link.");
           }

           List<Corpus> results = new List<Corpus>();

           Corpus corpus = new Corpus();
           results.Add(corpus);
           corpus.Documents = new List<Document>();

           Article article = LinkToArticle(str);
           var titleTokens=nlp.Tokenize(article.Title);
           var title = new Sentence(CommonUtils.MD5(str + "title"), article.Title, titleTokens,-1, -1, -1, -1, titleTokens.Count, "NULL");

           Document doc = new Document();
           doc.ID = CommonUtils.MD5(str);
           corpus.Documents.Add(doc);
           doc.Corpus = corpus;
           doc.Title = title;
           title.Doc = doc;
           doc.Sentences = new List<Sentence>();

           var sents=nlp.SplitSentence(article.Content);
           int position = 1;
           foreach (var sent in sents) {
               var tokens = nlp.Tokenize(sent);
               var csent = new Sentence(CommonUtils.MD5(str + position), sent, tokens, -1, -1, -1, position, tokens.Count, "NULL");
               doc.Sentences.Add(csent);
               csent.Doc = doc;
               position++;
           }

           return results;
       }
    }
}
