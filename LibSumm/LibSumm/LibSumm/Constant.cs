﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibSumm
{
   public static class Constant
    {

        public static string Root = "../../../../root";
        public static string ROUGE_ROOT = Path.Combine(Root, @"Rouge\RELEASE-1.5.5");
        public static string PERL_ROOT = @"D:\Program Files\Perl\bin";
    }
}
