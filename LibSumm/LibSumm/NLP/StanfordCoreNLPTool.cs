﻿using edu.stanford.nlp.dcoref;
using edu.stanford.nlp.ling;
using edu.stanford.nlp.parser.lexparser;
using edu.stanford.nlp.pipeline;
using edu.stanford.nlp.process;
using edu.stanford.nlp.trees;
using edu.stanford.nlp.util;
using java.util;
using MSRA.NLC.Common.NLP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLP
{
    public class StanfordCoreNLPTool : INLPTool
    {
        private StanfordCoreNLP pipeline;
        private Properties props = new Properties();
        WordNetStemmer stemmer;
        //private string lexparserModelFile = @"edu\stanford\nlp\models\lexparser\englishPCFG.ser.gz";
        //private LexicalizedParser parser;
        //private TreeBinarizer binarizer;

        /// <summary>
        /// for english
        /// </summary>
        /// <param name="modelDir"></param>
        /// <param name="stemmerFile"></param>
        /// <param name="annotators"></param>
        public StanfordCoreNLPTool(string modelDir, string stemmerFile, string annotators)
        {
            if (stemmerFile != null)
            {
                stemmer = new WordNetStemmer();
                stemmer.Init(stemmerFile);
            }

            // lexparserModelFile = Path.Combine(modelDir, lexparserModelFile);
            //parser = LexicalizedParser.loadModel(lexparserModelFile);
            //binarizer = TreeBinarizer.simpleTreeBinarizer(parser.getTLPParams().headFinder(), parser.treebankLanguagePack());

            // Annotation pipeline configuration
            //tokenize, ssplit, pos, lemma, ner, parse, dcoref
            props.setProperty("annotators", annotators);
            props.setProperty("ner.useSUTime", "false");

            // We should change current directory, so StanfordCoreNLP could find all the model files automatically
            var curDir = Environment.CurrentDirectory;
            Directory.SetCurrentDirectory(modelDir);
            pipeline = new StanfordCoreNLP(props);
            Directory.SetCurrentDirectory(curDir);
        }

        /// <summary>
        /// for chinese
        /// </summary>
        /// <param name="modelDir"></param>
        public StanfordCoreNLPTool(string modelDir)
        {
            //# Pipeline options - lemma is no-op for Chinese but currently needed because coref demands it (bad old requirements system)
            //            annotators = segment, ssplit, pos, lemma, ner, parse

            //# segment
            //customAnnotatorClass.segment = edu.stanford.nlp.pipeline.ChineseSegmenterAnnotator

            //segment.model = edu / stanford / nlp / models / segmenter / chinese / ctb.gz
            //segment.sighanCorporaDict = edu / stanford / nlp / models / segmenter / chinese
            //segment.serDictionary = edu / stanford / nlp / models / segmenter / chinese / dict - chris6.ser.gz
            //segment.sighanPostProcessing = true

            //# sentence split
            //ssplit.boundaryTokenRegex = [.] |[!?] +|[。] |[！？] +

            //# pos
            //pos.model = edu / stanford / nlp / models / pos - tagger / chinese - distsim / chinese - distsim.tagger

            //# ner
            //ner.model = edu / stanford / nlp / models / ner / chinese.misc.distsim.crf.ser.gz
            //ner.applyNumericClassifiers = false
            //ner.useSUTime = false

            //# parse
            //parse.model = edu / stanford / nlp / models / lexparser / chineseFactored.ser.gz

            //# coref
            //coref.sieves = ChineseHeadMatch, ExactStringMatch, PreciseConstructs, StrictHeadMatch1, StrictHeadMatch2, StrictHeadMatch3, StrictHeadMatch4, PronounMatch
            //coref.input.type = raw
            //coref.postprocessing = true
            //coref.calculateFeatureImportance = false
            //coref.useConstituencyTree = true
            //coref.useSemantics = false
            //coref.md.type = RULE
            //coref.mode = hybrid
            //coref.path.word2vec =
            //coref.language = zh
            //coref.print.md.log = false
            //coref.defaultPronounAgreement = true
            //coref.zh.dict = edu / stanford / nlp / models / dcoref / zh - attributes.txt.gz
            props.setProperty("annotators", "segment, ssplit, pos, lemma, ner");
            //props.setProperty("annotators", "segment, ssplit, pos, lemma, ner, parse, dcoref");
            props.setProperty("customAnnotatorClass.segment", "edu.stanford.nlp.pipeline.ChineseSegmenterAnnotator");
            props.setProperty("segment.model", Path.Combine(modelDir, "edu/stanford/nlp/models/segmenter/chinese/ctb.gz"));
            props.setProperty("segment.sighanCorporaDict", Path.Combine(modelDir, "edu/stanford/nlp/models/segmenter/chinese"));
            props.setProperty("segment.serDictionary", Path.Combine(modelDir, "edu/stanford/nlp/models/segmenter/chinese/dict-chris6.ser.gz"));
            props.setProperty("segment.sighanPostProcessing", "true");
            props.setProperty("ssplit.boundaryTokenRegex", "[.]|[!?]+|[。]|[！？]+");
            props.setProperty("pos.model", Path.Combine(modelDir, "edu/stanford/nlp/models/pos-tagger/chinese-distsim/chinese-distsim.tagger"));
            props.setProperty("ner.model", Path.Combine(modelDir, "edu/stanford/nlp/models/ner/chinese.misc.distsim.crf.ser.gz"));
            props.setProperty("ner.applyNumericClassifiers", "false");
            props.setProperty("ner.useSUTime", "false");
            //props.setProperty("parse.model", Path.Combine(modelDir, "edu/stanford/nlp/models/lexparser/chineseFactored.ser.gz"));
            //props.setProperty("coref.sieves", "ChineseHeadMatch, ExactStringMatch, PreciseConstructs, StrictHeadMatch1, StrictHeadMatch2, StrictHeadMatch3, StrictHeadMatch4, PronounMatch");
            //props.setProperty("coref.input.type", "raw");
            //props.setProperty("coref.postprocessing", "true");
            //props.setProperty("coref.calculateFeatureImportance", "false");
            //props.setProperty("coref.useConstituencyTree", "true");
            //props.setProperty("coref.useSemantics", "false");
            //props.setProperty("coref.md.type", "RULE");
            //props.setProperty("coref.mode", "hybrid");
            //props.setProperty("coref.path.word2vec", "");
            //props.setProperty("coref.language", "zh");
            //props.setProperty("coref.print.md.log", "false");
            //props.setProperty("coref.defaultPronounAgreement", "true");
            //props.setProperty("coref.zh.dict", Path.Combine(modelDir, "edu/stanford/nlp/models/dcoref/zh-attributes.txt.gz"));

            var curDir = Environment.CurrentDirectory;
            Directory.SetCurrentDirectory(modelDir);
            pipeline = new StanfordCoreNLP(props);
            Directory.SetCurrentDirectory(curDir);
        }
        public StanfordCoreNLPTool(string modelDir, string stemmerFile)
        {

            //lexparserModelFile = Path.Combine(modelDir, lexparserModelFile);
            //parser = LexicalizedParser.loadModel(lexparserModelFile);

            // Annotation pipeline configuration
            props = new Properties();
            //tokenize, ssplit, pos, lemma, ner, parse, dcoref
            props.setProperty("annotators", "tokenize, ssplit, pos,lemma, ner, parse, dcoref");
            props.setProperty("ner.useSUTime", "false");

            // We should change current directory, so StanfordCoreNLP could find all the model files automatically
            var curDir = Environment.CurrentDirectory;
            Directory.SetCurrentDirectory(modelDir);
            pipeline = new StanfordCoreNLP(props);
            Directory.SetCurrentDirectory(curDir);
        }

        public List<string> SplitSentence(string input)
        {

            List<string> rs = new List<string>();

            // Annotation
            var annotation = new Annotation(input);
            pipeline.annotate(annotation);

            // these are all the sentences in this document
            // a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
            var sentences = annotation.get(typeof(CoreAnnotations.SentencesAnnotation));
            if (sentences == null)
            {
                return rs;
            }
            foreach (Annotation sentence in sentences as ArrayList)
            {
                rs.Add(sentence.toString());
            }

            return rs;
        }

        public string Parse(string input)
        {
            var annotation = new Annotation(input);
            pipeline.annotate(annotation);

            // these are all the sentences in this document
            // a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
            java.util.List coreMaps = (java.util.List)annotation.get(typeof(edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation));
            Tree tree = (Tree)((CoreMap)coreMaps.get(0)).get(typeof(edu.stanford.nlp.trees.TreeCoreAnnotations.BinarizedTreeAnnotation));
            //Tree binarized = binarizer.transformTree(tree);

            return tree.toString();

            //var rawWords = Sentence.toCoreLabelList(input.Split(' '));
            //var tree = parser.apply(rawWords);
            //Tree binarized = binarizer.transformTree(tree);

            //return binarized.toString();
        }

        public List<Token> Tokenize(string input)
        {
            List<Token> rs = new List<Token>();

            // Annotation
            var annotation = new Annotation(input);
            pipeline.annotate(annotation);


            // these are all the sentences in this document
            // a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
            java.util.List sentences = (java.util.List)annotation.get(typeof(edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation));

            for (int i = 0; i < sentences.size(); i++)
            {
                CoreMap sentence = (CoreMap)sentences.get(i);

                java.util.List tokens = (java.util.List)sentence.get(typeof(edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation));
                for (int j = 0; j < tokens.size(); j++)
                {
                    CoreLabel token = (CoreLabel)tokens.get(j);

                    // this is the text of the token
                    string word = (string)token.get(typeof(edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation));
                    string pos = null;
                    if (((string)props.get("annotators")).Contains("pos"))
                    {
                        // this is the POS tag of the token
                        pos = (string)token.get(typeof(edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation));
                    }

                    string lemma = null;
                    if (((string)props.get("annotators")).Contains("lemma"))
                    {
                        lemma = (string)token.get(typeof(edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation));
                    }
                    string stem = null;
                    //if (((string)props.get("annotators")).Contains("stem"))
                    if (stemmer != null)
                    {
                        stem = stemmer.GetBaseForm(word);
                    }
                    string ner = null;
                    if (((string)props.get("annotators")).Contains("ner"))
                    {
                        // this is the NER label of the token
                        ner = (string)token.get(typeof(edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation));
                    }
                    rs.Add(new Token(word, pos, ner, lemma, stem));
                }
            }

            return rs;
        }

        private string ReplaceFirst(string text, string oldstr, string newstr)
        {
            int pos = text.IndexOf(oldstr);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + newstr + text.Substring(pos + oldstr.Length);
        }

        public string DcorefStr(string input)
        {
            // Annotation
            var annotation = new Annotation(input);
            pipeline.annotate(annotation);

            java.util.ArrayList coreMaps = (java.util.ArrayList)annotation.get(typeof(edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation));

            List<string> sentences = new List<string>();
            foreach (CoreMap cm in coreMaps)
            {
                sentences.Add(cm.ToString());
            }

            var graph = (Map)annotation.get(typeof(edu.stanford.nlp.dcoref.CorefCoreAnnotations.CorefChainAnnotation));

            Iterator it = graph.keySet().iterator();
            while (it.hasNext())
            {
                var k = it.next();
                var v = (CorefChain)graph.get(k);
                string representativeStr = v.getRepresentativeMention().mentionSpan;
                List mentions = v.getMentionsInTextualOrder();
                for (int i = 0; i < mentions.size(); i++)
                {
                    CorefChain.CorefMention mention = (CorefChain.CorefMention)mentions.get(i);

                    string mentionStr = mention.mentionSpan;
                    int mentionStart = mention.startIndex;
                    int mentionEnd = mention.headIndex;
                    if (!mentionStr.Equals(representativeStr))
                        sentences[mention.sentNum - 1] = ReplaceFirst(sentences[mention.sentNum - 1], mentionStr, representativeStr);
                    //Console.WriteLine(mentionStr+": "+mention.startIndex+":"+mention.headIndex+":"+mention.sentNum+":"+mention.number);
                }

            }
            StringBuilder sb = new StringBuilder();
            foreach (string s in sentences)
            {
                //Console.WriteLine(s);
                sb.Append(s + " ");
            }
            return sb.ToString();
        }


        public List<DcorefPair> DcorefList(string input)
        {
            List<DcorefPair> rs = new List<DcorefPair>();

            // Annotation
            var annotation = new Annotation(input);
            pipeline.annotate(annotation);

            java.util.ArrayList coreMaps = (java.util.ArrayList)annotation.get(typeof(edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation));

            List<string> sentences = new List<string>();
            foreach (CoreMap cm in coreMaps)
            {
                sentences.Add(cm.ToString());
            }

            var graph = (Map)annotation.get(typeof(edu.stanford.nlp.dcoref.CorefCoreAnnotations.CorefChainAnnotation));

            Iterator it = graph.keySet().iterator();
            while (it.hasNext())
            {
                var k = it.next();
                var v = (CorefChain)graph.get(k);
                string representativeStr = v.getRepresentativeMention().mentionSpan;
                List mentions = v.getMentionsInTextualOrder();
                for (int i = 0; i < mentions.size(); i++)
                {
                    CorefChain.CorefMention mention = (CorefChain.CorefMention)mentions.get(i);

                    string mentionStr = mention.mentionSpan;
                    int mentionStart = mention.startIndex;
                    int mentionEnd = mention.headIndex;

                    if (!mentionStr.Equals(representativeStr))
                        rs.Add(new DcorefPair(sentences[mention.sentNum - 1], mentionStr, mentionStart, mentionEnd, representativeStr));
                }

            }

            //foreach (DcorefPair s in rs)
            //{
            //    Console.WriteLine(s);
            //}
            return rs;
        }

        public static void Test()
        {

            string nlpRoot = @"E:\projects\LibSumm\root";
            // Path to the folder with models extracted from `stanford-corenlp-3.5.2-models.jar`
            var stanfordnlpDir = Path.Combine(nlpRoot, "model/stanford-chinese-corenlp-models");

            INLPTool nlp = new StanfordCoreNLPTool(stanfordnlpDir);

            var text = "金大中对克林顿的讲话报以掌声：克林顿总统在会谈中重申，他坚定地支持韩国摆脱经济危机。";
            var sents = nlp.SplitSentence(text);
            foreach (var sent in sents)
            {
                Console.WriteLine(sent);
            }

            var words = nlp.Tokenize(text);
            foreach (var w in words)
            {
                Console.WriteLine(w);
            }
            //Console.WriteLine(nlp.Parse(text));

            //StreamWriter sw = new StreamWriter(@"D:\visual studio 2013\Projects\NNChat\model\word.pos.vocab");
            //StreamReader sr = new StreamReader(@"D:\visual studio 2013\Projects\NNChat\model\word.vocab");
            //while (!sr.EndOfStream) {
            //   string[] line= sr.ReadLine().Split(' ');
            //   Token t= nlp.Tokenize(line[0])[0];
            //   sw.WriteLine(line[0]+" "+line[1]+" "+t.POS);
            //}
            //sr.Close();
            //sw.Close();




            // Text for processing
            //var text = "Kosgi Santosh sent an email to Stanford,University. emails, emails, computers, \r\n He didn't he gets a reply. did Andy sent too. He get a reply of it. is are were was";

            //INLPTool nlp = new StanfordCoreNLPTool(stanfordnlpDir,stemmerFile, "tokenize, ssplit, pos, lemma");

            //List<Token> tokens = nlp.Tokenize(text);
            //foreach (Token t in tokens)
            //    Console.WriteLine(t);

            //Console.WriteLine(nlp.DcorefStr(text));

            //foreach (DcorefPair s in nlp.DcorefList(text))
            //    Console.WriteLine(s);

            // Result - Pretty Print
            //using (var stream = new java.io.FileOutputStream(@"D:\tmp.xml"))
            //{
            //    pipeline.xmlPrint(annotation, new java.io.PrintWriter(stream));
            //    //pipeline.jsonPrint(annotation, new java.io.PrintWriter(stream));
            //    Console.WriteLine(stream.toString());
            //    stream.close();
            //}

        }

    }
}
