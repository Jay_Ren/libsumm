﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLP
{
    public class DcorefPair
    {
        public string Sentence { get; set; }
        public int StartIndex { get; set; }
        public int EndIndex { get; set; }
        public string Mention { get; set; }
        public string Representative { get; set; }

        public DcorefPair(string sentence, string mention, int start, int end, string representative)
        {
            Sentence = sentence;
            Mention = mention;
            StartIndex = start;
            EndIndex = end;
            Representative = representative;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
