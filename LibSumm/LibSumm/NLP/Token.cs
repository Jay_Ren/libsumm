﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLP
{
    public class Token
    {
        public string CONTENT { get; set; }

        public string LOWER { get; set; }
        public string POS { get; set; }
        public string NER { get; set; }

        public string LEMMA { get; set; }
        public string STEM { get; set; }

        public Token(string content, string pos, string ner, string lemma, string stem)
        {
            CONTENT = content;
            LOWER = content.ToLower();
            POS = pos;
            NER = ner;
            LEMMA = lemma;
            STEM = stem;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
