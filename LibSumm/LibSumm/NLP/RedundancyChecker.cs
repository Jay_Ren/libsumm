﻿using MSRA.NLC.Common.NLP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLP
{
    public class NgramRedundancyChecker{
        public int NgramSize { get; set; }

        public double NewThreshold { get; set; }

        private StopWordHandler stop = null;

        private HashSet<string> summaryNgramSet;

        public NgramRedundancyChecker(string stopFile, int ngramSize = 2, double newThreshold = 0.5) {
            NgramSize = ngramSize;
            this.stop = new StopWordHandler(stopFile);
            NewThreshold = newThreshold;
            summaryNgramSet = new HashSet<string>();
        }

        public NgramRedundancyChecker(StopWordHandler stop, int ngramSize = 2, double newThreshold = 0.5)
        {
            NgramSize = ngramSize;
            this.stop = stop;
            NewThreshold = newThreshold;
            summaryNgramSet = new HashSet<string>();
        }

        public void AddToSummary(string[] sentence)
        {
            var ngrams = ToNgrams(sentence);
            foreach (string ngram in ngrams)
            {
                summaryNgramSet.Add(ngram);
            }
        }

        public const string STOP_MASK = "#STOP#";

        public List<string> ToNgrams(string[] sentence)
        {
            List<string> ngrams = new List<string>();
            for (int i = 0; i <= sentence.Length - NgramSize; i++)
            {
                bool allStop = true;
                List<string> wordList = new List<string>(NgramSize);
                for (int j = 0; j < NgramSize; j++)
                {
                    string word = sentence[i + j];
                    if (stop.IsStopWord(word))
                    {
                        wordList.Add(STOP_MASK);
                        continue;
                    }
                    allStop = false;
                    wordList.Add(word);
                }
                if (allStop) continue;
                string ngram = string.Join(" ", wordList);
                ngrams.Add(ngram);
            }
            return ngrams;
        }

        public bool IsRedendant(string[] sentence)
        {
            var ngrams = ToNgrams(sentence);
            if (ngrams.Count == 0) return true;
            int newCount = ngrams.Where(x => !summaryNgramSet.Contains(x)).Count();
            double newRatio = newCount / (double)sentence.Length;
            return newRatio < NewThreshold;
        }
    }
}
