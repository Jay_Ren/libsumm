﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NLP
{
    public interface INLPTool
    {
        string DcorefStr(string input);
        List<DcorefPair> DcorefList(string input);
        List<string> SplitSentence(string input);
        List<Token> Tokenize(string input);
        string Parse(string input);

    }
   
}
